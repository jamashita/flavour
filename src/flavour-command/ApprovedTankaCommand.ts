import { IQuery } from '../flavour-general/MySQL/IQuery';
import { TankaID } from '../flavour-vo/TankaID';

export class ApprovedTankaCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): ApprovedTankaCommand {
    return new ApprovedTankaCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(tankaID: TankaID): Promise<any> {
    const query: string = `INSERT INTO approved_tankas VALUES (
      :tankaID
      );`;

    return this.query.execute(query, {
      tankaID: tankaID.get()
    });
  }

  public delete(tankaID: TankaID): Promise<any> {
    const query: string = `DELETE FROM approved_tankas
      WHERE tanka_id = :tankaID;`;

    return this.query.execute(query, {
      tankaID: tankaID.get()
    });
  }
}
