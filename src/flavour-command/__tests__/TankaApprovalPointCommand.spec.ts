import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { FlavourAccountID } from '../../flavour-vo/FlavourAccountID';
import { TankaApprovalPointCommand } from '../TankaApprovalPointCommand';

describe('TankaApprovalPointCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const tankaApprovalPointCommand: TankaApprovalPointCommand = TankaApprovalPointCommand.getInstance(query);
      await tankaApprovalPointCommand.create(flavourAccountID);

      expect(stub.withArgs(`INSERT INTO tanka_approval_points VALUES (
      :flavourAccountID,
      0
      );`, {
        flavourAccountID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true)
    });
  });

  describe('update', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const tankaApprovalPointCommand: TankaApprovalPointCommand = TankaApprovalPointCommand.getInstance(query);
      await tankaApprovalPointCommand.update(flavourAccountID, 200);

      expect(stub.withArgs(`UPDATE tanka_approval_points SET
      points = :points
      WHERE flavour_account_id = :flavourAccountID;`, {
        points: 200,
        flavourAccountID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true)
    });
  });
});
