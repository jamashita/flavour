import { Identifier } from '../flavour-general/Identifier';
import { JSONable } from '../flavour-general/JSONable';

export abstract class Entity<T extends Identifier> implements JSONable {

  public abstract getIdentifier(): T;

  public abstract toJSON(): any;

  public abstract toString(): string;

  public abstract copy(): Entity<T>;

  public equals(other: Entity<T>): boolean {
    if (this === other) {
      return true;
    }
    if (this.getIdentifier().equals(other.getIdentifier())) {
      return true;
    }

    return false;
  }
}
