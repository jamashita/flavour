import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Meaning } from '../../flavour-entity/Meaning';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { LanguageID } from '../../flavour-vo/LanguageID';
import { MeaningDescription } from '../../flavour-vo/MeaningDescription';
import { MeaningID } from '../../flavour-vo/MeaningID';
import { WordID } from '../../flavour-vo/WordID';
import { MeaningCommand } from '../MeaningCommand';

describe('MeaningCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const meaning: Meaning = Meaning.from(
        MeaningID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        MeaningDescription.of('meaning')
      );
      const wordID: WordID = WordID.of('90d73259-1bc0-41e5-b2d8-c237915f5276');
      const languageID: LanguageID = LanguageID.of(3);

      const meaningCommand: MeaningCommand = MeaningCommand.getInstance(query);
      await meaningCommand.create(meaning, wordID, languageID);

      expect(stub.withArgs(`INSERT INTO meanings VALUE(
      :meaningID,
      :wordID,
      :languageID,
      :meaning
      );`, {
        meaningID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        wordID: '90d73259-1bc0-41e5-b2d8-c237915f5276',
        languageID: 3,
        meaning: 'meaning'
      }).called).toEqual(true);
    });
  });

  describe('update', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const meaning: Meaning = Meaning.from(
        MeaningID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        MeaningDescription.of('meaning')
      );

      const meaningCommand: MeaningCommand = MeaningCommand.getInstance(query);
      await meaningCommand.update(meaning);

      expect(stub.withArgs(`UPDATE meanings SET
      meaning = :meaning
      WHERE meaning_id = :meaningID;`, {
        meaning: 'meaning',
        meaningID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });

  describe('delete', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const meaningID: MeaningID = MeaningID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const meaningCommand: MeaningCommand = MeaningCommand.getInstance(query);
      await meaningCommand.delete(meaningID);

      expect(stub.withArgs(`DELETE FROM meanings
      WHERE meaning_id = :meaningID;`, {
        meaningID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });

  describe('deleteByWordID', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const wordID: WordID = WordID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const meaningCommand: MeaningCommand = MeaningCommand.getInstance(query);
      await meaningCommand.deleteByWordID(wordID);

      expect(stub.withArgs(`DELETE FROM meanings
      WHERE word_id = :wordID;`, {
        wordID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });
});
