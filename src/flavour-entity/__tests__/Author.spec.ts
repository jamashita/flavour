import 'jest';
import { AuthorHiragana } from '../../flavour-vo/AuthorHiragana';
import { AuthorID } from '../../flavour-vo/AuthorID';
import { AuthorName } from '../../flavour-vo/AuthorName';
import { AuthorRomaji } from '../../flavour-vo/AuthorRomaji';
import { Author, AuthorJSON, AuthorRow } from '../Author';

describe('Author', () => {
  describe('equals', () => {
    it('returns true if the id equals', () => {
      const author1: Author = Author.from(AuthorID.of('07ca3964-f97e-4120-9888-64001682e301'), AuthorName.of('author 1'), AuthorHiragana.of('ひらがな'), AuthorRomaji.of('romaji'));
      const author2: Author = Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author 1'), AuthorHiragana.of('ひらがな'), AuthorRomaji.of('romaji'));
      const author3: Author = Author.from(AuthorID.of('07ca3964-f97e-4120-9888-64001682e301'), AuthorName.of('author 2'), AuthorHiragana.of('ひらがな'), AuthorRomaji.of('romaji'));
      const author4: Author = Author.from(AuthorID.of('07ca3964-f97e-4120-9888-64001682e301'), AuthorName.of('author 1'), AuthorHiragana.of('カタカナ'), AuthorRomaji.of('romaji'));
      const author5: Author = Author.from(AuthorID.of('07ca3964-f97e-4120-9888-64001682e301'), AuthorName.of('author 1'), AuthorHiragana.of('ひらがな'), AuthorRomaji.of('ローマ字'));

      expect(author1.equals(author1)).toEqual(true);
      expect(author1.equals(author2)).toEqual(false);
      expect(author1.equals(author3)).toEqual(true);
      expect(author1.equals(author4)).toEqual(true);
      expect(author1.equals(author5)).toEqual(true);
    });
  });

  describe('copy', () => {
    it('every properties are copied', () => {
      const authorID: AuthorID = AuthorID.of('07ca3964-f97e-4120-9888-64001682e301');
      const name: AuthorName = AuthorName.of('author');
      const hiragana: AuthorHiragana = AuthorHiragana.of('ひらがな');
      const romaji: AuthorRomaji = AuthorRomaji.of('romaji');

      const author: Author = Author.from(authorID, name, hiragana, romaji);
      const copied: Author = author.copy();

      expect(copied.getAuthorID()).toEqual(authorID);
      expect(copied.getName()).toEqual(name);
      expect(copied.getHiragana()).toEqual(hiragana);
      expect(copied.getRomaji()).toEqual(romaji);
    });
  });

  describe('toJSON', () => {
    it('normal case', () => {
      const authorID: AuthorID = AuthorID.of('07ca3964-f97e-4120-9888-64001682e301');
      const name: AuthorName = AuthorName.of('author');
      const hiragana: AuthorHiragana = AuthorHiragana.of('ひらがな');
      const romaji: AuthorRomaji = AuthorRomaji.of('romaji');

      const author: Author = Author.from(authorID, name, hiragana, romaji);

      expect(author.toJSON()).toEqual({
        authorID: '07ca3964-f97e-4120-9888-64001682e301',
        name: 'author',
        hiragana: 'ひらがな',
        romaji: 'romaji'
      });
    });
  });

  describe('from', () => {
    it('normal case', () => {
      const authorID: AuthorID = AuthorID.of('07ca3964-f97e-4120-9888-64001682e301');
      const name: AuthorName = AuthorName.of('Edgar Allan Poe');
      const hiragana: AuthorHiragana = AuthorHiragana.of('えどがーあらんぽー');
      const romaji: AuthorRomaji = AuthorRomaji.of('edoga- aran po-');

      const author: Author = Author.from(authorID, name, hiragana, romaji);

      expect(author.getAuthorID()).toEqual(authorID);
      expect(author.getName()).toEqual(name);
      expect(author.getHiragana()).toEqual(hiragana);
      expect(author.getRomaji()).toEqual(romaji);
    });
  });

  describe('fromJSON', () => {
    it('normal case', () => {
      const json: AuthorJSON = {
        authorID: '07ca3964-f97e-4120-9888-64001682e301',
        name: 'Edgar Allan Poe',
        hiragana: 'えどがーあらんぽー',
        romaji: 'edoga- aran po-'
      };

      const author: Author = Author.fromJSON(json);

      expect(author.getAuthorID().get()).toEqual(json.authorID);
      expect(author.getName().get()).toEqual(json.name);
      expect(author.getHiragana().get()).toEqual(json.hiragana);
      expect(author.getRomaji().get()).toEqual(json.romaji);
    });
  });

  describe('fromRow', () => {
    it('normal case', () => {
      const row: AuthorRow = {
        authorID: '07ca3964-f97e-4120-9888-64001682e301',
        name: 'Edgar Allan Poe',
        hiragana: 'えどがーあらんぽー',
        romaji: 'edoga- aran po-'
      };

      const author: Author = Author.fromRow(row);

      expect(author.getAuthorID().get()).toEqual(row.authorID);
      expect(author.getName().get()).toEqual(row.name);
      expect(author.getHiragana().get()).toEqual(row.hiragana);
      expect(author.getRomaji().get()).toEqual(row.romaji);
    });
  });
});
