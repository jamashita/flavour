import { ValueObject } from './ValueObject';

export class TankaRomaji extends ValueObject {
  private romaji: string;

  public static of(romaji: string): TankaRomaji {
    return new TankaRomaji(romaji);
  }

  private constructor(romaji: string) {
    super();
    this.romaji = romaji;
  }

  public get(): string {
    return this.romaji;
  }

  public equals(other: TankaRomaji): boolean {
    if (this === other) {
      return true;
    }
    if (this.romaji === other.get()) {
      return true;
    }

    return false;
  }

  public toString(): string {
    return this.romaji;
  }
}
