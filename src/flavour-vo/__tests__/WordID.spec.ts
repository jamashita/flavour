import 'jest';
import { WordID } from '../WordID';

describe('WordID', () => {
  describe('equals', () => {
    it('returns true if the property is the same', () => {
      const wordID1: WordID = WordID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');
      const wordID2: WordID = WordID.of('1f1600a8-803b-49a9-b283-e167c88523f7');
      const wordID3: WordID = WordID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');

      expect(wordID1.equals(wordID1)).toEqual(true);
      expect(wordID1.equals(wordID2)).toEqual(false);
      expect(wordID1.equals(wordID3)).toEqual(true);
    });
  });
});
