import 'jest';
import { TankaID } from '../TankaID';

describe('TankaID', () => {
  describe('equals', () => {
    it('returns true if the property is the same', () => {
      const tankaID1: TankaID = TankaID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');
      const tankaID2: TankaID = TankaID.of('1f1600a8-803b-49a9-b283-e167c88523f7');
      const tankaID3: TankaID = TankaID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');

      expect(tankaID1.equals(tankaID1)).toEqual(true);
      expect(tankaID1.equals(tankaID2)).toEqual(false);
      expect(tankaID1.equals(tankaID3)).toEqual(true);
    });
  });
});
