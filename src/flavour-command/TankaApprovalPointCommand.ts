import { IQuery } from '../flavour-general/MySQL/IQuery';
import { FlavourAccountID } from '../flavour-vo/FlavourAccountID';

export class TankaApprovalPointCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): TankaApprovalPointCommand {
    return new TankaApprovalPointCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(flavourAccountID: FlavourAccountID): Promise<any> {
    const query: string = `INSERT INTO tanka_approval_points VALUES (
      :flavourAccountID,
      0
      );`;

    return this.query.execute(query, {
      flavourAccountID: flavourAccountID.get()
    });
  }

  public update(flavourAccountID: FlavourAccountID, points: number): Promise<any> {
    const query: string = `UPDATE tanka_approval_points SET
      points = :points
      WHERE flavour_account_id = :flavourAccountID;`;

    return this.query.execute(query, {
      points,
      flavourAccountID: flavourAccountID.get()
    });
  }
}
