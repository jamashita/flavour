import express from 'express';
import { BAD_REQUEST, CREATED, INTERNAL_SERVER_ERROR, OK } from 'http-status';
import log4js from 'log4js';
import { Author, AuthorJSON } from '../../flavour-entity/Author';
import { Type } from '../../flavour-general/Type/Type';
import { AuthorInteractor } from '../../flavour-interactor/AuthorInteractor';
import { AuthorID } from '../../flavour-vo/AuthorID';

const router: express.Router = express.Router();
const logger: log4js.Logger = log4js.getLogger();

const authorInteractor: AuthorInteractor = AuthorInteractor.getInstance();

router.post('/', async (req: express.Request, res: express.Response): Promise<any> => {
  const {
    authorID,
    name,
    hiragana,
    romaji
  } = req.body;

  if (!Type.isString(authorID)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isString(name)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isString(hiragana)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isString(romaji)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }

  try {
    const json: AuthorJSON = req.body;
    const author: Author = Author.fromJSON(json);
    await authorInteractor.create(author);

    res.sendStatus(CREATED);
  }
  catch (err) {
    logger.fatal(err.message);
    res.sendStatus(INTERNAL_SERVER_ERROR);
  }
});

router.put('/', async (req: express.Request, res: express.Response): Promise<any> => {
  const {
    authorID,
    name,
    hiragana,
    romaji
  } = req.body;

  if (!Type.isString(authorID)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isString(name)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isString(hiragana)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isString(romaji)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }

  try {
    const json: AuthorJSON = req.body;
    const author: Author = Author.fromJSON(json);
    await authorInteractor.update(author);

    res.sendStatus(OK);
  }
  catch (err) {
    logger.fatal(err.message);
    res.sendStatus(INTERNAL_SERVER_ERROR);
  }
});

router.delete('/:authorID([0-9a-f\-]{36})', async (req: express.Request, res: express.Response): Promise<any> => {
  const {
    authorID
  } = req.params;

  try {
    await authorInteractor.delete(AuthorID.of(authorID));

    res.sendStatus(OK);
  }
  catch (err) {
    logger.fatal(err.message);
    res.sendStatus(INTERNAL_SERVER_ERROR);
  }
});

export const AuthorController: express.Router = router;
