import { RuntimeError } from '../flavour-error/RuntimeError';
import { UUID } from '../flavour-general/UUID';
import { ValueObject } from './ValueObject';

export class MeaningID extends ValueObject {
  private id: string;

  public static of(id: string): MeaningID {
    if (id.length === UUID.size()) {
      return new MeaningID(id);
    }

    throw new RuntimeError(`MeaningID requires ${UUID.size()} LENGTH`);
  }

  private constructor(id: string) {
    super();
    this.id = id;
  }

  public get(): string {
    return this.id;
  }

  public equals(other: MeaningID): boolean {
    if (this === other) {
      return true;
    }
    if (this.id === other.get()) {
      return true;
    }

    return false;
  }

  public toString(): string {
    return this.id;
  }
}
