import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Translation } from '../../flavour-entity/Translation';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { FlavourAccountID } from '../../flavour-vo/FlavourAccountID';
import { TranslationDescription } from '../../flavour-vo/TranslationDescription';
import { TranslationID } from '../../flavour-vo/TranslationID';
import { TranslationEditLogCommand } from '../TranslationEditLogCommand';

describe('TranslationEditLogCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const translation: Translation = Translation.from(
        TranslationID.of('2a24aea8-5d97-4ff6-8168-de30f59417e6'),
        TranslationDescription.of('translation')
      );
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const translationEditLogCommand: TranslationEditLogCommand = TranslationEditLogCommand.getInstance(query);
      await translationEditLogCommand.create(translation, flavourAccountID);

      expect(stub.withArgs(`INSERT INTO translation_edit_logs VALUES (
      :translationID,
      :flavourAccountID,
      :translation,
      UTC_TIMESTAMP()
      );`, {
        translationID: '2a24aea8-5d97-4ff6-8168-de30f59417e6',
        flavourAccountID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        translation: 'translation'
      }).called).toEqual(true);
    });
  });
});
