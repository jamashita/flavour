const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const rimraf = require('rimraf');

const plumber = require('gulp-plumber');

const ts = require('gulp-typescript');
const tsc = ts.createProject('./tsconfig.json');

const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');

const webpack = require('webpack');
const stream = require('webpack-stream');
const webpackConfig = require('./webpack.config');

gulp.task('flavour-command', () => {
  return gulp.src(['src/flavour-command/**/*.ts'], {
    since : gulp.lastRun('flavour-command')
  })
    .pipe(plumber())
    .pipe(tsc())
    .pipe(gulp.dest('dist/flavour-command'));
});

gulp.task('flavour-controller', () => {
  return gulp.src(['src/flavour-controller/**/*.ts'], {
    since : gulp.lastRun('flavour-controller')
  })
    .pipe(plumber())
    .pipe(tsc())
    .pipe(gulp.dest('dist/flavour-controller'));
});

gulp.task('flavour-entity', () => {
  return gulp.src(['src/flavour-entity/**/*.ts'], {
    since : gulp.lastRun('flavour-entity')
  })
    .pipe(plumber())
    .pipe(tsc())
    .pipe(gulp.dest('dist/flavour-entity'));
});

// gulp.task('flavour-enum', () => {
//   return gulp.src(['src/flavour-enum/**/*.ts'], {
//     since : gulp.lastRun('flavour-enum')
//   })
//     .pipe(plumber())
//     .pipe(tsc())
//     .pipe(gulp.dest('dist/flavour-enum'));
// });

gulp.task('flavour-error', () => {
  return gulp.src(['src/flavour-error/**/*.ts'], {
    since : gulp.lastRun('flavour-error')
  })
    .pipe(plumber())
    .pipe(tsc())
    .pipe(gulp.dest('dist/flavour-error'));
});

// gulp.task('flavour-frontend', () => {
//   return gulp.src(['src/flavour-frontend/**/*.ts', 'src/flavour-frontend/**/*.tsx'])
//     .pipe(plumber())
//     .pipe(stream(webpackConfig, webpack))
//     .pipe(gulp.dest('dist/flavour-server/public/js'));
// });

gulp.task('flavour-general', () => {
  return gulp.src(['src/flavour-general/**/*.ts'], {
    since : gulp.lastRun('flavour-general')
  })
    .pipe(plumber())
    .pipe(tsc())
    .pipe(gulp.dest('dist/flavour-general'));
});

gulp.task('flavour-infrastructure', () => {
  return gulp.src(['src/flavour-infrastructure/**/*.ts'], {
    since : gulp.lastRun('flavour-infrastructure')
  })
    .pipe(plumber())
    .pipe(tsc())
    .pipe(gulp.dest('dist/flavour-infrastructure'));
});

gulp.task('flavour-interactor', () => {
  return gulp.src(['src/flavour-interactor/**/*.ts'], {
    since : gulp.lastRun('flavour-interactor')
  })
    .pipe(plumber())
    .pipe(tsc())
    .pipe(gulp.dest('dist/flavour-interactor'));
});

gulp.task('flavour-query', () => {
  return gulp.src(['src/flavour-query/**/*.ts'], {
    since : gulp.lastRun('flavour-query')
  })
    .pipe(plumber())
    .pipe(tsc())
    .pipe(gulp.dest('dist/flavour-query'));
});

gulp.task('flavour-server', () => {
  return gulp.src(['src/flavour-server/**/*.ts'], {
    since : gulp.lastRun('flavour-server')
  })
    .pipe(plumber())
    .pipe(tsc())
    .pipe(gulp.dest('dist/flavour-server'));
});

gulp.task('flavour-service', () => {
  return gulp.src(['src/flavour-service/**/*.ts'], {
    since : gulp.lastRun('flavour-service')
  })
    .pipe(plumber())
    .pipe(tsc())
    .pipe(gulp.dest('dist/flavour-service'));
});

gulp.task('flavour-transaction', () => {
  return gulp.src(['src/flavour-transaction/**/*.ts'], {
    since : gulp.lastRun('flavour-transaction')
  })
    .pipe(plumber())
    .pipe(tsc())
    .pipe(gulp.dest('dist/flavour-transaction'));
});

gulp.task('flavour-vo', () => {
  return gulp.src(['src/flavour-vo/**/*.ts'], {
    since : gulp.lastRun('flavour-vo')
  })
    .pipe(plumber())
    .pipe(tsc())
    .pipe(gulp.dest('dist/flavour-vo'));
});

gulp.task('pug', () => {
  return gulp.src(['src/flavour-server/views/*.pug'])
    .pipe(gulp.dest('dist/flavour-server/views'));
});

gulp.task('sass', () => {
  return gulp.src(['src/flavour-server/sass/*.scss'])
    .pipe(plumber())
    .pipe(sass())
    .pipe(autoprefixer({
      'browsers': ['last 4 versions']
    }))
    .pipe(cleanCSS())
    .pipe(gulp.dest('dist/flavour-server/public/css'));
});

gulp.task('favicon', () => {
  return gulp.src(['src/flavour-server/*.ico'])
    .pipe(gulp.dest('dist/flavour-server'));
});

gulp.task('font', () => {
  return gulp.src(['node_modules/@fortawesome/fontawesome-free/webfonts/*'])
    .pipe(gulp.dest('dist/flavour-server/public/webfonts'));
});

gulp.task('nodemon', (callback) => {
  let started = false;
  return nodemon({
    script: 'dist/flavour-server/Server.js',
    watch: ['dist/**/*.js'],
    ext: 'js',
    stdout: true,
    delay: 500,
    env: {
      'NODE_ENV': 'development'
    }
  }).on('start', () => {
    if (!started) {
      callback();
      started = true;
    }
  }).on('restart', () => {
    console.log('SERVER RESTARTED');
  });
});

gulp.task('clean', (callback) => {
  rimraf('dist', callback);
});

gulp.task(
  'build',
  gulp.series(
    'clean',
    gulp.parallel(
      'pug',
      'sass',
      'favicon',
      'font'
    ),
    'flavour-command',
    'flavour-controller',
    'flavour-entity',
    // 'flavour-enum',
    'flavour-error',
    // 'flavour-frontend',
    'flavour-general',
    'flavour-infrastructure',
    'flavour-interactor',
    'flavour-query',
    'flavour-server',
    'flavour-service',
    'flavour-transaction',
    'flavour-vo'
  )
);

gulp.task(
  'default',
  gulp.parallel(
    'nodemon',
    (callback) => {
      gulp.watch('src/flavour-command/**/*.ts', gulp.series('flavour-command'));
      gulp.watch('src/flavour-controller/**/*.ts', gulp.series('flavour-controller'));
      gulp.watch('src/flavour-entity/**/*.ts', gulp.series('flavour-entity'));
      // gulp.watch('src/flavour-enum/**/*.ts', gulp.series('flavour-enum'));
      gulp.watch('src/flavour-error/**/*.ts', gulp.series('flavour-error'));
      gulp.watch('src/flavour-general/**/*.ts', gulp.series('flavour-general'));
      gulp.watch('src/flavour-infrastructure/**/*.ts', gulp.series('flavour-infrastructure'));
      gulp.watch('src/flavour-interactor/**/*.ts', gulp.series('flavour-interactor'));
      gulp.watch('src/flavour-query/**/*.ts', gulp.series('flavour-query'));
      gulp.watch('src/flavour-server/**/*.ts', gulp.series('flavour-server'));
      gulp.watch('src/flavour-service/**/*.ts', gulp.series('flavour-service'));
      gulp.watch('src/flavour-transaction/**/*.ts', gulp.series('flavour-transaction'));
      gulp.watch('src/flavour-vo/**/*.ts', gulp.series('flavour-vo'));
      gulp.watch('src/flavour-server/views/*.pug', gulp.series('pug'));
      gulp.watch('src/flavour-server/sass/*.scss', gulp.series('sass'));
      callback();
    }
  )
);
