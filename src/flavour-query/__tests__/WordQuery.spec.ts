import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Words } from '../../flavour-entity/collection/Words';
import { Language } from '../../flavour-entity/Language';
import { Meaning } from '../../flavour-entity/Meaning';
import { None } from '../../flavour-general/Optional/None';
import { flavourMySQL } from '../../flavour-infrastructure/FlavourMySQL';
import { ISO639 } from '../../flavour-vo/ISO639';
import { LanguageID } from '../../flavour-vo/LanguageID';
import { LanguageName } from '../../flavour-vo/LanguageName';
import { TankaID } from '../../flavour-vo/TankaID';
import { WordQuery } from '../WordQuery';

describe('WordQuery', () => {
  describe('findByTankaID', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      flavourMySQL.execute = stub;
      stub.resolves([
        {
          wordID: '998106de-b2e7-4981-9643-22cd30cd74de',
          word: 'word1',
          meaningID: null,
          meaning: null
        },
        {
          wordID: '511fca29-cff7-489d-98e0-7461b36da2eb',
          word: 'word2',
          meaningID: 'cfad1044-3e36-49d7-8730-baee833676f2',
          meaning: 'meaning2'
        }
      ]);

      const wordQuery: WordQuery = WordQuery.getInstance();
      const words: Words = await wordQuery.findByTankaID(TankaID.of('a5cab4b6-81e9-4e73-94a5-8afb3b4b1307'), Language.from(LanguageID.of(1), LanguageName.of('japanese'), ISO639.of('ja')));

      expect(words.length()).toEqual(2);
      expect(words.get(0).getWordID().get()).toEqual('998106de-b2e7-4981-9643-22cd30cd74de');
      expect(words.get(0).getWord().get()).toEqual('word1');
      expect(words.get(0).getMeaning()).toEqual(None.of<Meaning>());
      expect(words.get(1).getWordID().get()).toEqual('511fca29-cff7-489d-98e0-7461b36da2eb');
      expect(words.get(1).getWord().get()).toEqual('word2');
      expect(words.get(1).getMeaning().isPresent()).toEqual(true);
      expect(words.get(1).getMeaning().get().getMeaningID().get()).toEqual('cfad1044-3e36-49d7-8730-baee833676f2');
      expect(words.get(1).getMeaning().get().getMeaning().get()).toEqual('meaning2');
    });
  });
});
