import { AuthorCommand } from '../flavour-command/AuthorCommand';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { ITransaction } from '../flavour-general/MySQL/ITransaction';
import { AuthorID } from '../flavour-vo/AuthorID';

export class AuthorDeleteTransaction implements ITransaction {
  private authorID: AuthorID;

  public static getInstance(authorID: AuthorID): AuthorDeleteTransaction {
    return new AuthorDeleteTransaction(authorID);
  }

  private constructor(authorID: AuthorID) {
    this.authorID = authorID;
  }

  public with(query: IQuery): Promise<any> {
    const authorCommand: AuthorCommand = AuthorCommand.getInstance(query);

    const {
      authorID
    } = this;

    return authorCommand.deleteByAuthorID(authorID);
  }
}
