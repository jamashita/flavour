import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Meaning } from '../../flavour-entity/Meaning';
import { Word } from '../../flavour-entity/Word';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { present } from '../../flavour-general/Optional/Present';
import { FlavourAccountID } from '../../flavour-vo/FlavourAccountID';
import { MeaningDescription } from '../../flavour-vo/MeaningDescription';
import { MeaningID } from '../../flavour-vo/MeaningID';
import { WordDescription } from '../../flavour-vo/WordDescription';
import { WordID } from '../../flavour-vo/WordID';
import { WordEditLogCommand } from '../WordEditLogCommand';

describe('WordEditLogCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const word: Word = Word.from(
        WordID.of('2a24aea8-5d97-4ff6-8168-de30f59417e6'),
        WordDescription.of('word'),
        present<Meaning>(Meaning.from(
          MeaningID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
          MeaningDescription.of('meaning')
        ))
      );
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('7eefaff5-8081-4201-838e-699aaa5614b6');

      const wordEditLogCommand: WordEditLogCommand = WordEditLogCommand.getInstance(query);
      await wordEditLogCommand.create(word, flavourAccountID);

      expect(stub.withArgs(`INSERT INTO word_edit_logs VALUES (
      :wordID,
      :flavourAccountID,
      :word,
      UTC_TIMESTAMP()
      );`, {
        wordID: '2a24aea8-5d97-4ff6-8168-de30f59417e6',
        flavourAccountID: '7eefaff5-8081-4201-838e-699aaa5614b6',
        word: 'word'
      }).called).toEqual(true);
    });
  });
});
