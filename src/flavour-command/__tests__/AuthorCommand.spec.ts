import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Author } from '../../flavour-entity/Author';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { AuthorHiragana } from '../../flavour-vo/AuthorHiragana';
import { AuthorID } from '../../flavour-vo/AuthorID';
import { AuthorName } from '../../flavour-vo/AuthorName';
import { AuthorRomaji } from '../../flavour-vo/AuthorRomaji';
import { AuthorCommand } from '../AuthorCommand';

describe('AuthorCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const author: Author = Author.from(
        AuthorID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        AuthorName.of('author'),
        AuthorHiragana.of('ちょしゃ'),
        AuthorRomaji.of('chosha')
      );

      const authorCommand: AuthorCommand = AuthorCommand.getInstance(query);
      await authorCommand.create(author);

      expect(stub.withArgs(`INSERT INTO authors VALUES (
      :authorID
      :name,
      :hiragana,
      :romaji
      );`, {
        authorID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        name: 'author',
        hiragana: 'ちょしゃ',
        romaji: 'chosha'
      }).called).toEqual(true);
    });
  });

  describe('update', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const author: Author = Author.from(
        AuthorID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        AuthorName.of('author'),
        AuthorHiragana.of('ちょしゃ'),
        AuthorRomaji.of('chosha')
      );

      const authorCommand: AuthorCommand = AuthorCommand.getInstance(query);
      await authorCommand.update(author);

      expect(stub.withArgs(`UPDATE authors SET
      name = :name,
      hiragana = :hiragana,
      romaji = :romaji
      WHERE author_id = :authorID;`, {
        authorID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        name: 'author',
        hiragana: 'ちょしゃ',
        romaji: 'chosha'
      }).called).toEqual(true);
    });
  });

  describe('delete', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const authorID: AuthorID = AuthorID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const authorCommand: AuthorCommand = AuthorCommand.getInstance(query);
      await authorCommand.delete(authorID);

      expect(stub.withArgs(`DELETE FROM authors
      WHERE author_id = :authorID;`, {
        authorID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });
});
