import { LanguageCommand } from '../flavour-command/LanguageCommand';
import { Languages } from '../flavour-entity/collection/Languages';
import { LanguageJSON, LanguageRow } from '../flavour-entity/Language';
import { flavourMySQL } from '../flavour-infrastructure/FlavourMySQL';
import { flavourRedis } from '../flavour-infrastructure/FlavourRedis';

const languageCommand: LanguageCommand = LanguageCommand.getInstance();

const REDIS_KEY: string = 'LANGUAGES';

export class LanguageQuery {
  private static instance: LanguageQuery = new LanguageQuery();

  public static getInstance(): LanguageQuery {
    return LanguageQuery.instance;
  }

  private constructor() {
  }

  public async all(): Promise<Languages> {
    const languageString: string | null = await flavourRedis.getString().get(REDIS_KEY);

    if (languageString !== null) {
      const languageJSONs: Array<LanguageJSON> = JSON.parse(languageString);
      return Languages.fromJSON(languageJSONs);
    }

    const query: string = `SELECT
      R1.language_id AS languageID,
      R1.name,
      R1.iso639
      FROM languages R1;`;

    const languageRows: Array<LanguageRow> = await flavourMySQL.execute(query);
    const languages: Languages = Languages.fromRow(languageRows);

    await languageCommand.insertAll(languages);

    return languages;
  }
}
