import { MeaningDescription } from '../flavour-vo/MeaningDescription';
import { MeaningID } from '../flavour-vo/MeaningID';
import { Entity } from './Entity';

export type MeaningJSON = {
  meaningID: string;
  meaning: string;
};

export type MeaningRow = {
  meaningID: string;
  meaning: string;
};

export class Meaning extends Entity<MeaningID> {
  private meaningID: MeaningID;
  private meaning: MeaningDescription;

  public static from(meaningID: MeaningID, meaning: MeaningDescription): Meaning {
    return new Meaning(meaningID, meaning);
  }

  public static fromJSON(json: MeaningJSON): Meaning {
    const {
      meaningID,
      meaning
    } = json;

    return Meaning.from(MeaningID.of(meaningID), MeaningDescription.of(meaning));
  }

  public static fromRow(row: MeaningRow): Meaning {
    const {
      meaningID,
      meaning
    } = row;

    return Meaning.from(MeaningID.of(meaningID), MeaningDescription.of(meaning));
  }

  private constructor(meaningID: MeaningID, meaning: MeaningDescription) {
    super();
    this.meaningID = meaningID;
    this.meaning = meaning;
  }

  public getMeaningID(): MeaningID {
    return this.meaningID;
  }

  public getMeaning(): MeaningDescription {
    return this.meaning;
  }

  public getIdentifier(): MeaningID {
    return this.meaningID;
  }

  public copy(): Meaning {
    const {
      meaningID,
      meaning
    } = this;

    return new Meaning(meaningID, meaning);
  }

  public toJSON(): MeaningJSON {
    const {
      meaningID,
      meaning
    } = this;

    return {
      meaningID: meaningID.get(),
      meaning: meaning.get()
    };
  }

  public toString(): string {
    const {
      meaningID,
      meaning
    } = this;

    return `${meaningID.toString()} ${meaning.toString()}`;
  }
}
