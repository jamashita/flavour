import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import { FlavourAccount, FlavourAccountJSON } from '../flavour-entity/FlavourAccount';
import { AuthenticationInteractor } from '../flavour-interactor/AuthenticationInteractor';

const authenticationInteractor: AuthenticationInteractor = AuthenticationInteractor.getInstance();

passport.use(new LocalStrategy(
  {
    usernameField: 'account',
    passwordField: 'password',
    session: true
  },
  authenticationInteractor.review
));

passport.serializeUser<FlavourAccount, FlavourAccountJSON>((account: FlavourAccount, done: (err: any, json: FlavourAccountJSON) => void): void => {
  done(null, account.toJSON());
});

passport.deserializeUser<FlavourAccount, FlavourAccountJSON>((json: FlavourAccountJSON, done: (err: any, account: FlavourAccount) => void): void => {
  done(null, FlavourAccount.fromJSON(json));
});
