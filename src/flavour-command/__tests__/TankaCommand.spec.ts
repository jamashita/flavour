import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Author } from '../../flavour-entity/Author';
import { Words } from '../../flavour-entity/collection/Words';
import { Meaning } from '../../flavour-entity/Meaning';
import { Tanka } from '../../flavour-entity/Tanka';
import { Translation } from '../../flavour-entity/Translation';
import { Word } from '../../flavour-entity/Word';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { empty } from '../../flavour-general/Optional/Empty';
import { present } from '../../flavour-general/Optional/Present';
import { AuthorHiragana } from '../../flavour-vo/AuthorHiragana';
import { AuthorID } from '../../flavour-vo/AuthorID';
import { AuthorName } from '../../flavour-vo/AuthorName';
import { AuthorRomaji } from '../../flavour-vo/AuthorRomaji';
import { TankaDescription } from '../../flavour-vo/TankaDescription';
import { TankaHiragana } from '../../flavour-vo/TankaHiragana';
import { TankaID } from '../../flavour-vo/TankaID';
import { TankaRomaji } from '../../flavour-vo/TankaRomaji';
import { TranslationDescription } from '../../flavour-vo/TranslationDescription';
import { TranslationID } from '../../flavour-vo/TranslationID';
import { WordDescription } from '../../flavour-vo/WordDescription';
import { WordID } from '../../flavour-vo/WordID';
import { TankaCommand } from '../TankaCommand';

describe('TankaCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const tanka: Tanka = Tanka.from(
        TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        Author.from(
          AuthorID.of('90d73259-1bc0-41e5-b2d8-c237915f5276'),
          AuthorName.of('author'),
          AuthorHiragana.of('author hiragana'),
          AuthorRomaji.of('author romaji')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('tanka hiragana'),
        TankaRomaji.of('tanka romaji'),
        present<Translation>(Translation.from(
          TranslationID.of('2a24aea8-5d97-4ff6-8168-de30f59417e6'),
          TranslationDescription.of('translation')
        )),
        Words.from([
          Word.from(
            WordID.of('377db089-13ac-4436-ac9f-01192b815c4a'),
            WordDescription.of('word'),
            empty<Meaning>()
          )
        ])
      );

      const tankaCommand: TankaCommand = TankaCommand.getInstance(query);
      await tankaCommand.create(tanka);

      expect(stub.withArgs(`INSERT INTO tankas VALUES (
      :tankaID,
      :authorID,
      :tanka,
      :hiragana,
      :romaji
      );`, {
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        authorID: '90d73259-1bc0-41e5-b2d8-c237915f5276',
        tanka: 'tanka',
        hiragana: 'tanka hiragana',
        romaji: 'tanka romaji'
      }).called).toEqual(true);
    });
  });

  describe('update', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const tanka: Tanka = Tanka.from(
        TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        Author.from(
          AuthorID.of('90d73259-1bc0-41e5-b2d8-c237915f5276'),
          AuthorName.of('author'),
          AuthorHiragana.of('author hiragana'),
          AuthorRomaji.of('author romaji')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('tanka hiragana'),
        TankaRomaji.of('tanka romaji'),
        present<Translation>(Translation.from(
          TranslationID.of('2a24aea8-5d97-4ff6-8168-de30f59417e6'),
          TranslationDescription.of('translation')
        )),
        Words.from([
          Word.from(
            WordID.of('377db089-13ac-4436-ac9f-01192b815c4a'),
            WordDescription.of('word'),
            empty<Meaning>()
          )
        ])
      );

      const tankaCommand: TankaCommand = TankaCommand.getInstance(query);
      await tankaCommand.update(tanka);

      expect(stub.withArgs(`UPDATE tankas SET
      author_id = :authorID,
      tanka = :tanka,
      hiragana = :hiragana,
      romaji = :romaji
      WHERE tanka_id = :tankaID;`, {
        authorID: '90d73259-1bc0-41e5-b2d8-c237915f5276',
        tanka: 'tanka',
        hiragana: 'tanka hiragana',
        romaji: 'tanka romaji',
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });

  describe('delete', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const tankaID: TankaID = TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const tankaCommand: TankaCommand = TankaCommand.getInstance(query);
      await tankaCommand.delete(tankaID);

      expect(stub.withArgs(`DELETE FROM tankas
      WHERE tanka_id = :tankaID;`, {
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });
});
