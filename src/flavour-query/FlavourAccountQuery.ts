import { FlavourAccount, FlavourAccountRow } from '../flavour-entity/FlavourAccount';
import { NoSuchElementError } from '../flavour-error/NoSuchElementError';
import { flavourMySQL } from '../flavour-infrastructure/FlavourMySQL';
import { AccountName } from '../flavour-vo/AccountName';
import { FlavourAccountID } from '../flavour-vo/FlavourAccountID';

export type FlavourAccountHash = {
  flavourAccount: FlavourAccount;
  hash: string;
  active: boolean;
};

export class FlavourAccountQuery {
  private static instance: FlavourAccountQuery = new FlavourAccountQuery();

  public static getInstance(): FlavourAccountQuery {
    return FlavourAccountQuery.instance;
  }

  private constructor() {
  }

  public async findByAccount(account: AccountName): Promise<FlavourAccountHash> {
    const query: string = `SELECT
      R1.flavour_account_id AS flavourAccountID,
      R1.account,
      R2.language_id AS languageID,
      R2.name AS languageName,
      R2.iso639,
      R1.hash,
      R1.active
      FROM flavour_accounts R1
      INNER JOIN languages R2
      USING(language_id)
      WHERE R1.account = :account
      AND R1.active = true;`;

    const flavourAccountRows: Array<FlavourAccountRow> = await flavourMySQL.execute(query, {
      account: account.get()
    });

    if (flavourAccountRows.length === 0) {
      throw new NoSuchElementError(account.get());
    }

    const flavourAccountRow: FlavourAccountRow = flavourAccountRows[0];

    return {
      flavourAccount: FlavourAccount.fromRow(flavourAccountRow),
      hash: flavourAccountRow.hash,
      active: Boolean(flavourAccountRow.active)
    };
  }

  public async find(flavourAccountID: FlavourAccountID): Promise<FlavourAccountHash> {
    const query: string = `SELECT
      R1.flavour_account_id AS flavourAccountID,
      R1.account,
      R2.language_id AS languageID,
      R2.name AS languageName,
      R2.iso639,
      R1.hash,
      R1.active
      FROM flavour_accounts R1
      INNER JOIN languages R2
      USING(language_id)
      WHERE R1.flavour_account_id = :flavourAccountID;`;

    const flavourAccountRows: Array<FlavourAccountRow> = await flavourMySQL.execute(query, {
      flavourAccountID: flavourAccountID.get()
    });

    if (flavourAccountRows.length === 0) {
      throw new NoSuchElementError(flavourAccountID.get());
    }

    const flavourAccountRow: FlavourAccountRow = flavourAccountRows[0];

    return {
      flavourAccount: FlavourAccount.fromRow(flavourAccountRow),
      hash: flavourAccountRow.hash,
      active: Boolean(flavourAccountRow.active)
    };
  }
}
