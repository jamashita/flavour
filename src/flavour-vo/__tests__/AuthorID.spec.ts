import 'jest';
import { AuthorID } from '../AuthorID';

describe('AuthorID', () => {
  describe('equals', () => {
    it('returns true if the property is the same', () => {
      const authorID1: AuthorID = AuthorID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');
      const authorID2: AuthorID = AuthorID.of('1f1600a8-803b-49a9-b283-e167c88523f7');
      const authorID3: AuthorID = AuthorID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');

      expect(authorID1.equals(authorID1)).toEqual(true);
      expect(authorID1.equals(authorID2)).toEqual(false);
      expect(authorID1.equals(authorID3)).toEqual(true);
    });
  });
});
