import { ValueObject } from './ValueObject';

export class AuthorName extends ValueObject {
  private name: string;

  public static of(name: string): AuthorName {
    return new AuthorName(name);
  }

  private constructor(name: string) {
    super();
    this.name = name;
  }

  public get(): string {
    return this.name;
  }

  public equals(other: AuthorName): boolean {
    if (this === other) {
      return true;
    }
    if (this.name === other.get()) {
      return true;
    }

    return false;
  }

  public toString(): string {
    return this.name;
  }
}
