import { IQuery } from '../flavour-general/MySQL/IQuery';
import { TranslationID } from '../flavour-vo/TranslationID';

export class PendingTranslationCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): PendingTranslationCommand {
    return new PendingTranslationCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(translationID: TranslationID): Promise<any> {
    const query: string = `INSERT INTO pending_translations VALUES (
      :translationID,
      0
      );`;

    return this.query.execute(query, {
      translationID: translationID.get()
    });
  }

  public update(translationID: TranslationID, points: number): Promise<any> {
    const query: string = `UPDATE pending_translations SET
      points = :points
      WHERE translation_id = :translationID;`;

    return this.query.execute(query, {
      points,
      translationID: translationID.get()
    });
  }

  public delete(translationID: TranslationID): Promise<any> {
    const query: string = `DELETE FROM pending_translations
      WHERE translation_id = :translationID;`;

    return this.query.execute(query, {
      translationID: translationID.get()
    });
  }
}
