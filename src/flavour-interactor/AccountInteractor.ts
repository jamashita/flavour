import log4js from 'log4js';
import { FlavourAccount } from '../flavour-entity/FlavourAccount';
import { AccountAlreadyInUseError } from '../flavour-error/AccountAlreadyInUseError';
import { NoSuchElementError } from '../flavour-error/NoSuchElementError';
import { Digest } from '../flavour-general/Digest';
import { ITransaction } from '../flavour-general/MySQL/ITransaction';
import { Random } from '../flavour-general/Random';
import { flavourMySQL } from '../flavour-infrastructure/FlavourMySQL';
import { FlavourAccountHash, FlavourAccountQuery } from '../flavour-query/FlavourAccountQuery';
import { FlavourAccountCreateTransaction } from '../flavour-transaction/FlavourAccountCreateTransaction';
import { FlavourAccountUpdateTransaction } from '../flavour-transaction/FlavourAccountUpdateTransaction';
import { AccountName } from '../flavour-vo/AccountName';
import { FlavourAccountID } from '../flavour-vo/FlavourAccountID';

const logger: log4js.Logger = log4js.getLogger();

const flavourAccountQuery: FlavourAccountQuery = FlavourAccountQuery.getInstance();

const PASSWORD_LENGTH: number = 20;

export class AccountInteractor {
  private static instance: AccountInteractor = new AccountInteractor();

  public static getInstance(): AccountInteractor {
    return AccountInteractor.instance;
  }

  private constructor() {
  }

  public async create(flavourAccount: FlavourAccount): Promise<string> {
    const password: string = Random.string(PASSWORD_LENGTH);
    const hash: string = await Digest.generate(password);

    try {
      const account: AccountName = flavourAccount.getAccount();

      // check flavour account whose account name is as same as new account name
      await flavourAccountQuery.findByAccount(account);
      logger.warn(`THIS ACCOUNT IS ALREADY IN USE: ${account}`);

      throw new AccountAlreadyInUseError(account);
    }
    catch (err) {
      if (err instanceof NoSuchElementError) {
        const flavourAccountCreateTransaction: ITransaction = FlavourAccountCreateTransaction.getInstance(flavourAccount, hash);

        await flavourAccountCreateTransaction.with(flavourMySQL);

        return password;
      }

      throw err;
    }
  }

  public async update(flavourAccount: FlavourAccount): Promise<any> {
    try {
      const account: AccountName = flavourAccount.getAccount();

      // check flavour account whose account name is as same as new account name
      await flavourAccountQuery.findByAccount(account);
      logger.warn(`THIS ACCOUNT IS ALREADY IN USE: ${account}`);

      throw new AccountAlreadyInUseError(account);
    }
    catch (err) {
      if (err instanceof NoSuchElementError) {
        const flavourAccountHash: FlavourAccountHash = await flavourAccountQuery.find(flavourAccount.getFlavourAccountID());

        const {
          hash,
          active
        } = flavourAccountHash;

        const flavourAccountUpdateTransaction: ITransaction = FlavourAccountUpdateTransaction.getInstance(flavourAccount, hash, active);

        return flavourAccountUpdateTransaction.with(flavourMySQL);
      }

      throw err;
    }
  }

  public async regeneratePassword(flavourAccountID: FlavourAccountID): Promise<string> {
    const password: string = Random.string(PASSWORD_LENGTH);
    const hash: string = await Digest.generate(password);

    const flavourAccountHash: FlavourAccountHash = await flavourAccountQuery.find(flavourAccountID);

    const {
      flavourAccount,
      active
    } = flavourAccountHash;

    const flavourAccountUpdateTransaction: ITransaction = FlavourAccountUpdateTransaction.getInstance(flavourAccount, hash, active);

    await flavourAccountUpdateTransaction.with(flavourMySQL);

    return password;
  }

  public async deactivate(flavourAccountID: FlavourAccountID): Promise<any> {
    const flavourAccountHash: FlavourAccountHash = await flavourAccountQuery.find(flavourAccountID);

    const {
      flavourAccount,
      hash
    } = flavourAccountHash;

    const flavourAccountUpdateTransaction: ITransaction = FlavourAccountUpdateTransaction.getInstance(flavourAccount, hash, false);

    return flavourAccountUpdateTransaction.with(flavourMySQL);
  }

  public async activate(flavourAccountID: FlavourAccountID): Promise<any> {
    const flavourAccountHash: FlavourAccountHash = await flavourAccountQuery.find(flavourAccountID);

    const {
      flavourAccount,
      hash
    } = flavourAccountHash;

    const flavourAccountUpdateTransaction: ITransaction = FlavourAccountUpdateTransaction.getInstance(flavourAccount, hash, true);

    return flavourAccountUpdateTransaction.with(flavourMySQL);
  }
}
