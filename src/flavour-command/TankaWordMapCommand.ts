import { IQuery } from '../flavour-general/MySQL/IQuery';
import { TankaID } from '../flavour-vo/TankaID';
import { WordID } from '../flavour-vo/WordID';

export class TankaWordMapCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): TankaWordMapCommand {
    return new TankaWordMapCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(tankaID: TankaID, wordID: WordID): Promise<any> {
    const query: string = `INSERT INTO tanka_word_map VALUES (
      :tankaID,
      :wordID
      );`;

    return this.query.execute(query, {
      tankaID: tankaID.get(),
      wordID: wordID.get()
    });
  }

  public deleteByTankaID(tankaID: TankaID): Promise<any> {
    const query: string = `DELETE FROM tanka_word_map
      WHERE tanka_id = :tankaID;`;

    return this.query.execute(query, {
      tankaID: tankaID.get()
    });
  }

  public deleteByWordID(wordID: WordID): Promise<any> {
    const query: string = `DELETE FROM tanka_word_map
      WHERE word_id = :wordID;`;

    return this.query.execute(query, {
      wordID: wordID.get()
    });
  }
}
