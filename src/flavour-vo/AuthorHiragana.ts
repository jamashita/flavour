import { ValueObject } from './ValueObject';

export class AuthorHiragana extends ValueObject {
  private hiragana: string;

  public static of(hiragana: string): AuthorHiragana {
    return new AuthorHiragana(hiragana);
  }

  private constructor(hiragana: string) {
    super();
    this.hiragana = hiragana;
  }

  public get(): string {
    return this.hiragana;
  }

  public equals(other: AuthorHiragana): boolean {
    if (this === other) {
      return true;
    }
    if (this.hiragana === other.get()) {
      return true;
    }

    return false;
  }

  public toString(): string {
    return this.hiragana;
  }
}
