import bodyParser from 'body-parser';
import express from 'express';
import { BAD_REQUEST, CREATED, INTERNAL_SERVER_ERROR, OK } from 'http-status';
import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import supertest from 'supertest';
import { AuthorInteractor } from '../../../flavour-interactor/AuthorInteractor';
import { AuthorController } from '../AuthorController';

describe('AuthorController', () => {
  describe('POST /', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      AuthorInteractor.prototype.create = stub;
      stub.resolves();

      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).post('/')
        .send({
          authorID: '01ff40aa-b4a1-473d-94f9-9993440c0950',
          name: '著者',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        });

      expect(response.status).toEqual(CREATED);
    });

    it('authorID is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).post('/')
        .send({
          name: '著者',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('name is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).post('/')
        .send({
          authorID: '01ff40aa-b4a1-473d-94f9-9993440c0950',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('hiragana is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).post('/')
        .send({
          authorID: '01ff40aa-b4a1-473d-94f9-9993440c0950',
          name: '著者',
          romaji: 'chosha'
        });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('romaji is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).post('/')
        .send({
          authorID: '01ff40aa-b4a1-473d-94f9-9993440c0950',
          name: '著者',
          hiragana: 'ちょしゃ'
        });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('thrown error', async () => {
      const stub: SinonStub = sinon.stub();
      AuthorInteractor.prototype.create = stub;
      stub.rejects(new Error());

      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).post('/')
        .send({
          authorID: '01ff40aa-b4a1-473d-94f9-9993440c0950',
          name: '著者',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        });

      expect(response.status).toEqual(INTERNAL_SERVER_ERROR);
    });
  });

  describe('PUT /', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      AuthorInteractor.prototype.update = stub;
      stub.resolves();

      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).put('/')
        .send({
          authorID: '01ff40aa-b4a1-473d-94f9-9993440c0950',
          name: '著者',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        });

      expect(response.status).toEqual(OK);
    });

    it('authorID is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).put('/')
        .send({
          name: '著者',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('name is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).put('/')
        .send({
          authorID: '01ff40aa-b4a1-473d-94f9-9993440c0950',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('hiragana is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).put('/')
        .send({
          authorID: '01ff40aa-b4a1-473d-94f9-9993440c0950',
          name: '著者',
          romaji: 'chosha'
        });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('romaji is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).put('/')
        .send({
          authorID: '01ff40aa-b4a1-473d-94f9-9993440c0950',
          name: '著者',
          hiragana: 'ちょしゃ'
        });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('thrown error', async () => {
      const stub: SinonStub = sinon.stub();
      AuthorInteractor.prototype.update = stub;
      stub.rejects(new Error());

      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).put('/')
        .send({
          authorID: '01ff40aa-b4a1-473d-94f9-9993440c0950',
          name: '著者',
          hiragana: 'ちょしゃ',
          romaji: 'romaji'
        });

      expect(response.status).toEqual(INTERNAL_SERVER_ERROR);
    });
  });

  describe('/:authorID([0-9a-f\-]{36})', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      AuthorInteractor.prototype.delete = stub;
      stub.resolves();

      const app: express.Express = express();
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).delete('/01ff40aa-b4a1-473d-94f9-9993440c0950');

      expect(response.status).toEqual(OK);
    });

    it('thrown error', async () => {
      const stub: SinonStub = sinon.stub();
      AuthorInteractor.prototype.delete = stub;
      stub.rejects(new Error());

      const app: express.Express = express();
      app.use('/', AuthorController);
      const response: supertest.Response = await supertest(app).delete('/01ff40aa-b4a1-473d-94f9-9993440c0950');

      expect(response.status).toEqual(INTERNAL_SERVER_ERROR);
    });
  });
});
