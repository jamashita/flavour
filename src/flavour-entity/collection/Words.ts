import { NoSuchElementError } from '../../flavour-error/NoSuchElementError';
import { JSONable } from '../../flavour-general/JSONable';
import { Word, WordJSON, WordRow } from '../Word';

export class Words implements JSONable {
  private words: Array<Word>;

  public static from(words: Array<Word>): Words {
    return new Words(words);
  }

  public static fromJSON(json: Array<WordJSON>): Words {
    return Words.from(json.map<Word>((word: WordJSON): Word => {
      return Word.fromJSON(word);
    }));
  }

  public static fromRow(rows: Array<WordRow>): Words {
    return Words.from(rows.map<Word>((word: WordRow): Word => {
      return Word.fromRow(word);
    }));
  }

  private constructor(words: Array<Word>) {
    this.words = words;
  }

  public get(index: number): Word {
    const word: Word | undefined = this.words[index];

    if (word === undefined) {
      throw new NoSuchElementError(index.toString());
    }

    return word;
  }

  public add(word: Word): Words {
    return new Words([
      ...this.words,
      word
    ]);
  }

  public length(): number {
    return this.words.length;
  }

  public equals(other: Words): boolean {
    if (this === other) {
      return true;
    }

    const length: number = this.words.length;

    if (length !== other.length()) {
      return false;
    }

    for (let i: number = 0; i < length; i++) {
      if (!this.words[i].equals(other.get(i))) {
        return false;
      }
    }

    return true;
  }

  public copy(): Words {
    return Words.from(this.words.map<Word>((word: Word): Word => {
      return word.copy();
    }));
  }

  public toJSON(): Array<WordJSON> {
    return this.words.map<WordJSON>((word: Word): WordJSON => {
      return word.toJSON();
    });
  }

  public toString(): string {
    return this.words.map<string>((word: Word): string => {
      return word.toString();
    }).join(', ');
  }
}
