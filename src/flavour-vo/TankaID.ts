import { RuntimeError } from '../flavour-error/RuntimeError';
import { UUID } from '../flavour-general/UUID';
import { ValueObject } from './ValueObject';

export class TankaID extends ValueObject {
  private id: string;

  public static of(id: string): TankaID {
    if (id.length === UUID.size()) {
      return new TankaID(id);
    }

    throw new RuntimeError(`TankaID requires ${UUID.size()} LENGTH`);
  }

  private constructor(id: string) {
    super();
    this.id = id;
  }

  public get(): string {
    return this.id;
  }

  public equals(other: TankaID): boolean {
    if (this === other) {
      return true;
    }
    if (this.id === other.get()) {
      return true;
    }

    return false;
  }

  public toString(): string {
    return this.id;
  }
}
