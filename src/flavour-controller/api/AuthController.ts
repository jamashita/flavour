import express from 'express';
import { OK } from 'http-status';
import passport from 'passport';

const router: express.Router = express.Router();

router.post('/', passport.authenticate('local'), (req: express.Request, res: express.Response): any => {
  res.status(OK).send(req.account.toJSON());
});

export const AuthController: express.Router = router;
