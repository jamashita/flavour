import 'jest';
import { empty } from '../../flavour-general/Optional/Empty';
import { Optional } from '../../flavour-general/Optional/Optional';
import { present } from '../../flavour-general/Optional/Present';
import { AuthorHiragana } from '../../flavour-vo/AuthorHiragana';
import { AuthorID } from '../../flavour-vo/AuthorID';
import { AuthorName } from '../../flavour-vo/AuthorName';
import { AuthorRomaji } from '../../flavour-vo/AuthorRomaji';
import { TankaDescription } from '../../flavour-vo/TankaDescription';
import { TankaHiragana } from '../../flavour-vo/TankaHiragana';
import { TankaID } from '../../flavour-vo/TankaID';
import { TankaRomaji } from '../../flavour-vo/TankaRomaji';
import { TranslationDescription } from '../../flavour-vo/TranslationDescription';
import { TranslationID } from '../../flavour-vo/TranslationID';
import { WordDescription } from '../../flavour-vo/WordDescription';
import { WordID } from '../../flavour-vo/WordID';
import { Author } from '../Author';
import { Words } from '../collection/Words';
import { Meaning } from '../Meaning';
import { Tanka, TankaJSON, TankaRow } from '../Tanka';
import { Translation } from '../Translation';
import { Word } from '../Word';

describe('Tanka', () => {
  describe('equals', () => {
    it('returns true if the id equals', () => {
      const tanka1: Tanka = Tanka.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('たんか'), TankaRomaji.of('tanka'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))), Words.from([]));
      const tanka2: Tanka = Tanka.from(TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('たんか'), TankaRomaji.of('tanka'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))), Words.from([]));
      const tanka3: Tanka = Tanka.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('たんか'), TankaRomaji.of('tanka'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))), Words.from([]));
      const tanka4: Tanka = Tanka.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('haiku'), TankaHiragana.of('たんか'), TankaRomaji.of('tanka'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))), Words.from([]));
      const tanka5: Tanka = Tanka.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('はいく'), TankaRomaji.of('tanka'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))), Words.from([]));
      const tanka6: Tanka = Tanka.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('たんか'), TankaRomaji.of('haiku'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))), Words.from([]));
      const tanka7: Tanka = Tanka.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('たんか'), TankaRomaji.of('tanka'), empty<Translation>(), Words.from([]));
      const tanka8: Tanka = Tanka.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('たんか'), TankaRomaji.of('tanka'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))), Words.from([Word.from(WordID.of('4ec71344-a37d-4dba-a90f-9aa93545ca0e'), WordDescription.of('word'), empty<Meaning>())]));

      expect(tanka1.equals(tanka1)).toEqual(true);
      expect(tanka1.equals(tanka2)).toEqual(false);
      expect(tanka1.equals(tanka3)).toEqual(true);
      expect(tanka1.equals(tanka4)).toEqual(true);
      expect(tanka1.equals(tanka5)).toEqual(true);
      expect(tanka1.equals(tanka6)).toEqual(true);
      expect(tanka1.equals(tanka7)).toEqual(true);
      expect(tanka1.equals(tanka8)).toEqual(true);
    });
  });

  describe('copy', () => {
    it('translation is empty', () => {
      const tankaID: TankaID = TankaID.of('07ca3964-f97e-4120-9888-64001682e301');
      const author: Author = Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha'));
      const tankaString: TankaDescription = TankaDescription.of('tanka');
      const hiragana: TankaHiragana = TankaHiragana.of('ひらがな');
      const romaji: TankaRomaji = TankaRomaji.of('romaji');
      const translation: Optional<Translation> = empty<Translation>();
      const words: Words = Words.from([]);

      const tanka: Tanka = Tanka.from(tankaID, author, tankaString, hiragana, romaji, translation, words);
      const copied: Tanka = tanka.copy();

      expect(copied.getTankaID()).toEqual(tankaID);
      expect(copied.getAuthor()).toEqual(author);
      expect(copied.getTanka()).toEqual(tankaString);
      expect(copied.getHiragana()).toEqual(hiragana);
      expect(copied.getRomaji()).toEqual(romaji);
      expect(copied.getTranslation()).toEqual(translation);
      expect(copied.getWords()).toEqual(words);
    });

    it('translation is present', () => {
      const tankaID: TankaID = TankaID.of('07ca3964-f97e-4120-9888-64001682e301');
      const author: Author = Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha'));
      const tankaString: TankaDescription = TankaDescription.of('tanka');
      const hiragana: TankaHiragana = TankaHiragana.of('ひらがな');
      const romaji: TankaRomaji = TankaRomaji.of('romaji');
      const translation: Optional<Translation> = present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation')));
      const words: Words = Words.from([]);

      const tanka: Tanka = Tanka.from(tankaID, author, tankaString, hiragana, romaji, translation, words);
      const copied: Tanka = tanka.copy();

      expect(copied.getTankaID()).toEqual(tankaID);
      expect(copied.getAuthor()).toEqual(author);
      expect(copied.getTanka()).toEqual(tankaString);
      expect(copied.getHiragana()).toEqual(hiragana);
      expect(copied.getRomaji()).toEqual(romaji);
      expect(copied.getTranslation()).toEqual(translation);
      expect(copied.getWords()).toEqual(words);
    });
  });

  describe('toJSON', () => {
    it('translation is empty', () => {
      const tankaID: TankaID = TankaID.of('07ca3964-f97e-4120-9888-64001682e301');
      const author: Author = Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha'));
      const tankaString: TankaDescription = TankaDescription.of('tanka');
      const hiragana: TankaHiragana = TankaHiragana.of('ひらがな');
      const romaji: TankaRomaji = TankaRomaji.of('romaji');
      const translation: Optional<Translation> = empty<Translation>();
      const words: Words = Words.from([]);

      const tanka: Tanka = Tanka.from(tankaID, author, tankaString, hiragana, romaji, translation, words);

      expect(tanka.toJSON()).toEqual({
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        author: {
          authorID: '731ec97d-9dc0-4691-9609-32703890c1cf',
          name: 'author',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        },
        tanka: 'tanka',
        hiragana: 'ひらがな',
        romaji: 'romaji',
        translation: null,
        words: []
      });
    });

    it('translation is present', () => {
      const tankaID: TankaID = TankaID.of('07ca3964-f97e-4120-9888-64001682e301');
      const author: Author = Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha'));
      const tankaString: TankaDescription = TankaDescription.of('tanka');
      const hiragana: TankaHiragana = TankaHiragana.of('ひらがな');
      const romaji: TankaRomaji = TankaRomaji.of('romaji');
      const translation: Optional<Translation> = present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation')));
      const words: Words = Words.from([]);

      const tanka: Tanka = Tanka.from(tankaID, author, tankaString, hiragana, romaji, translation, words);

      expect(tanka.toJSON()).toEqual({
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        author: {
          authorID: '731ec97d-9dc0-4691-9609-32703890c1cf',
          name: 'author',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        },
        tanka: 'tanka',
        hiragana: 'ひらがな',
        romaji: 'romaji',
        translation: {
          translationID: '985e107a-e61e-4122-be8d-c4d2dbd22b33',
          translation: 'translation'
        },
        words: []
      });
    });
  });

  describe('from', () => {
    it('normal case', () => {
      const tankaID: TankaID = TankaID.of('07ca3964-f97e-4120-9888-64001682e301');
      const author: Author = Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha'));
      const tankaString: TankaDescription = TankaDescription.of('tanka');
      const hiragana: TankaHiragana = TankaHiragana.of('ひらがな');
      const romaji: TankaRomaji = TankaRomaji.of('romaji');
      const translation: Optional<Translation> = present(Translation.from(TranslationID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'), TranslationDescription.of('translation')));
      const words: Words = Words.from([]);

      const tanka: Tanka = Tanka.from(tankaID, author, tankaString, hiragana, romaji, translation, words);

      expect(tanka.getTankaID()).toEqual(tankaID);
      expect(tanka.getAuthor()).toEqual(author);
      expect(tanka.getTanka()).toEqual(tankaString);
      expect(tanka.getHiragana()).toEqual(hiragana);
      expect(tanka.getRomaji()).toEqual(romaji);
      expect(tanka.getTranslation()).toEqual(translation);
      expect(tanka.getWords()).toEqual(words);
    });
  });

  describe('fromJSON', () => {
    it('translation is empty', () => {
      const json: TankaJSON = {
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        author: {
          authorID: '731ec97d-9dc0-4691-9609-32703890c1cf',
          name: 'author',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        },
        tanka: 'tanka',
        hiragana: 'ひらがな',
        romaji: 'romaji',
        translation: null,
        words: []
      };

      const tanka: Tanka = Tanka.fromJSON(json);

      expect(tanka.getTankaID().get()).toEqual(json.tankaID);
      expect(tanka.getAuthor().getAuthorID().get()).toEqual(json.author.authorID);
      expect(tanka.getAuthor().getName().get()).toEqual(json.author.name);
      expect(tanka.getAuthor().getHiragana().get()).toEqual(json.author.hiragana);
      expect(tanka.getAuthor().getRomaji().get()).toEqual(json.author.romaji);
      expect(tanka.getTanka().get()).toEqual(json.tanka);
      expect(tanka.getHiragana().get()).toEqual(json.hiragana);
      expect(tanka.getRomaji().get()).toEqual(json.romaji);
      expect(tanka.getTranslation().isPresent()).toEqual(false);
    });

    it('translation is present', () => {
      const json: TankaJSON = {
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        author: {
          authorID: '731ec97d-9dc0-4691-9609-32703890c1cf',
          name: 'author',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        },
        tanka: 'tanka',
        hiragana: 'ひらがな',
        romaji: 'romaji',
        translation: {
          translationID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
          translation: 'translation'
        },
        words: []
      };

      const tanka: Tanka = Tanka.fromJSON(json);

      expect(tanka.getTankaID().get()).toEqual(json.tankaID);
      expect(tanka.getAuthor().getAuthorID().get()).toEqual(json.author.authorID);
      expect(tanka.getAuthor().getName().get()).toEqual(json.author.name);
      expect(tanka.getAuthor().getHiragana().get()).toEqual(json.author.hiragana);
      expect(tanka.getAuthor().getRomaji().get()).toEqual(json.author.romaji);
      expect(tanka.getTanka().get()).toEqual(json.tanka);
      expect(tanka.getHiragana().get()).toEqual(json.hiragana);
      expect(tanka.getRomaji().get()).toEqual(json.romaji);
      expect(tanka.getTranslation().isPresent()).toEqual(true);
      // @ts-ignore
      expect(tanka.getTranslation().get().getTranslationID().get()).toEqual(json.translation.translationID);
      // @ts-ignore
      expect(tanka.getTranslation().get().getTranslation().get()).toEqual(json.translation.translation);
    });
  });

  describe('fromRow', () => {
    it('translation is empty', () => {
      const row: TankaRow = {
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        authorID: '731ec97d-9dc0-4691-9609-32703890c1cf',
        author: 'author',
        authorHiragana: 'ちょしゃ',
        authorRomaji: 'chosha',
        tanka: 'tanka',
        hiragana: 'ひらがな',
        romaji: 'romaji',
        translationID: null,
        translation: null
      };
      const words: Words = Words.from([]);

      const tanka: Tanka = Tanka.fromRow(row, words);

      expect(tanka.getTankaID().get()).toEqual(row.tankaID);
      expect(tanka.getAuthor().getAuthorID().get()).toEqual(row.authorID);
      expect(tanka.getAuthor().getName().get()).toEqual(row.author);
      expect(tanka.getAuthor().getHiragana().get()).toEqual(row.authorHiragana);
      expect(tanka.getAuthor().getRomaji().get()).toEqual(row.authorRomaji);
      expect(tanka.getTanka().get()).toEqual(row.tanka);
      expect(tanka.getHiragana().get()).toEqual(row.hiragana);
      expect(tanka.getRomaji().get()).toEqual(row.romaji);
      expect(tanka.getTranslation().isPresent()).toEqual(false);
    });


    it('translation is present', () => {
      const row: TankaRow = {
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        authorID: '731ec97d-9dc0-4691-9609-32703890c1cf',
        author: 'author',
        authorHiragana: 'ちょしゃ',
        authorRomaji: 'chosha',
        tanka: 'tanka',
        hiragana: 'ひらがな',
        romaji: 'romaji',
        translationID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        translation: 'translation'
      };
      const words: Words = Words.from([]);

      const tanka: Tanka = Tanka.fromRow(row, words);

      expect(tanka.getTankaID().get()).toEqual(row.tankaID);
      expect(tanka.getAuthor().getAuthorID().get()).toEqual(row.authorID);
      expect(tanka.getAuthor().getName().get()).toEqual(row.author);
      expect(tanka.getAuthor().getHiragana().get()).toEqual(row.authorHiragana);
      expect(tanka.getAuthor().getRomaji().get()).toEqual(row.authorRomaji);
      expect(tanka.getTanka().get()).toEqual(row.tanka);
      expect(tanka.getHiragana().get()).toEqual(row.hiragana);
      expect(tanka.getRomaji().get()).toEqual(row.romaji);
      expect(tanka.getTranslation().isPresent()).toEqual(true);
      expect(tanka.getTranslation().get().getTranslationID().get()).toEqual(row.translationID);
      expect(tanka.getTranslation().get().getTranslation().get()).toEqual(row.translation);
    });
  });
});
