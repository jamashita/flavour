import { AccountName } from '../flavour-vo/AccountName';
import { FlavourAccountID } from '../flavour-vo/FlavourAccountID';
import { ISO639 } from '../flavour-vo/ISO639';
import { LanguageID } from '../flavour-vo/LanguageID';
import { LanguageName } from '../flavour-vo/LanguageName';
import { Entity } from './Entity';
import { Language, LanguageJSON } from './Language';

export type FlavourAccountJSON = {
  flavourAccountID: string;
  account: string;
  language: LanguageJSON;
};

export type FlavourAccountRow = {
  flavourAccountID: string;
  account: string;
  languageID: number;
  languageName: string;
  iso639: string;
  hash: string;
  active: number;
};

export class FlavourAccount extends Entity<FlavourAccountID> {
  private flavourAccountID: FlavourAccountID;
  private account: AccountName;
  private language: Language;

  public static from(flavourAccountID: FlavourAccountID, account: AccountName, language: Language): FlavourAccount {
    return new FlavourAccount(flavourAccountID, account, language);
  }

  public static fromJSON(json: FlavourAccountJSON): FlavourAccount {
    const {
      flavourAccountID,
      account,
      language
    } = json;

    return FlavourAccount.from(FlavourAccountID.of(flavourAccountID), AccountName.of(account),Language.fromJSON(language));
  }

  public static fromRow(row: FlavourAccountRow): FlavourAccount {
    const {
      flavourAccountID,
      account,
      languageID,
      languageName,
      iso639
    } = row;

    const language: Language = Language.from(LanguageID.of(languageID), LanguageName.of(languageName), ISO639.of(iso639));

    return FlavourAccount.from(FlavourAccountID.of(flavourAccountID), AccountName.of(account), language);
  }

  public static default(): FlavourAccount {
    return new FlavourAccount(FlavourAccountID.default(), AccountName.default(), Language.default());
  }

  private constructor(flavourAccountID: FlavourAccountID, account: AccountName, language: Language) {
    super();
    this.flavourAccountID = flavourAccountID;
    this.account = account;
    this.language = language;
  }

  public getFlavourAccountID(): FlavourAccountID {
    return this.flavourAccountID;
  }

  public getAccount(): AccountName {
    return this.account;
  }

  public getLanguage(): Language {
    return this.language;
  }

  public getIdentifier(): FlavourAccountID {
    return this.flavourAccountID;
  }

  public copy(): FlavourAccount {
    const {
      flavourAccountID,
      account,
      language
    } = this;

    return new FlavourAccount(flavourAccountID, account, language);
  }

  public toJSON(): FlavourAccountJSON {
    const {
      flavourAccountID,
      account,
      language
    } = this;

    return {
      flavourAccountID: flavourAccountID.get(),
      account: account.get(),
      language: language.toJSON()
    };
  }

  public toString(): string {
    const {
      flavourAccountID,
      account,
      language
    } = this;

    return `${flavourAccountID.toString()} ${account.toString()} ${language.toString()}`;
  }
}
