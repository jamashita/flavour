import config from 'config';
import IORedis from 'ioredis';
import { Redis } from '../flavour-general/Redis/Redis';

export const flavourRedis: Redis = new Redis(config.get<IORedis.RedisOptions>('redis'));
