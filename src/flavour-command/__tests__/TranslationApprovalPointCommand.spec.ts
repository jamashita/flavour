import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { FlavourAccountID } from '../../flavour-vo/FlavourAccountID';
import { LanguageID } from '../../flavour-vo/LanguageID';
import { TranslationApprovalPointCommand } from '../TranslationApprovalPointCommand';

describe('TranslationApprovalPointCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');
      const languageID: LanguageID = LanguageID.of(250);

      const translationApprovalPointCommand: TranslationApprovalPointCommand = TranslationApprovalPointCommand.getInstance(query);
      await translationApprovalPointCommand.create(flavourAccountID, languageID);

      expect(stub.withArgs(`INSERT INTO translation_approval_points VALUES(
      :flavourAccountID,
      :languageID,
      0
      );`, {
        flavourAccountID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        languageID: 250
      }).called).toEqual(true)
    });
  });

  describe('update', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');
      const languageID: LanguageID = LanguageID.of(250);

      const translationApprovalPointCommand: TranslationApprovalPointCommand = TranslationApprovalPointCommand.getInstance(query);
      await translationApprovalPointCommand.update(flavourAccountID, languageID, 2000);

      expect(stub.withArgs(`UPDATE translation_approval_points SET
      points = :points
      WHERE flavour_account_id = :flavourAccountID
      AND language_id = :languageID;`, {
        points: 2000,
        flavourAccountID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        languageID: 250
      }).called).toEqual(true)
    });
  });
});
