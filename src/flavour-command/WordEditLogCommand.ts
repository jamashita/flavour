import { Word } from '../flavour-entity/Word';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { FlavourAccountID } from '../flavour-vo/FlavourAccountID';

export class WordEditLogCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): WordEditLogCommand {
    return new WordEditLogCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(word: Word, flavourAccountID: FlavourAccountID): Promise<any> {
    const query: string = `INSERT INTO word_edit_logs VALUES (
      :wordID,
      :flavourAccountID,
      :word,
      UTC_TIMESTAMP()
      );`;

    return this.query.execute(query, {
      wordID: word.getWordID().get(),
      flavourAccountID: flavourAccountID.get(),
      word: word.getWord().get()
    });
  }
}
