import { FlavourAccountCommand } from '../flavour-command/FlavourAccountCommand';
import { FlavourAccount } from '../flavour-entity/FlavourAccount';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { ITransaction } from '../flavour-general/MySQL/ITransaction';

export class FlavourAccountUpdateTransaction implements ITransaction {
  private flavourAccount: FlavourAccount;
  private hash: string;
  private active: boolean;

  public static getInstance(flavourAccount: FlavourAccount, hash: string, active: boolean): FlavourAccountUpdateTransaction {
    return new FlavourAccountUpdateTransaction(flavourAccount, hash, active);
  }

  private constructor(flavourAccount: FlavourAccount, hash: string, active: boolean) {
    this.flavourAccount = flavourAccount;
    this.hash = hash;
    this.active = active;
  }

  public with(query: IQuery): Promise<any> {
    const flavourAccountCommand: FlavourAccountCommand = FlavourAccountCommand.getInstance(query);

    const {
      flavourAccount,
      hash,
      active
    } = this;

    return flavourAccountCommand.update(flavourAccount, hash, active);
  }
}
