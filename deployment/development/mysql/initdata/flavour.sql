# ************************************************************
# Sequel Pro SQL dump
# Version 5438
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.16)
# Database: flavour
# Generation Time: 2019-05-19 11:55:43 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table approved_tankas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `approved_tankas`;

CREATE TABLE `approved_tankas` (
  `tanka_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  UNIQUE KEY `tanka_id` (`tanka_id`),
  CONSTRAINT `approved_tankas_ibfk_1` FOREIGN KEY (`tanka_id`) REFERENCES `tankas` (`tanka_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table approved_translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `approved_translations`;

CREATE TABLE `approved_translations` (
  `translation_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  UNIQUE KEY `translation_id` (`translation_id`),
  CONSTRAINT `approved_translations_ibfk_1` FOREIGN KEY (`translation_id`) REFERENCES `translations` (`translation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table authors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `authors`;

CREATE TABLE `authors` (
  `author_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `hiragana` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `romaji` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`author_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table flavour_accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `flavour_accounts`;

CREATE TABLE `flavour_accounts` (
  `flavour_account_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `language_id` tinyint(3) unsigned NOT NULL,
  `account` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `hash` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `active` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`flavour_account_id`),
  UNIQUE KEY `account` (`account`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `flavour_accounts_ibfk_1` FOREIGN KEY (`language_id`) REFERENCES `languages` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

LOCK TABLES `flavour_accounts` WRITE;
/*!40000 ALTER TABLE `flavour_accounts` DISABLE KEYS */;

INSERT INTO `flavour_accounts` (`flavour_account_id`, `language_id`, `account`, `hash`, `active`)
VALUES
	(X'32333833303563622D393465392D343661612D393861382D326162303461363861336136',1,X'E697A5E69CACE8AA9E',X'2432622431342445454B766B31586A74414B63707256423033504F6F2E565077624F652E32475833347762334A3872595450414A317144496F686661',1),
	(X'34613564356565322D303030342D346530382D383965622D333535393235616534623634',2,X'456E676C697368',X'243262243134243159723935626B6343497938532E4A3262706D6F324F725A796377556B755A7671713162376259346F5A73573477534E4234556B4B',1);

/*!40000 ALTER TABLE `flavour_accounts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table languages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages` (
  `language_id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `iso639` char(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`language_id`),
  UNIQUE KEY `iso639` (`iso639`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;

INSERT INTO `languages` (`language_id`, `name`, `iso639`)
VALUES
	(1,X'E697A5E69CACE8AA9E',X'6A61'),
	(2,X'456E676C697368',X'656E');

/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table meaning_edit_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meaning_edit_logs`;

CREATE TABLE `meaning_edit_logs` (
  `meaning_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `flavour_account_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `meaning` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `when` datetime NOT NULL,
  KEY `meaning_id` (`meaning_id`,`when`),
  KEY `flavour_account_id` (`flavour_account_id`),
  CONSTRAINT `meaning_edit_logs_ibfk_1` FOREIGN KEY (`meaning_id`) REFERENCES `meanings` (`meaning_id`),
  CONSTRAINT `meaning_edit_logs_ibfk_2` FOREIGN KEY (`flavour_account_id`) REFERENCES `flavour_accounts` (`flavour_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table meanings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meanings`;

CREATE TABLE `meanings` (
  `meaning_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `word_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `language_id` tinyint(3) unsigned NOT NULL,
  `meaning` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`meaning_id`),
  UNIQUE KEY `word_id` (`word_id`,`language_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `meanings_ibfk_1` FOREIGN KEY (`word_id`) REFERENCES `words` (`word_id`),
  CONSTRAINT `meanings_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table pending_tankas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pending_tankas`;

CREATE TABLE `pending_tankas` (
  `tanka_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `approval_points` smallint(5) unsigned NOT NULL,
  UNIQUE KEY `tanka_id` (`tanka_id`),
  CONSTRAINT `pending_tankas_ibfk_1` FOREIGN KEY (`tanka_id`) REFERENCES `tankas` (`tanka_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table pending_translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pending_translations`;

CREATE TABLE `pending_translations` (
  `translation_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `approval_points` smallint(5) unsigned NOT NULL,
  UNIQUE KEY `translation_id` (`translation_id`),
  CONSTRAINT `pending_translations_ibfk_1` FOREIGN KEY (`translation_id`) REFERENCES `translations` (`translation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table tanka_approval_points
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tanka_approval_points`;

CREATE TABLE `tanka_approval_points` (
  `flavour_account_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `points` smallint(5) unsigned NOT NULL,
  UNIQUE KEY `flavour_account_id` (`flavour_account_id`),
  CONSTRAINT `tanka_approval_points_ibfk_1` FOREIGN KEY (`flavour_account_id`) REFERENCES `flavour_accounts` (`flavour_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table tanka_edit_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tanka_edit_logs`;

CREATE TABLE `tanka_edit_logs` (
  `tanka_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `flavour_account_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `author_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `tanka` char(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `hiragana` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `romaji` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `when` datetime NOT NULL,
  KEY `tanka_id` (`tanka_id`,`when`),
  KEY `flavour_account_id` (`flavour_account_id`),
  KEY `author_id` (`author_id`),
  CONSTRAINT `tanka_edit_logs_ibfk_1` FOREIGN KEY (`tanka_id`) REFERENCES `tankas` (`tanka_id`),
  CONSTRAINT `tanka_edit_logs_ibfk_2` FOREIGN KEY (`flavour_account_id`) REFERENCES `flavour_accounts` (`flavour_account_id`),
  CONSTRAINT `tanka_edit_logs_ibfk_3` FOREIGN KEY (`author_id`) REFERENCES `authors` (`author_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table tanka_word_map
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tanka_word_map`;

CREATE TABLE `tanka_word_map` (
  `tanka_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `word_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  UNIQUE KEY `tanka_id` (`tanka_id`,`word_id`),
  KEY `word_id` (`word_id`),
  CONSTRAINT `tanka_word_map_ibfk_1` FOREIGN KEY (`tanka_id`) REFERENCES `tankas` (`tanka_id`),
  CONSTRAINT `tanka_word_map_ibfk_2` FOREIGN KEY (`word_id`) REFERENCES `words` (`word_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table tankas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tankas`;

CREATE TABLE `tankas` (
  `tanka_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `author_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `tanka` char(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `hiragana` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `romaji` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`tanka_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table translation_approval_points
# ------------------------------------------------------------

DROP TABLE IF EXISTS `translation_approval_points`;

CREATE TABLE `translation_approval_points` (
  `flavour_account_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `language_id` tinyint(3) unsigned NOT NULL,
  `points` smallint(5) unsigned NOT NULL,
  UNIQUE KEY `flavour_account_id` (`flavour_account_id`,`language_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `translation_approval_points_ibfk_1` FOREIGN KEY (`flavour_account_id`) REFERENCES `flavour_accounts` (`flavour_account_id`),
  CONSTRAINT `translation_approval_points_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table translation_edit_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `translation_edit_logs`;

CREATE TABLE `translation_edit_logs` (
  `translation_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `flavour_account_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `translation` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `when` datetime NOT NULL,
  KEY `translation_id` (`translation_id`,`when`),
  KEY `flavour_account_id` (`flavour_account_id`),
  CONSTRAINT `translation_edit_logs_ibfk_1` FOREIGN KEY (`translation_id`) REFERENCES `translations` (`translation_id`),
  CONSTRAINT `translation_edit_logs_ibfk_2` FOREIGN KEY (`flavour_account_id`) REFERENCES `flavour_accounts` (`flavour_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table translations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `translations`;

CREATE TABLE `translations` (
  `translation_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `tanka_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `language_id` tinyint(3) unsigned NOT NULL,
  `translation` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`translation_id`),
  UNIQUE KEY `tanka_id` (`tanka_id`,`language_id`),
  KEY `language_id` (`language_id`),
  CONSTRAINT `translations_ibfk_1` FOREIGN KEY (`tanka_id`) REFERENCES `tankas` (`tanka_id`),
  CONSTRAINT `translations_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table word_edit_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `word_edit_logs`;

CREATE TABLE `word_edit_logs` (
  `word_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `flavour_account_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `word` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `when` datetime NOT NULL,
  KEY `word_id` (`word_id`,`when`),
  KEY `flavour_account_id` (`flavour_account_id`),
  CONSTRAINT `word_edit_logs_ibfk_1` FOREIGN KEY (`word_id`) REFERENCES `words` (`word_id`),
  CONSTRAINT `word_edit_logs_ibfk_2` FOREIGN KEY (`flavour_account_id`) REFERENCES `flavour_accounts` (`flavour_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;



# Dump of table words
# ------------------------------------------------------------

DROP TABLE IF EXISTS `words`;

CREATE TABLE `words` (
  `word_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `word` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`word_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
