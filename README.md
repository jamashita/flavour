# Flavour

Flavour (発音 /fleɪvər/) は日本の短歌を収蔵するデータベースです。

Flavour (pronunciation /fleɪvər/)  is Japanese tanka collecting database.

## What is Flavour?

食べ物や飲み物のFlavourはその味です。

Flavour of a food or drink is its taste.

### Account and Password

日本のユーザー向け

```
Account: 日本語
Password: nihongo
```

for English users

```
Account: english
Password: english
```

## Requests

Flavourは構築中です。要望があるときは問題を作成してください。

Flavour is constructing. When you have some requests, create a new issue.

## When you find wrong translations

私の翻訳能力が十分でないため、Flavourには誤った翻訳が含まれている可能性があります。  
誤った翻訳を見つけた時は伝えてください、すぐに修正します。  
ご協力ありがとうございます。

Flavour may contain some wrong translations because of my insufficient translation ability.  
If you find one, please tell me and I will correct it immediately.  
Thank you for your cooperation.

## For developers
### Run in development mode
#### Prerequirement
docker is required.  
if you haven't installed docker for desktop, install it first.

1. run `yarn install`
2. move to `deployment/development` directory
3. run `docker-compose up -d` to run mysql and redis in docker
4. move to root directory in this project
5. build the app by running `yarn build`
6. run `yarn gulp:watch` to run the server (server runs in foreground)
7. run `yarn webpack:watch` to run the frontend (frontend also runs in foreground so you have to launch new window in addition to the server)
8. access `http://localhost:4000` and you will see the app login page
9. the account and password are already provided, please see the *Account and Password* section.

## License

[MIT](LICENSE)
