import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Language } from '../../flavour-entity/Language';
import { Tanka } from '../../flavour-entity/Tanka';
import { flavourMySQL } from '../../flavour-infrastructure/FlavourMySQL';
import { ISO639 } from '../../flavour-vo/ISO639';
import { LanguageID } from '../../flavour-vo/LanguageID';
import { LanguageName } from '../../flavour-vo/LanguageName';
import { TankaID } from '../../flavour-vo/TankaID';
import { TankaQuery } from '../TankaQuery';

describe('TankaQuery', () => {
  describe('find', () => {
    it('translation is present', async () => {
      const stub: SinonStub = sinon.stub();
      flavourMySQL.execute = stub;
      stub.onCall(0).resolves([
        {
          tankaID: '998106de-b2e7-4981-9643-22cd30cd74de',
          authorID: '511fca29-cff7-489d-98e0-7461b36da2eb',
          author: 'author',
          authorHiragana: 'ちょしゃ',
          authorRomaji: 'chosha',
          tanka: '短歌',
          hiragana: 'ひらがな',
          romaji: 'romaji',
          translationID: 'a5cab4b6-81e9-4e73-94a5-8afb3b4b1307',
          translation: 'translation'
        }
      ]);
      stub.onCall(1).resolves([
      ]);

      const tankaQuery: TankaQuery = TankaQuery.getInstance();
      const tanka: Tanka = await tankaQuery.find(TankaID.of('998106de-b2e7-4981-9643-22cd30cd74de'), Language.from(LanguageID.of(1), LanguageName.of('japanese'), ISO639.of('ja')));

      expect(tanka.getTankaID().get()).toEqual('998106de-b2e7-4981-9643-22cd30cd74de');
      expect(tanka.getAuthor().getAuthorID().get()).toEqual('511fca29-cff7-489d-98e0-7461b36da2eb');
      expect(tanka.getAuthor().getName().get()).toEqual('author');
      expect(tanka.getAuthor().getHiragana().get()).toEqual('ちょしゃ');
      expect(tanka.getAuthor().getRomaji().get()).toEqual('chosha');
      expect(tanka.getTanka().get()).toEqual('短歌');
      expect(tanka.getHiragana().get()).toEqual('ひらがな');
      expect(tanka.getRomaji().get()).toEqual('romaji');
      expect(tanka.getTranslation().isPresent()).toEqual(true);
      expect(tanka.getTranslation().get().getTranslationID().get()).toEqual('a5cab4b6-81e9-4e73-94a5-8afb3b4b1307');
      expect(tanka.getTranslation().get().getTranslation().get()).toEqual('translation');
      expect(tanka.getWords().length()).toEqual(0);
    });

    it('translation is not present', async () => {
      const stub: SinonStub = sinon.stub();
      flavourMySQL.execute = stub;
      stub.onCall(0).resolves([
        {
          tankaID: '998106de-b2e7-4981-9643-22cd30cd74de',
          authorID: '511fca29-cff7-489d-98e0-7461b36da2eb',
          author: 'author',
          authorHiragana: 'ちょしゃ',
          authorRomaji: 'chosha',
          tanka: '短歌',
          hiragana: 'ひらがな',
          romaji: 'romaji',
          translationID: null,
          translation: null
        }
      ]);
      stub.onCall(1).resolves([
      ]);

      const tankaQuery: TankaQuery = TankaQuery.getInstance();
      const tanka: Tanka = await tankaQuery.find(TankaID.of('998106de-b2e7-4981-9643-22cd30cd74de'), Language.from(LanguageID.of(1), LanguageName.of('japanese'), ISO639.of('ja')));

      expect(tanka.getTankaID().get()).toEqual('998106de-b2e7-4981-9643-22cd30cd74de');
      expect(tanka.getAuthor().getAuthorID().get()).toEqual('511fca29-cff7-489d-98e0-7461b36da2eb');
      expect(tanka.getAuthor().getName().get()).toEqual('author');
      expect(tanka.getAuthor().getHiragana().get()).toEqual('ちょしゃ');
      expect(tanka.getAuthor().getRomaji().get()).toEqual('chosha');
      expect(tanka.getTanka().get()).toEqual('短歌');
      expect(tanka.getHiragana().get()).toEqual('ひらがな');
      expect(tanka.getRomaji().get()).toEqual('romaji');
      expect(tanka.getTranslation().isPresent()).toEqual(false);
    });
  });
});
