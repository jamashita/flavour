import { Author, AuthorRow } from '../flavour-entity/Author';
import { NoSuchElementError } from '../flavour-error/NoSuchElementError';
import { flavourMySQL } from '../flavour-infrastructure/FlavourMySQL';
import { AuthorID } from '../flavour-vo/AuthorID';

export class AuthorQuery {
  private static instance: AuthorQuery = new AuthorQuery();

  public static getInstance(): AuthorQuery {
    return AuthorQuery.instance;
  }

  private constructor() {
  }

  public async find(authorID: AuthorID): Promise<Author> {
    const query: string = `SELECT
      R1.author_id AS authorID,
      R1.name,
      R1.hiragana,
      R1.romaji
      FROM authors R1
      WHERE R1.author_id = :authorID;`;

    const authors: Array<AuthorRow> = await flavourMySQL.execute(query, {
      authorID: authorID.get()
    });

    if (authors.length === 0) {
      throw new NoSuchElementError(authorID.get());
    }

    return Author.fromRow(authors[0]);
  }
}
