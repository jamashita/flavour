import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { TranslationID } from '../../flavour-vo/TranslationID';
import { ApprovedTranslationCommand } from '../ApprovedTranslationCommand';

describe('ApprovedTranslationCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const translationID: TranslationID = TranslationID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const approvedTranslationCommand: ApprovedTranslationCommand = ApprovedTranslationCommand.getInstance(query);
      await approvedTranslationCommand.create(translationID);

      expect(stub.withArgs(`INSERT INTO approved_translations VALUES (
      :translationID
      );`, {
        translationID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });

  describe('delete', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const translationID: TranslationID = TranslationID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const approvedTranslationCommand: ApprovedTranslationCommand = ApprovedTranslationCommand.getInstance(query);
      await approvedTranslationCommand.delete(translationID);

      expect(stub.withArgs(`DELETE FROM approved_translations
      WHERE translation_id = :translationID;`, {
        translationID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });
});
