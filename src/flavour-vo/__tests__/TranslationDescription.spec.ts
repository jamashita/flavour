import 'jest';
import { TranslationDescription } from '../TranslationDescription';

describe('TranslationDescription', () => {
  describe('equals', () => {
    it('returns true if both properties are the same', () => {
      const description1: TranslationDescription = TranslationDescription.of('description 1');
      const description2: TranslationDescription = TranslationDescription.of('description 2');
      const description3: TranslationDescription = TranslationDescription.of('description 1');

      expect(description1.equals(description1)).toEqual(true);
      expect(description1.equals(description2)).toEqual(false);
      expect(description1.equals(description3)).toEqual(true);
    });
  });
});
