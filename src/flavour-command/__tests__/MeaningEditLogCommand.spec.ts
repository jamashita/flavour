import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Meaning } from '../../flavour-entity/Meaning';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { FlavourAccountID } from '../../flavour-vo/FlavourAccountID';
import { MeaningDescription } from '../../flavour-vo/MeaningDescription';
import { MeaningID } from '../../flavour-vo/MeaningID';
import { MeaningEditLogCommand } from '../MeaningEditLogCommand';

describe('MeaningEditLogCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const meaning: Meaning = Meaning.from(
        MeaningID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        MeaningDescription.of('description')
      );
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('90d73259-1bc0-41e5-b2d8-c237915f5276');

      const meaningEditLogCommand: MeaningEditLogCommand = MeaningEditLogCommand.getInstance(query);
      await meaningEditLogCommand.create(meaning, flavourAccountID);

      expect(stub.withArgs(`INSERT INTO meaning_edit_logs VALUES (
      :meaningID,
      :flavourAccountID,
      :meaning,
      UTC_TIMESTAMP()
      );`, {
        meaningID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        flavourAccountID: '90d73259-1bc0-41e5-b2d8-c237915f5276',
        meaning: 'description'
      }).called).toEqual(true)
    });
  });
});
