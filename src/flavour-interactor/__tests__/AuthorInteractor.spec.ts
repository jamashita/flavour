import 'jest';
import sinon from 'sinon';
import { SinonSpy } from 'sinon';
import { Author } from '../../flavour-entity/Author';
import { AuthorCreateTransaction } from '../../flavour-transaction/AuthorCreateTransaction';
import { AuthorDeleteTransaction } from '../../flavour-transaction/AuthorDeleteTransaction';
import { AuthorUpdateTransaction } from '../../flavour-transaction/AuthorUpdateTransaction';
import { AuthorHiragana } from '../../flavour-vo/AuthorHiragana';
import { AuthorID } from '../../flavour-vo/AuthorID';
import { AuthorName } from '../../flavour-vo/AuthorName';
import { AuthorRomaji } from '../../flavour-vo/AuthorRomaji';
import { AuthorInteractor } from '../AuthorInteractor';

describe('AuthorInteractor', () => {
  describe('create', () => {
    it('normal case', async () => {
      const authorID: AuthorID = AuthorID.of('49490ea1-1eaf-418b-9d72-bc8d66363f4a');
      const name: AuthorName = AuthorName.of('author');
      const hiragana: AuthorHiragana = AuthorHiragana.of('ちょしゃ');
      const romaji: AuthorRomaji = AuthorRomaji.of('chosha');
      const author: Author = Author.from(authorID, name, hiragana, romaji);

      const spy: SinonSpy = sinon.spy();
      AuthorCreateTransaction.prototype.with = spy;

      const authorInteractor: AuthorInteractor = AuthorInteractor.getInstance();

      await authorInteractor.create(author);

      expect(spy.called).toEqual(true);
    });
  });

  describe('update', () => {
    it('normal case', async () => {
      const authorID: AuthorID = AuthorID.of('49490ea1-1eaf-418b-9d72-bc8d66363f4a');
      const name: AuthorName = AuthorName.of('author');
      const hiragana: AuthorHiragana = AuthorHiragana.of('ちょしゃ');
      const romaji: AuthorRomaji = AuthorRomaji.of('chosha');
      const author: Author = Author.from(authorID, name, hiragana, romaji);

      const spy: SinonSpy = sinon.spy();
      AuthorUpdateTransaction.prototype.with = spy;

      const authorInteractor: AuthorInteractor = AuthorInteractor.getInstance();

      await authorInteractor.update(author);

      expect(spy.called).toEqual(true);
    });
  });

  describe('delete', () => {
    it('normal case', async () => {
      const authorID: AuthorID = AuthorID.of('49490ea1-1eaf-418b-9d72-bc8d66363f4a');

      const spy: SinonSpy = sinon.spy();
      AuthorDeleteTransaction.prototype.with = spy;

      const authorInteractor: AuthorInteractor = AuthorInteractor.getInstance();

      await authorInteractor.delete(authorID);

      expect(spy.called).toEqual(true);
    });
  });
});
