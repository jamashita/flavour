import 'jest';
import { TranslationDescription } from '../../flavour-vo/TranslationDescription';
import { TranslationID } from '../../flavour-vo/TranslationID';
import { Translation, TranslationJSON, TranslationRow } from '../Translation';

describe('Translation', () => {
  describe('equals', () => {
    it('returns true if the id equals', () => {
      const translation1: Translation = Translation.from(TranslationID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3'), TranslationDescription.of('english'));
      const translation2: Translation = Translation.from(TranslationID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3'), TranslationDescription.of('français'));
      const translation3: Translation = Translation.from(TranslationID.of('1f1600a8-803b-49a9-b283-e167c88523f7'), TranslationDescription.of('english'));
      const translation4: Translation = Translation.from(TranslationID.of('1f1600a8-803b-49a9-b283-e167c88523f7'), TranslationDescription.of('français'));

      expect(translation1.equals(translation1)).toEqual(true);
      expect(translation1.equals(translation2)).toEqual(true);
      expect(translation1.equals(translation3)).toEqual(false);
      expect(translation1.equals(translation4)).toEqual(false);
    });
  });

  describe('copy', () => {
    it('every properties are copied', () => {
      const translationID: TranslationID = TranslationID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');
      const translationString: TranslationDescription = TranslationDescription.of('english');

      const traslation: Translation = Translation.from(translationID, translationString);
      const copied: Translation = traslation.copy();

      expect(copied.getTranslationID()).toEqual(translationID);
      expect(copied.getTranslation()).toEqual(translationString);
    });
  });

  describe('toJSON', () => {
    it('normal case', () => {
      const translation: Translation = Translation.from(TranslationID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3'), TranslationDescription.of('english'));

      expect(translation.toJSON()).toEqual({
        translationID: '6bd0255c-1972-4060-9bb4-88b4670e51b3',
        translation: 'english'
      });
    });
  });

  describe('from', () => {
    it('normal case', () => {
      const translationID: TranslationID = TranslationID.of('07ca3964-f97e-4120-9888-64001682e301');
      const translationString: TranslationDescription = TranslationDescription.of('fitting room');

      const translation: Translation = Translation.from(translationID, translationString);

      expect(translation.getTranslationID()).toEqual(translationID);
      expect(translation.getTranslation()).toEqual(translationString);
    });
  });

  describe('fromJSON', () => {
    it('normal case', () => {
      const json: TranslationJSON = {
        translationID: '07ca3964-f97e-4120-9888-64001682e301',
        translation: 'fitting room'
      };

      const translation: Translation = Translation.fromJSON(json);

      expect(translation.getTranslationID().get()).toEqual(json.translationID);
      expect(translation.getTranslation().get()).toEqual(json.translation);
    });
  });

  describe('fromRow', () => {
    it('normal case', () => {
      const row: TranslationRow = {
        translationID: '07ca3964-f97e-4120-9888-64001682e301',
        translation: 'fitting room'
      };

      const translation: Translation = Translation.fromRow(row);

      expect(translation.getTranslationID().get()).toEqual(row.translationID);
      expect(translation.getTranslation().get()).toEqual(row.translation);
    });
  });
});
