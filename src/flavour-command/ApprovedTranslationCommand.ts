import { IQuery } from '../flavour-general/MySQL/IQuery';
import { TranslationID } from '../flavour-vo/TranslationID';

export class ApprovedTranslationCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): ApprovedTranslationCommand {
    return new ApprovedTranslationCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(translationID: TranslationID): Promise<any> {
    const query: string = `INSERT INTO approved_translations VALUES (
      :translationID
      );`;

    return this.query.execute(query, {
      translationID: translationID.get()
    });
  }

  public delete(translationID: TranslationID): Promise<any> {
    const query: string = `DELETE FROM approved_translations
      WHERE translation_id = :translationID;`;

    return this.query.execute(query, {
      translationID: translationID.get()
    });
  }
}
