import 'jest';
import { ISO639 } from '../../flavour-vo/ISO639';
import { LanguageID } from '../../flavour-vo/LanguageID';
import { LanguageName } from '../../flavour-vo/LanguageName';
import { Language, LanguageJSON, LanguageRow } from '../Language';

describe('Language', () => {
  describe('equals', () => {
    it('returns true if the id equals', () => {
      const language1: Language = Language.from(LanguageID.of(1), LanguageName.of('english'), ISO639.of('en'));
      const language2: Language = Language.from(LanguageID.of(1), LanguageName.of('français'), ISO639.of('en'));
      const language3: Language = Language.from(LanguageID.of(1), LanguageName.of('english'), ISO639.of('fr'));
      const language4: Language = Language.from(LanguageID.of(1), LanguageName.of('français'), ISO639.of('fr'));
      const language5: Language = Language.from(LanguageID.of(2), LanguageName.of('english'), ISO639.of('en'));

      expect(language1.equals(language1)).toEqual(true);
      expect(language1.equals(language2)).toEqual(true);
      expect(language1.equals(language3)).toEqual(true);
      expect(language1.equals(language4)).toEqual(true);
      expect(language1.equals(language5)).toEqual(false);
    });
  });

  describe('copy', () => {
    it('every properties are copied', () => {
      const languageID: LanguageID = LanguageID.of(2);
      const name: LanguageName = LanguageName.of('english');
      const iso639: ISO639 = ISO639.of('en');

      const language: Language = Language.from(languageID, name, iso639);
      const copied: Language = language.copy();

      expect(copied.getLanguageID()).toEqual(languageID);
      expect(copied.getName()).toEqual(name);
      expect(copied.getISO639()).toEqual(iso639);
    });
  });

  describe('toJSON', () => {
    it('normal case', () => {
      const languageID: LanguageID = LanguageID.of(2);
      const name: LanguageName = LanguageName.of('english');
      const iso639: ISO639 = ISO639.of('en');

      const language: Language = Language.from(languageID, name, iso639);

      expect(language.toJSON()).toEqual({
        languageID: 2,
        name: 'english',
        iso639: 'en'
      });
    });
  });

  describe('from', () => {
    it('normal case', () => {
      const languageID: LanguageID = LanguageID.of(1);
      const name: LanguageName = LanguageName.of('japanese');
      const iso639: ISO639 = ISO639.of('ja');

      const language: Language = Language.from(languageID, name, iso639);

      expect(language.getLanguageID()).toEqual(languageID);
      expect(language.getName()).toEqual(name);
      expect(language.getISO639()).toEqual(iso639);
    });
  });

  describe('fromJSON', () => {
    it('normal case', () => {
      const json: LanguageJSON = {
        languageID: 1,
        name: 'japanese',
        iso639: 'ja'
      };

      const language: Language = Language.fromJSON(json);

      expect(language.getLanguageID().get()).toEqual(json.languageID);
      expect(language.getName().get()).toEqual(json.name);
      expect(language.getISO639().get()).toEqual(json.iso639);
    });
  });

  describe('fromRow', () => {
    it('normal case', () => {
      const row: LanguageRow = {
        languageID: 1,
        name: 'japanese',
        iso639: 'ja'
      };

      const language: Language = Language.fromRow(row);

      expect(language.getLanguageID().get()).toEqual(row.languageID);
      expect(language.getName().get()).toEqual(row.name);
      expect(language.getISO639().get()).toEqual(row.iso639);
    });
  });
});
