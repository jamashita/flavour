import 'jest';
import { AuthorHiragana } from '../AuthorHiragana';

describe('AuthorHiragana', () => {
  describe('equals', () => {
    it('returns true if both properties are the same', () => {
      const hiragana1: AuthorHiragana = AuthorHiragana.of('hiragana 1');
      const hiragana2: AuthorHiragana = AuthorHiragana.of('hiragana 2');
      const hiragana3: AuthorHiragana = AuthorHiragana.of('hiragana 1');

      expect(hiragana1.equals(hiragana1)).toEqual(true);
      expect(hiragana1.equals(hiragana2)).toEqual(false);
      expect(hiragana1.equals(hiragana3)).toEqual(true);
    });
  });
});
