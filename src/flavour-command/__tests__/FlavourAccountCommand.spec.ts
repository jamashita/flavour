import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { FlavourAccount } from '../../flavour-entity/FlavourAccount';
import { Language } from '../../flavour-entity/Language';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { AccountName } from '../../flavour-vo/AccountName';
import { FlavourAccountID } from '../../flavour-vo/FlavourAccountID';
import { ISO639 } from '../../flavour-vo/ISO639';
import { LanguageID } from '../../flavour-vo/LanguageID';
import { LanguageName } from '../../flavour-vo/LanguageName';
import { FlavourAccountCommand } from '../FlavourAccountCommand';

describe('FlavourAccountCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const account: FlavourAccount = FlavourAccount.from(
        FlavourAccountID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        AccountName.of('flavour'),
        Language.from(
          LanguageID.of(1),
          LanguageName.of('language'),
          ISO639.of('aa')
        )
      );

      const flavourAccountCommand: FlavourAccountCommand = FlavourAccountCommand.getInstance(query);
      await flavourAccountCommand.create(account, 'hash');

      expect(stub.withArgs(`INSERT INTO flavour_accounts VALUES (
      :flavourAccountID,
      :languageID,
      :account,
      :hash,
      true
      );`, {
        flavourAccountID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        languageID: 1,
        account: 'flavour',
        hash: 'hash'
      }).called).toEqual(true);
    });
  });

  describe('update', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const account: FlavourAccount = FlavourAccount.from(
        FlavourAccountID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        AccountName.of('flavour'),
        Language.from(
          LanguageID.of(1),
          LanguageName.of('language'),
          ISO639.of('aa')
        )
      );

      const flavourAccountCommand: FlavourAccountCommand = FlavourAccountCommand.getInstance(query);
      await flavourAccountCommand.update(account, 'hash', false);

      expect(stub.withArgs(`UPDATE flavour_accounts SET
      language_id = :languageID,
      account = :account,
      hash = :hash,
      active = :active
      WHERE flavour_account_id = :flavourAccountID;`, {
        languageID: 1,
        account: 'flavour',
        hash: 'hash',
        active: false,
        flavourAccountID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });
});
