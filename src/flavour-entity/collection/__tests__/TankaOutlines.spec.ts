import 'jest';
import { NoSuchElementError } from '../../../flavour-error/NoSuchElementError';
import { empty } from '../../../flavour-general/Optional/Empty';
import { AuthorHiragana } from '../../../flavour-vo/AuthorHiragana';
import { AuthorID } from '../../../flavour-vo/AuthorID';
import { AuthorName } from '../../../flavour-vo/AuthorName';
import { AuthorRomaji } from '../../../flavour-vo/AuthorRomaji';
import { TankaDescription } from '../../../flavour-vo/TankaDescription';
import { TankaHiragana } from '../../../flavour-vo/TankaHiragana';
import { TankaID } from '../../../flavour-vo/TankaID';
import { TankaRomaji } from '../../../flavour-vo/TankaRomaji';
import { Author } from '../../Author';
import { TankaOutline, TankaOutlineJSON, TankaOutlineRow } from '../../TankaOutline';
import { Translation } from '../../Translation';
import { TankaOutlines } from '../TankaOutlines';

describe('TankaOutlines', () => {
  describe('equals', () => {
    it('returns false if the elements lengths are different', () => {
      const outline1: TankaOutline = TankaOutline.from(
        TankaID.of('07ca3964-f97e-4120-9888-64001682e301'),
        Author.from(
          AuthorID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );
      const outline2: TankaOutline = TankaOutline.from(
        TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        Author.from(
          AuthorID.of('07ca3964-f97e-4120-9888-64001682e301'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );

      const outlines1: TankaOutlines = TankaOutlines.from([outline1]);
      const outlines2: TankaOutlines = TankaOutlines.from([outline1, outline2]);

      expect(outlines1.equals(outlines1)).toEqual(true);
      expect(outlines1.equals(outlines2)).toEqual(false);
    });

    it('returns false if the element sequence is different', () => {
      const outline1: TankaOutline = TankaOutline.from(
        TankaID.of('07ca3964-f97e-4120-9888-64001682e301'),
        Author.from(
          AuthorID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );
      const outline2: TankaOutline = TankaOutline.from(
        TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        Author.from(
          AuthorID.of('07ca3964-f97e-4120-9888-64001682e301'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );

      const outlines1: TankaOutlines = TankaOutlines.from([outline1, outline2]);
      const outlines2: TankaOutlines = TankaOutlines.from([outline2, outline1]);

      expect(outlines1.equals(outlines1)).toEqual(true);
      expect(outlines1.equals(outlines2)).toEqual(false);
    });

    it('returns true if the elements lengths and the sequence are the same', () => {
      const outline1: TankaOutline = TankaOutline.from(
        TankaID.of('07ca3964-f97e-4120-9888-64001682e301'),
        Author.from(
          AuthorID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );
      const outline2: TankaOutline = TankaOutline.from(
        TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        Author.from(
          AuthorID.of('07ca3964-f97e-4120-9888-64001682e301'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );

      const outlines1: TankaOutlines = TankaOutlines.from([outline1, outline2]);
      const outlines2: TankaOutlines = TankaOutlines.from([outline1, outline2]);

      expect(outlines1.equals(outlines1)).toEqual(true);
      expect(outlines1.equals(outlines2)).toEqual(true);
    });
  });

  describe('get', () => {
    it('returns TankaOutline instance at the correct index', () => {
      const outline1: TankaOutline = TankaOutline.from(
        TankaID.of('07ca3964-f97e-4120-9888-64001682e301'),
        Author.from(
          AuthorID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );
      const outline2: TankaOutline = TankaOutline.from(
        TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        Author.from(
          AuthorID.of('07ca3964-f97e-4120-9888-64001682e301'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );

      const outlines: TankaOutlines = TankaOutlines.from([outline1, outline2]);

      expect(outlines.length()).toEqual(2);
      expect(outlines.get(0)).toEqual(outline1);
      expect(outlines.get(1)).toEqual(outline2);
    });

    it('throws NoSuchElementError if the index is out of bound', () => {
      const outline1: TankaOutline = TankaOutline.from(
        TankaID.of('07ca3964-f97e-4120-9888-64001682e301'),
        Author.from(
          AuthorID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );
      const outline2: TankaOutline = TankaOutline.from(
        TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        Author.from(
          AuthorID.of('07ca3964-f97e-4120-9888-64001682e301'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );

      const outlines: TankaOutlines = TankaOutlines.from([outline1, outline2]);

      expect(() => {
        outlines.get(-1);
      }).toThrow(NoSuchElementError);
      expect(() => {
        outlines.get(2);
      }).toThrow(NoSuchElementError);
    });
  });

  describe('copy', () => {
    it('elements are deeply copied', () => {
      const outline1: TankaOutline = TankaOutline.from(
        TankaID.of('07ca3964-f97e-4120-9888-64001682e301'),
        Author.from(
          AuthorID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );
      const outline2: TankaOutline = TankaOutline.from(
        TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        Author.from(
          AuthorID.of('07ca3964-f97e-4120-9888-64001682e301'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );

      const outlines: TankaOutlines = TankaOutlines.from([outline1, outline2]);
      const copied: TankaOutlines = outlines.copy();

      expect(outlines).not.toBe(copied);
      expect(outlines.get(0)).not.toBe(copied.get(0));
      expect(outlines.get(0)).toEqual(copied.get(0));
      expect(outlines.get(1)).not.toBe(copied.get(1));
      expect(outlines.get(1)).toEqual(copied.get(1));
    });
  });

  describe('toJSON', () => {
    it('normal case', () => {
      const outline1: TankaOutline = TankaOutline.from(
        TankaID.of('07ca3964-f97e-4120-9888-64001682e301'),
        Author.from(
          AuthorID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );
      const outline2: TankaOutline = TankaOutline.from(
        TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        Author.from(
          AuthorID.of('07ca3964-f97e-4120-9888-64001682e301'),
          AuthorName.of('author name'),
          AuthorHiragana.of('ちょしゃ'),
          AuthorRomaji.of('chosha')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>()
      );

      const outlines: TankaOutlines = TankaOutlines.from([outline1, outline2]);

      expect(outlines.toJSON()).toEqual([
        {
          tankaID: '07ca3964-f97e-4120-9888-64001682e301',
          author: {
            authorID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
            name: 'author name',
            hiragana: 'ちょしゃ',
            romaji: 'chosha'
          },
          tanka: 'tanka',
          hiragana: 'たんか',
          romaji: 'tanka',
          translation: null
        },
        {
          tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
          author: {
            authorID: '07ca3964-f97e-4120-9888-64001682e301',
            name: 'author name',
            hiragana: 'ちょしゃ',
            romaji: 'chosha'
          },
          tanka: 'tanka',
          hiragana: 'たんか',
          romaji: 'tanka',
          translation: null
        }
      ]);
    });
  });

  describe('fromJSON', () => {
    it('normal case', () => {
      const json: Array<TankaOutlineJSON> = [
        {
          tankaID: '07ca3964-f97e-4120-9888-64001682e301',
          author: {
            authorID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
            name: 'author name',
            hiragana: 'ちょしゃ',
            romaji: 'chosha'
          },
          tanka: 'tanka',
          hiragana: 'たんか',
          romaji: 'tanka',
          translation: null
        },
        {
          tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
          author: {
            authorID: '07ca3964-f97e-4120-9888-64001682e301',
            name: 'author name',
            hiragana: 'ちょしゃ',
            romaji: 'chosha'
          },
          tanka: 'tanka',
          hiragana: 'たんか',
          romaji: 'tanka',
          translation: null
        }
      ];

      const outlines: TankaOutlines = TankaOutlines.fromJSON(json);

      expect(outlines.length()).toEqual(2);
      for (let i = 0; i < outlines.length(); i++) {
        expect(outlines.get(i).getTankaID().get()).toEqual(json[i].tankaID);
        expect(outlines.get(i).getAuthor().getAuthorID().get()).toEqual(json[i].author.authorID);
        expect(outlines.get(i).getAuthor().getName().get()).toEqual(json[i].author.name);
        expect(outlines.get(i).getAuthor().getHiragana().get()).toEqual(json[i].author.hiragana);
        expect(outlines.get(i).getAuthor().getRomaji().get()).toEqual(json[i].author.romaji);
        expect(outlines.get(i).getTanka().get()).toEqual(json[i].tanka);
        expect(outlines.get(i).getHiragana().get()).toEqual(json[i].hiragana);
        expect(outlines.get(i).getRomaji().get()).toEqual(json[i].romaji);
        expect(outlines.get(i).getTranslation().isPresent()).toEqual(false);
      }
    });
  });

  describe('fromRow', () => {
    const rows: Array<TankaOutlineRow> = [
      {
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        authorID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        author: 'author name',
        authorHiragana: 'ちょしゃ',
        authorRomaji: 'chosha',
        tanka: 'tanka',
        hiragana: 'たんか',
        romaji: 'tanka',
        translationID: null,
        translation: null
      },
      {
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        authorID: '07ca3964-f97e-4120-9888-64001682e301',
        author: 'author name',
        authorHiragana: 'ちょしゃ',
        authorRomaji: 'chosha',
        tanka: 'tanka',
        hiragana: 'たんか',
        romaji: 'tanka',
        translationID: null,
        translation: null
      }
    ];

    const outlines: TankaOutlines = TankaOutlines.fromRow(rows);

    expect(outlines.length()).toEqual(2);
    for (let i = 0; i < outlines.length(); i++) {
      expect(outlines.get(i).getTankaID().get()).toEqual(rows[i].tankaID);
      expect(outlines.get(i).getAuthor().getAuthorID().get()).toEqual(rows[i].authorID);
      expect(outlines.get(i).getAuthor().getName().get()).toEqual(rows[i].author);
      expect(outlines.get(i).getAuthor().getHiragana().get()).toEqual(rows[i].authorHiragana);
      expect(outlines.get(i).getAuthor().getRomaji().get()).toEqual(rows[i].authorRomaji);
      expect(outlines.get(i).getTanka().get()).toEqual(rows[i].tanka);
      expect(outlines.get(i).getHiragana().get()).toEqual(rows[i].hiragana);
      expect(outlines.get(i).getRomaji().get()).toEqual(rows[i].romaji);
      expect(outlines.get(i).getTranslation().isPresent()).toEqual(false);
    }
  })
});
