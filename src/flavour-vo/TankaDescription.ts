import { ValueObject } from './ValueObject';

export class TankaDescription extends ValueObject {
  private description: string;

  public static of(description: string): TankaDescription {
    return new TankaDescription(description);
  }

  private constructor(description: string) {
    super();
    this.description = description;
  }

  public get(): string {
    return this.description;
  }

  public equals(other: TankaDescription): boolean {
    if (this === other) {
      return true;
    }
    if (this.description === other.get()) {
      return true;
    }

    return false;
  }

  public toString(): string {
    return this.description;
  }
}
