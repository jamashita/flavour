import { ISO639 } from '../flavour-vo/ISO639';
import { LanguageID } from '../flavour-vo/LanguageID';
import { LanguageName } from '../flavour-vo/LanguageName';
import { Entity } from './Entity';

export type LanguageJSON = {
  languageID: number;
  name: string;
  iso639: string;
};

export type LanguageRow = {
  languageID: number;
  name: string;
  iso639: string;
};

export class Language extends Entity<LanguageID> {
  private languageID: LanguageID;
  private name: LanguageName;
  private iso639: ISO639;

  public static from(languageID: LanguageID, name: LanguageName, iso639: ISO639): Language {
    return new Language(languageID, name, iso639);
  }

  public static fromJSON(json: LanguageJSON): Language {
    const {
      languageID,
      name,
      iso639
    } = json;

    return Language.from(LanguageID.of(languageID), LanguageName.of(name), ISO639.of(iso639));
  }

  public static fromRow(row: LanguageRow): Language {
    const {
      languageID,
      name,
      iso639
    } = row;

    return Language.from(LanguageID.of(languageID), LanguageName.of(name), ISO639.of(iso639));
  }

  public static default(): Language {
    return Language.from(LanguageID.of(0), LanguageName.default(), ISO639.default());
  }

  private constructor(languageID: LanguageID, name: LanguageName, iso639: ISO639) {
    super();
    this.languageID = languageID;
    this.name = name;
    this.iso639 = iso639;
  }

  public getLanguageID(): LanguageID {
    return this.languageID;
  }

  public getName(): LanguageName {
    return this.name;
  }

  public getISO639(): ISO639 {
    return this.iso639;
  }

  public getIdentifier(): LanguageID {
    return this.languageID;
  }

  public copy(): Language {
    const {
      languageID,
      name,
      iso639
    } = this;

    return new Language(languageID, name, iso639);
  }

  public toJSON(): LanguageJSON {
    const {
      languageID,
      name,
      iso639
    } = this;

    return {
      languageID: languageID.get(),
      name: name.get(),
      iso639: iso639.get()
    };
  }

  public toString(): string {
    const {
      languageID,
      name,
      iso639
    } = this;

    return `${languageID.toString()} ${name.toString()} ${iso639.toString()}`;
  }
}
