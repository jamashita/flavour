import { AuthorCommand } from '../flavour-command/AuthorCommand';
import { Author } from '../flavour-entity/Author';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { ITransaction } from '../flavour-general/MySQL/ITransaction';

export class AuthorCreateTransaction implements ITransaction {
  private author: Author;

  public static getInstance(author: Author): AuthorCreateTransaction {
    return new AuthorCreateTransaction(author);
  }

  private constructor(author: Author) {
    this.author = author;
  }

  public with(query: IQuery): Promise<any> {
    const authorCommand: AuthorCommand = AuthorCommand.getInstance(query);

    const {
      author
    } = this;

    return authorCommand.create(author);
  }
}
