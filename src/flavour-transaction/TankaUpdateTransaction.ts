import { TankaCommand } from '../flavour-command/TankaCommand';
import { TankaEditLogCommand } from '../flavour-command/TankaEditLogCommand';
import { Tanka } from '../flavour-entity/Tanka';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { ITransaction } from '../flavour-general/MySQL/ITransaction';
import { FlavourAccountID } from '../flavour-vo/FlavourAccountID';

export class TankaUpdateTransaction implements ITransaction {
  private tanka: Tanka;
  private flavourAccountID: FlavourAccountID;

  public static getInstance(tanka: Tanka, flavourAccountID: FlavourAccountID): TankaUpdateTransaction {
    return new TankaUpdateTransaction(tanka, flavourAccountID);
  }

  private constructor(tanka: Tanka, flavourAccountID: FlavourAccountID) {
    this.tanka = tanka;
    this.flavourAccountID = flavourAccountID;
  }

  public with(query: IQuery): Promise<any> {
    const tankaCommand: TankaCommand = TankaCommand.getInstance(query);
    const tankaEditLogCommand: TankaEditLogCommand = TankaEditLogCommand.getInstance(query);

    const {
      tanka,
      flavourAccountID
    } = this;

    return Promise.all<any>([
      tankaCommand.update(tanka),
      tankaEditLogCommand.create(tanka, flavourAccountID)
    ]);
  }
}
