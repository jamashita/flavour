module.exports = {
  apps: [
    {
      name: 'flavour',
      script: 'dist/flavour-server/Server.js',
      output: 'logs/output.log',
      error: 'logs/error.log',
      exec_mode: 'cluster',
      instances: 'max'
    }
  ]
};
