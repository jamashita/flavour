import 'jest';
import { MeaningDescription } from '../MeaningDescription';

describe('MeaningDescription', () => {
  describe('equals', () => {
    it('returns true if both properties are the same', () => {
      const description1: MeaningDescription = MeaningDescription.of('description 1');
      const description2: MeaningDescription = MeaningDescription.of('description 2');
      const description3: MeaningDescription = MeaningDescription.of('description 1');

      expect(description1.equals(description1)).toEqual(true);
      expect(description1.equals(description2)).toEqual(false);
      expect(description1.equals(description3)).toEqual(true);
    });
  });
});
