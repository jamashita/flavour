import { IQuery } from '../flavour-general/MySQL/IQuery';
import { TankaID } from '../flavour-vo/TankaID';

export class PendingTankaCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): PendingTankaCommand {
    return new PendingTankaCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(tankaID: TankaID): Promise<any> {
    const query: string = `INSERT INTO pending_tankas VALUES (
      :tankaID,
      0
      );`;

    return this.query.execute(query, {
      tankaID: tankaID.get()
    });
  }

  public update(tankaID: TankaID, points: number): Promise<any> {
    const query: string = `UPDATE pending_tankas SET
      approval_points = :points
      WHERE tanka_id = :tankaID;`;

    return this.query.execute(query, {
      points,
      tankaID: tankaID.get()
    });
  }

  public delete(tankaID: TankaID): Promise<any> {
    const query: string = `DELETE FROM pending_tankas
      WHERE tanka_id = :tankaID;`;

    return this.query.execute(query, {
      tankaID: tankaID.get()
    });
  }
}
