import { IQuery } from '../flavour-general/MySQL/IQuery';
import { FlavourAccountID } from '../flavour-vo/FlavourAccountID';
import { LanguageID } from '../flavour-vo/LanguageID';

export class TranslationApprovalPointCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): TranslationApprovalPointCommand {
    return new TranslationApprovalPointCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(flavourAccountID: FlavourAccountID, languageID: LanguageID): Promise<any> {
    const query: string = `INSERT INTO translation_approval_points VALUES(
      :flavourAccountID,
      :languageID,
      0
      );`;

    return this.query.execute(query, {
      flavourAccountID: flavourAccountID.get(),
      languageID: languageID.get()
    });
  }

  public update(flavourAccountID: FlavourAccountID, languageID: LanguageID, points: number): Promise<any> {
    const query: string = `UPDATE translation_approval_points SET
      points = :points
      WHERE flavour_account_id = :flavourAccountID
      AND language_id = :languageID;`;

    return this.query.execute(query, {
      points,
      flavourAccountID: flavourAccountID.get(),
      languageID: languageID.get()
    });
  }
}
