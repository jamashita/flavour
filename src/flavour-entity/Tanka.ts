import { RuntimeError } from '../flavour-error/RuntimeError';
import { empty } from '../flavour-general/Optional/Empty';
import { Optional } from '../flavour-general/Optional/Optional';
import { present } from '../flavour-general/Optional/Present';
import { AuthorHiragana } from '../flavour-vo/AuthorHiragana';
import { AuthorID } from '../flavour-vo/AuthorID';
import { AuthorName } from '../flavour-vo/AuthorName';
import { AuthorRomaji } from '../flavour-vo/AuthorRomaji';
import { TankaDescription } from '../flavour-vo/TankaDescription';
import { TankaHiragana } from '../flavour-vo/TankaHiragana';
import { TankaID } from '../flavour-vo/TankaID';
import { TankaRomaji } from '../flavour-vo/TankaRomaji';
import { TranslationDescription } from '../flavour-vo/TranslationDescription';
import { TranslationID } from '../flavour-vo/TranslationID';
import { Author, AuthorJSON } from './Author';
import { Words } from './collection/Words';
import { Entity } from './Entity';
import { Translation, TranslationJSON } from './Translation';
import { WordJSON } from './Word';

export type TankaJSON = {
  tankaID: string;
  author: AuthorJSON;
  tanka: string;
  hiragana: string;
  romaji: string;
  translation: TranslationJSON | null;
  words: Array<WordJSON>;
};

export type TankaRow = {
  tankaID: string;
  authorID: string;
  author: string;
  authorHiragana: string;
  authorRomaji: string;
  tanka: string;
  hiragana: string;
  romaji: string;
  translationID: string | null;
  translation: string | null;
};

export class Tanka extends Entity<TankaID> {
  private tankaID: TankaID;
  private author: Author;
  private tanka: TankaDescription;
  private hiragana: TankaHiragana;
  private romaji: TankaRomaji;
  private translation: Optional<Translation>;
  private words: Words;

  public static from(tankaID: TankaID, author: Author, tanka: TankaDescription, hiragana: TankaHiragana, romaji: TankaRomaji, translation: Optional<Translation>, words: Words): Tanka {
    return new Tanka(tankaID, author, tanka, hiragana, romaji, translation, words);
  }

  public static fromJSON(json: TankaJSON): Tanka {
    const {
      tankaID,
      author,
      tanka,
      hiragana,
      romaji,
      translation,
      words
    } = json;

    if (translation === null) {
      return Tanka.from(
        TankaID.of(tankaID),
        Author.fromJSON(author),
        TankaDescription.of(tanka),
        TankaHiragana.of(hiragana),
        TankaRomaji.of(romaji),
        empty<Translation>(),
        Words.fromJSON(words)
      );
    }

    return Tanka.from(
      TankaID.of(tankaID),
      Author.fromJSON(author),
      TankaDescription.of(tanka),
      TankaHiragana.of(hiragana),
      TankaRomaji.of(romaji),
      present<Translation>(Translation.fromJSON(translation)),
      Words.fromJSON(words)
    );
  }

  public static fromRow(row: TankaRow, words: Words): Tanka {
    const {
      tankaID,
      authorID,
      author,
      authorHiragana,
      authorRomaji,
      tanka,
      hiragana,
      romaji,
      translationID,
      translation
    } = row;

    if (translationID === null) {
      return Tanka.from(
        TankaID.of(tankaID),
        Author.from(AuthorID.of(authorID), AuthorName.of(author), AuthorHiragana.of(authorHiragana), AuthorRomaji.of(authorRomaji)),
        TankaDescription.of(tanka),
        TankaHiragana.of(hiragana),
        TankaRomaji.of(romaji),
        empty<Translation>(),
        words
      );
    }

    if (translation === null) {
      throw new RuntimeError('TRANSLATION ID IS NOT NULL BUT TRANSLATION IS NULL');
    }

    return Tanka.from(
      TankaID.of(tankaID),
      Author.from(AuthorID.of(authorID), AuthorName.of(author), AuthorHiragana.of(authorHiragana), AuthorRomaji.of(authorRomaji)),
      TankaDescription.of(tanka),
      TankaHiragana.of(hiragana),
      TankaRomaji.of(romaji),
      present<Translation>(Translation.from(TranslationID.of(translationID), TranslationDescription.of(translation))),
      words
    );
  }

  private constructor(tankaID: TankaID, author: Author, tanka: TankaDescription, hiragana: TankaHiragana, romaji: TankaRomaji, translation: Optional<Translation>, words: Words) {
    super();
    this.tankaID = tankaID;
    this.author = author;
    this.tanka = tanka;
    this.hiragana = hiragana;
    this.romaji = romaji;
    this.translation = translation;
    this.words = words;
  }

  public getTankaID(): TankaID {
    return this.tankaID;
  }

  public getAuthor(): Author {
    return this.author;
  }

  public getTanka(): TankaDescription {
    return this.tanka;
  }

  public getHiragana(): TankaHiragana {
    return this.hiragana;
  }

  public getRomaji(): TankaRomaji {
    return this.romaji;
  }

  public getTranslation(): Optional<Translation> {
    return this.translation;
  }

  public getWords(): Words {
    return this.words;
  }

  public getIdentifier(): TankaID {
    return this.tankaID;
  }

  public copy(): Tanka {
    const {
      tankaID,
      author,
      tanka,
      hiragana,
      romaji,
      translation,
      words
    } = this;

    if (translation.isPresent()) {
      const newTranslation: Translation = translation.get().copy();

      return new Tanka(tankaID, author.copy(), tanka, hiragana, romaji, present<Translation>(newTranslation), words.copy());
    }

    return new Tanka(tankaID, author.copy(), tanka, hiragana, romaji, empty<Translation>(), words.copy());
  }

  public toJSON(): TankaJSON {
    const {
      tankaID,
      author,
      tanka,
      hiragana,
      romaji,
      translation,
      words
    } = this;

    if (translation.isPresent()) {
      return {
        tankaID: tankaID.get(),
        author: author.toJSON(),
        tanka: tanka.get(),
        hiragana: hiragana.get(),
        romaji: romaji.get(),
        translation: translation.get().toJSON(),
        words: words.toJSON()
      };
    }

    return {
      tankaID: tankaID.get(),
      author: author.toJSON(),
      tanka: tanka.get(),
      hiragana: hiragana.get(),
      romaji: romaji.get(),
      translation: null,
      words: words.toJSON()
    };
  }

  public toString(): string {
    const {
      tankaID,
      author,
      tanka,
      hiragana,
      romaji,
      translation
    } = this;

    return`${tankaID.toString()} ${author.toString()} ${tanka.toString()} ${hiragana.toString()} ${romaji.toString()} ${translation.toString()}`;
  }
}
