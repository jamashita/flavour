import 'jest';
import { empty } from '../../flavour-general/Optional/Empty';
import { None } from '../../flavour-general/Optional/None';
import { Optional } from '../../flavour-general/Optional/Optional';
import { present } from '../../flavour-general/Optional/Present';
import { MeaningDescription } from '../../flavour-vo/MeaningDescription';
import { MeaningID } from '../../flavour-vo/MeaningID';
import { WordDescription } from '../../flavour-vo/WordDescription';
import { WordID } from '../../flavour-vo/WordID';
import { Meaning } from '../Meaning';
import { Word, WordJSON, WordRow } from '../Word';

describe('Word', () => {
  describe('equals', () => {
    it('returns true if the id equals', () => {
      const word1: Word = Word.from(WordID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3'), WordDescription.of('meaning 1'), present<Meaning>(Meaning.from(MeaningID.of('f11d2821-2839-48ca-b698-06260dd52154'), MeaningDescription.of('meaning in english'))));
      const word2: Word = Word.from(WordID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3'), WordDescription.of('meaning 2'), present<Meaning>(Meaning.from(MeaningID.of('f11d2821-2839-48ca-b698-06260dd52154'), MeaningDescription.of('meaning in english'))));
      const word3: Word = Word.from(WordID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3'), WordDescription.of('meaning 1'), empty<Meaning>());
      const word4: Word = Word.from(WordID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3'), WordDescription.of('meaning 2'), present<Meaning>(Meaning.from(MeaningID.of('9eb8f3f5-bf66-4723-9a96-4fa456aee965'), MeaningDescription.of('meaning en français'))));
      const word5: Word = Word.from(WordID.of('1f1600a8-803b-49a9-b283-e167c88523f7'), WordDescription.of('meaning 2'), present<Meaning>(Meaning.from(MeaningID.of('9eb8f3f5-bf66-4723-9a96-4fa456aee965'), MeaningDescription.of('meaning en français'))));

      expect(word1.equals(word1)).toEqual(true);
      expect(word1.equals(word2)).toEqual(true);
      expect(word1.equals(word3)).toEqual(true);
      expect(word1.equals(word4)).toEqual(true);
      expect(word1.equals(word5)).toEqual(false);
    });
  });

  describe('copy', () => {
    it('every properties are copied', () => {
      const wordID: WordID = WordID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');
      const wordString: WordDescription = WordDescription.of('meaning 1');
      const meaning: Optional<Meaning> = present<Meaning>(Meaning.from(MeaningID.of('f11d2821-2839-48ca-b698-06260dd52154'), MeaningDescription.of('meaning in english')));

      const word: Word = Word.from(wordID, wordString, meaning);
      const copied: Word = word.copy();

      expect(copied.getWordID()).toEqual(wordID);
      expect(copied.getWord()).toEqual(wordString);
      expect(copied.getMeaning()).toEqual(meaning);
    });
  });

  describe('toJSON', () => {
    it('meaning is present', () => {
      const wordID: WordID = WordID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');
      const wordString: WordDescription = WordDescription.of('meaning 1');
      const meaning: Optional<Meaning> = present<Meaning>(Meaning.from(MeaningID.of('f11d2821-2839-48ca-b698-06260dd52154'), MeaningDescription.of('meaning in english')));

      const word: Word = Word.from(wordID, wordString, meaning);

      expect(word.toJSON()).toEqual({
        wordID: '6bd0255c-1972-4060-9bb4-88b4670e51b3',
        word: 'meaning 1',
        meaning: {
          meaningID: 'f11d2821-2839-48ca-b698-06260dd52154',
          meaning: 'meaning in english'
        }
      });
    });

    it('meaning is null', () => {
      const wordID: WordID = WordID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');
      const wordString: WordDescription = WordDescription.of('meaning 1');
      const meaning: Optional<Meaning> = None.of<Meaning>();

      const word: Word = Word.from(wordID, wordString, meaning);

      expect(word.toJSON()).toEqual({
        wordID: '6bd0255c-1972-4060-9bb4-88b4670e51b3',
        word: 'meaning 1',
        meaning: null
      });
    });
  });

  describe('from', () => {
    it('normal case', () => {
      const wordID: WordID = WordID.of('07ca3964-f97e-4120-9888-64001682e301');
      const wordString: WordDescription = WordDescription.of('estantería');
      const meaning: Meaning = Meaning.from(MeaningID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), MeaningDescription.of('bookshelf'));

      const word: Word = Word.from(wordID, wordString, present<Meaning>(meaning));

      expect(word.getWordID()).toEqual(wordID);
      expect(word.getWord()).toEqual(wordString);
      expect(word.getMeaning()).toEqual(present<Meaning>(meaning));
    });
  });

  describe('fromJSON', () => {
    it('word is empty', () => {
      const json: WordJSON = {
        wordID: '07ca3964-f97e-4120-9888-64001682e301',
        word: 'estantería',
        meaning: null
      };

      const word: Word = Word.fromJSON(json);

      expect(word.getWordID().get()).toEqual(json.wordID);
      expect(word.getWord().get()).toEqual(json.word);
      expect(word.getMeaning().isPresent()).toEqual(false);
    });

    it('word is present', () => {
      const json: WordJSON = {
        wordID: '07ca3964-f97e-4120-9888-64001682e301',
        word: 'estantería',
        meaning: {
          meaningID: '731ec97d-9dc0-4691-9609-32703890c1cf',
          meaning: 'bookshelf'
        }
      };

      const word: Word = Word.fromJSON(json);

      expect(word.getWordID().get()).toEqual(json.wordID);
      expect(word.getWord().get()).toEqual(json.word);
      expect(word.getMeaning().isPresent()).toEqual(true);
      expect(word.getMeaning().get().getMeaningID().get()).toEqual('731ec97d-9dc0-4691-9609-32703890c1cf');
      expect(word.getMeaning().get().getMeaning().get()).toEqual('bookshelf');
    });
  });

  describe('fromRow', () => {
    it('word is empty', () => {
      const row: WordRow = {
        wordID: '07ca3964-f97e-4120-9888-64001682e301',
        word: 'estantería',
        meaningID: null,
        meaning: null
      };

      const word: Word = Word.fromRow(row);

      expect(word.getWordID().get()).toEqual(row.wordID);
      expect(word.getWord().get()).toEqual(row.word);
      expect(word.getMeaning().isPresent()).toEqual(false);
    });

    it('word is present', () => {
      const row: WordRow = {
        wordID: '07ca3964-f97e-4120-9888-64001682e301',
        word: 'estantería',
        meaningID: '731ec97d-9dc0-4691-9609-32703890c1cf',
        meaning: 'bookshelf'
      };

      const word: Word = Word.fromRow(row);

      expect(word.getWordID().get()).toEqual(row.wordID);
      expect(word.getWord().get()).toEqual(row.word);
      expect(word.getMeaning().isPresent()).toEqual(true);
      expect(word.getMeaning().get().getMeaningID().get()).toEqual(row.meaningID);
      expect(word.getMeaning().get().getMeaning().get()).toEqual(row.meaning);
    });
  });
});
