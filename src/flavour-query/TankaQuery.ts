import { Words } from '../flavour-entity/collection/Words';
import { Language } from '../flavour-entity/Language';
import { Tanka, TankaRow } from '../flavour-entity/Tanka';
import { TankaOutline, TankaOutlineRow } from '../flavour-entity/TankaOutline';
import { NoSuchElementError } from '../flavour-error/NoSuchElementError';
import { flavourMySQL } from '../flavour-infrastructure/FlavourMySQL';
import { TankaID } from '../flavour-vo/TankaID';
import { WordQuery } from './WordQuery';

type Count = {
  count: number;
};

const wordQuery: WordQuery = WordQuery.getInstance();

export class TankaQuery {
  private static instance: TankaQuery = new TankaQuery();

  public static getInstance(): TankaQuery {
    return TankaQuery.instance;
  }

  private constructor() {
  }

  public async find(tankaID: TankaID, language: Language): Promise<Tanka> {
    const query: string = `SELECT
      R1.tanka_id AS tankaID,
      R3.author_id AS authorID,
      R3.name AS author,
      R3.hiragana AS authorHiragana,
      R3.romaji AS authorRomaji,
      R1.tanka,
      R1.hiragana,
      R1.romaji,
      R4.translation_id AS translationID,
      R4.translation
      FROM tankas R1
      INNER JOIN approved_tankas R2
      USING(tanka_id)
      INNER JOIN authors R3
      USING(author_id)
      LEFT JOIN translations R4
      USING(tanka_id, language_id)
      INNER JOIN approved_translations R5
      USING(translation_id)
      WHERE R1.tanka_id = :tankaID
      AND R4.language_id = :languageID;`;

    const tankas: Array<TankaRow> = await flavourMySQL.execute(query, {
      tankaID: tankaID.get(),
      languageID: language.getLanguageID().get()
    });

    if (tankas.length === 0) {
      throw new NoSuchElementError(tankaID.get());
    }

    const words: Words = await wordQuery.findByTankaID(tankaID, language);

    return Tanka.fromRow(tankas[0], words);
  }

  public async findByLanguage(language: Language, limit: number, offset: number): Promise<Array<TankaOutline>> {
    const query: string = `SELECT
      R1.tanka_id AS tankaID,
      R3.author_id AS authorID,
      R3.name AS author,
      R3.hiragana AS authorHiragana,
      R3.romaji AS authorRomaji,
      R1.tanka,
      R1.hiragana,
      R1.romaji,
      R4.translation_id AS translationID,
      R4.translation
      FROM tankas R1
      INNER JOIN approved_tankas R2
      USING(tanka_id)
      INNER JOIN authors R3
      USING(author_id)
      LEFT JOIN translations R4
      USING(tanka_id)
      INNER JOIN approved_translations R5
      USING(translation_id)
      WHERE R4.language_id = :languageID
      LIMIT :limit
      OFFSET :offset;`;

    const outlines: Array<TankaOutlineRow> = await flavourMySQL.execute(query, {
      languageID: language.getLanguageID().get(),
      limit,
      offset
    });

    return outlines.map<TankaOutline>((outline: TankaOutlineRow): TankaOutline => {
      return TankaOutline.fromRow(outline);
    });
  }

  public async findPendingTankas(language: Language, limit: number, offset: number): Promise<Array<TankaOutline>> {
    const query: string = `SELECT
      R1.tanka_id AS tankaID,
      R3.author_id AS authorID,
      R3.name AS author,
      R3.hiragana AS authorHiragana,
      R3.romaji AS authorRomaji,
      R1.tanka,
      R1.hiragana,
      R1.romaji,
      R4.translation_id AS translationID,
      R4.translation
      FROM tankas R1
      INNER JOIN pending_tankas R2
      USING(tanka_id)
      INNER JOIN authors R3
      USING(author_id)
      LEFT JOIN translations R4
      USING(tanka_id)
      INNER JOIN approved_translations R5
      USING(translation_id)
      WHERE R4.language_id = :languageID
      LIMIT :limit
      OFFSET :offset;`;

    const outlines: Array<TankaOutlineRow> = await flavourMySQL.execute(query, {
      languageID: language.getLanguageID().get(),
      limit,
      offset
    });

    return outlines.map<TankaOutline>((outline: TankaOutlineRow): TankaOutline => {
      return TankaOutline.fromRow(outline);
    });
  }

  public async count(language: Language): Promise<number> {
    const query: string = `SELECT COUNT(*) AS count
      FROM tankas R1
      INNER JOIN approved_tankas R2
      USING(tanka_id)
      INNER JOIN translations R3
      USING(tanka_id)
      WHERE R3.language_id = :languageID;`;

    const counts: Array<Count> = await flavourMySQL.execute(query, {
      languageID: language.getLanguageID().get()
    });

    return counts[0].count;
  }
}
