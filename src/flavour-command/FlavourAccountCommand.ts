import { FlavourAccount } from '../flavour-entity/FlavourAccount';
import { IQuery } from '../flavour-general/MySQL/IQuery';

export class FlavourAccountCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): FlavourAccountCommand {
    return new FlavourAccountCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public async create(flavourAccount: FlavourAccount, hash: string): Promise<any> {
    const query: string = `INSERT INTO flavour_accounts VALUES (
      :flavourAccountID,
      :languageID,
      :account,
      :hash,
      true
      );`;

    return this.query.execute(query, {
      flavourAccountID: flavourAccount.getFlavourAccountID().get(),
      languageID: flavourAccount.getLanguage().getLanguageID().get(),
      account: flavourAccount.getAccount().get(),
      hash
    });
  }

  public async update(flavourAccount: FlavourAccount, hash: string, active: boolean): Promise<any> {
    const query: string = `UPDATE flavour_accounts SET
      language_id = :languageID,
      account = :account,
      hash = :hash,
      active = :active
      WHERE flavour_account_id = :flavourAccountID;`;

    return this.query.execute(query, {
      languageID: flavourAccount.getLanguage().getLanguageID().get(),
      account: flavourAccount.getAccount().get(),
      hash,
      active,
      flavourAccountID: flavourAccount.getFlavourAccountID().get()
    });
  }
}
