import 'jest';
import { AuthorRomaji } from '../AuthorRomaji';

describe('AuthorRomaji', () => {
  describe('equals', () => {
    it('returns true if both properties are the same', () => {
      const romaji1: AuthorRomaji = AuthorRomaji.of('romaji 1');
      const romaji2: AuthorRomaji = AuthorRomaji.of('romaji 2');
      const romaji3: AuthorRomaji = AuthorRomaji.of('romaji 1');

      expect(romaji1.equals(romaji1)).toEqual(true);
      expect(romaji1.equals(romaji2)).toEqual(false);
      expect(romaji1.equals(romaji3)).toEqual(true);
    });
  });
});
