import { Language } from '../flavour-entity/Language';
import { Tanka } from '../flavour-entity/Tanka';
import { TankaID } from '../flavour-vo/TankaID';

export class TranslationInteractor {

  public async findPendingTranslations(language: Language): Promise<Array<Tanka>> {
    // TODO
    return [];
  }

  public async approveTranslation(tankaID: TankaID, language: Language): Promise<any> {
    // TODO
  }
}
