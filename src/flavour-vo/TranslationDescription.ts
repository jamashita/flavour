import { ValueObject } from './ValueObject';

export class TranslationDescription extends ValueObject {
  private description: string;

  public static of(description: string): TranslationDescription {
    return new TranslationDescription(description);
  }

  private constructor(description: string) {
    super();
    this.description = description;
  }

  public get(): string {
    return this.description;
  }

  public equals(other: TranslationDescription): boolean {
    if (this === other) {
      return true;
    }
    if (this.description === other.get()) {
      return true;
    }

    return false;
  }

  public toString(): string {
    return this.description;
  }
}
