import { Language } from '../flavour-entity/Language';
import { Tanka } from '../flavour-entity/Tanka';
import { TankaOutline } from '../flavour-entity/TankaOutline';
import { NoTankaError } from '../flavour-error/NoTankaError';
import { ITransaction } from '../flavour-general/MySQL/ITransaction';
import { Random } from '../flavour-general/Random';
import { flavourMySQL } from '../flavour-infrastructure/FlavourMySQL';
import { TankaQuery } from '../flavour-query/TankaQuery';
import { TankaApproveTransaction } from '../flavour-transaction/TankaApproveTransaction';
import { TankaCreateTransaction } from '../flavour-transaction/TankaCreateTransaction';
import { TankaUpdateTransaction } from '../flavour-transaction/TankaUpdateTransaction';
import { FlavourAccountID } from '../flavour-vo/FlavourAccountID';
import { TankaID } from '../flavour-vo/TankaID';

const tankaQuery: TankaQuery = TankaQuery.getInstance();

const LIMIT: number = 40;

export class TankaInteractor {
  private static instance: TankaInteractor = new TankaInteractor();

  public static getInstance(): TankaInteractor {
    return TankaInteractor.instance;
  }

  private constructor() {
  }

  public async findTankaAtRandom(language: Language): Promise<TankaOutline> {
    const counts: number = await tankaQuery.count(language);

    if (counts === 0) {
      throw new NoTankaError();
    }

    const limit: number = 1;
    const offset: number = Random.integer(0, counts - 1);
    const tankas: Array<TankaOutline> = await tankaQuery.findByLanguage(language, limit, offset);

    if (tankas.length === 0) {
      throw new NoTankaError();
    }

    return tankas[0];
  }

  public findTanka(tankaID: TankaID, language: Language): Promise<Tanka> {
    return tankaQuery.find(tankaID, language);
  }

  public create(tanka: Tanka, flavourAccountID: FlavourAccountID): Promise<any> {
    const tankaCreateTransaction: ITransaction = TankaCreateTransaction.getInstance(tanka, flavourAccountID);

    return flavourMySQL.transact(tankaCreateTransaction);
  }

  public async findPendings(language: Language, page: number): Promise<Array<TankaOutline>> {
    const limit: number = LIMIT;
    const offset: number = (page - 1) * LIMIT;

    return tankaQuery.findPendingTankas(language, limit, offset);
  }

  public approve(tankaID: TankaID): Promise<any> {
    const tankaApproveTransaction: ITransaction = TankaApproveTransaction.getInstance(tankaID);

    return flavourMySQL.transact(tankaApproveTransaction);
  }

  public update(tanka: Tanka, flavourAccountID: FlavourAccountID): Promise<any> {
    const tankaUpdateTransaction: ITransaction = TankaUpdateTransaction.getInstance(tanka, flavourAccountID);

    return flavourMySQL.transact(tankaUpdateTransaction);
  }
}
