import { RuntimeError } from '../flavour-error/RuntimeError';
import { UUID } from '../flavour-general/UUID';
import { ValueObject } from './ValueObject';

export class WordID extends ValueObject {
  private id: string;

  public static of(id: string): WordID {
    if (id.length === UUID.size()) {
      return new WordID(id);
    }

    throw new RuntimeError(`WordID requires ${UUID.size()} LENGTH`);
  }

  private constructor(id: string) {
    super();
    this.id = id;
  }

  public get(): string {
    return this.id;
  }

  public equals(other: WordID): boolean {
    if (this === other) {
      return true;
    }
    if (this.id === other.get()) {
      return true;
    }

    return false;
  }

  public toString(): string {
    return this.id;
  }
}
