export class OptionalError extends Error {

  public constructor(message: string) {
    super(message);
  }
}
