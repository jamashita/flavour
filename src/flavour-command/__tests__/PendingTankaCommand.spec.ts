import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { TankaID } from '../../flavour-vo/TankaID';
import { PendingTankaCommand } from '../PendingTankaCommand';

describe('PendingTankaCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const taknaID: TankaID = TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const pendingTankaCommand: PendingTankaCommand = PendingTankaCommand.getInstance(query);
      await pendingTankaCommand.create(taknaID);

      expect(stub.withArgs(`INSERT INTO pending_tankas VALUES (
      :tankaID,
      0
      );`, {
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });

  describe('update', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const taknaID: TankaID = TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const pendingTankaCommand: PendingTankaCommand = PendingTankaCommand.getInstance(query);
      await pendingTankaCommand.update(taknaID, 100);

      expect(stub.withArgs(`UPDATE pending_tankas SET
      approval_points = :points
      WHERE tanka_id = :tankaID;`, {
        points: 100,
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });

  describe('delete', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const taknaID: TankaID = TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const pendingTankaCommand: PendingTankaCommand = PendingTankaCommand.getInstance(query);
      await pendingTankaCommand.delete(taknaID);

      expect(stub.withArgs(`DELETE FROM pending_tankas
      WHERE tanka_id = :tankaID;`, {
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });
});
