import { RuntimeError } from '../flavour-error/RuntimeError';
import { UUID } from '../flavour-general/UUID';
import { ValueObject } from './ValueObject';

export class TranslationID extends ValueObject {
  private id: string;

  public static of(id: string): TranslationID {
    if (id.length === UUID.size()) {
      return new TranslationID(id);
    }

    throw new RuntimeError(`TranslationID requires ${UUID.size()} LENGTH`);
  }

  private constructor(id: string) {
    super();
    this.id = id;
  }

  public get(): string {
    return this.id;
  }

  public equals(other: TranslationID): boolean {
    if (this === other) {
      return true;
    }
    if (this.id === other.get()) {
      return true;
    }

    return false;
  }

  public toString(): string {
    return this.id;
  }
}
