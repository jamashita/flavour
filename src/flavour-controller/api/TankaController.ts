import express from 'express';
import { INTERNAL_SERVER_ERROR, OK } from 'http-status';
import log4js from 'log4js';
import { TankaOutline } from '../../flavour-entity/TankaOutline';
import { TankaInteractor } from '../../flavour-interactor/TankaInteractor';

const router: express.Router = express.Router();
const logger: log4js.Logger = log4js.getLogger();

const tankaInteractor: TankaInteractor = TankaInteractor.getInstance();

router.get('/random', async (req: express.Request, res: express.Response): Promise<any> => {
  try {
    const tankaOutline: TankaOutline = await tankaInteractor.findTankaAtRandom();

    res.status(OK).send(tankaOutline.toJSON());
  }
  catch (err) {
    logger.fatal(err.message);
    res.sendStatus(INTERNAL_SERVER_ERROR);
  }
});

export const TankaController: express.Router = router;
