import { Tanka } from '../flavour-entity/Tanka';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { FlavourAccountID } from '../flavour-vo/FlavourAccountID';

export class TankaEditLogCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): TankaEditLogCommand {
    return new TankaEditLogCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(tanka: Tanka, flavourAccountID: FlavourAccountID): Promise<any> {
    const query: string = `INSERT INTO tanka_edit_logs VALUES (
      :tankaID,
      :flavourAccountID,
      :authorID,
      :tanka,
      :hiragana,
      :romaji,
      UTC_TIMESTAMP()
      );`;

    return this.query.execute(query, {
      tankaID: tanka.getTankaID().get(),
      flavourAccountID: flavourAccountID.get(),
      authorID: tanka.getAuthor().getAuthorID().get(),
      tanka: tanka.getTanka().get(),
      hiragana: tanka.getHiragana().get(),
      romaji: tanka.getRomaji().get()
    });
  }
}
