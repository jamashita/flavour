import { AuthorHiragana } from '../flavour-vo/AuthorHiragana';
import { AuthorID } from '../flavour-vo/AuthorID';
import { AuthorName } from '../flavour-vo/AuthorName';
import { AuthorRomaji } from '../flavour-vo/AuthorRomaji';
import { Entity } from './Entity';

export type AuthorJSON = {
  authorID: string;
  name: string;
  hiragana: string;
  romaji: string;
};

export type AuthorRow = {
  authorID: string;
  name: string;
  hiragana: string;
  romaji: string;
};

export class Author extends Entity<AuthorID> {
  private authorID: AuthorID;
  private name: AuthorName;
  private hiragana: AuthorHiragana;
  private romaji: AuthorRomaji;

  public static from(authorID: AuthorID, name: AuthorName, hiragana: AuthorHiragana, romaji: AuthorRomaji): Author {
    return new Author(authorID, name, hiragana, romaji);
  }

  public static fromJSON(json: AuthorJSON): Author {
    const {
      authorID,
      name,
      hiragana,
      romaji
    } = json;

    return Author.from(AuthorID.of(authorID), AuthorName.of(name), AuthorHiragana.of(hiragana), AuthorRomaji.of(romaji));
  }

  public static fromRow(row: AuthorRow): Author {
    const {
      authorID,
      name,
      hiragana,
      romaji
    } = row;

    return Author.from(AuthorID.of(authorID), AuthorName.of(name), AuthorHiragana.of(hiragana), AuthorRomaji.of(romaji));
  }

  private constructor(authorID: AuthorID, name: AuthorName, hiragana: AuthorHiragana, romaji: AuthorRomaji) {
    super();
    this.authorID = authorID;
    this.name = name;
    this.hiragana = hiragana;
    this.romaji = romaji;
  }

  public getAuthorID(): AuthorID {
    return this.authorID;
  }

  public getName(): AuthorName {
    return this.name;
  }

  public getHiragana(): AuthorHiragana {
    return this.hiragana;
  }

  public getRomaji(): AuthorRomaji {
    return this.romaji;
  }

  public getIdentifier(): AuthorID {
    return this.authorID;
  }

  public toJSON(): AuthorJSON {
    const {
      authorID,
      name,
      hiragana,
      romaji
    } = this;

    return {
      authorID: authorID.get(),
      name: name.get(),
      hiragana: hiragana.get(),
      romaji: romaji.get()
    };
  }

  public copy(): Author {
    const {
      authorID,
      name,
      hiragana,
      romaji
    } = this;

    return new Author(authorID, name, hiragana, romaji);
  }

  public toString(): string {
    const {
      authorID,
      name,
      hiragana,
      romaji
    } = this;

    return `${authorID.toString()}: ${name.toString()}: ${hiragana.toString()}: ${romaji.toString()}`;
  }
}
