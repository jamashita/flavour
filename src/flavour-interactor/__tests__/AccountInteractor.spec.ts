import 'jest';
import sinon from 'sinon';
import { SinonSpy, SinonStub } from 'sinon';
import { FlavourAccount } from '../../flavour-entity/FlavourAccount';
import { Language } from '../../flavour-entity/Language';
import { AccountAlreadyInUseError } from '../../flavour-error/AccountAlreadyInUseError';
import { NoSuchElementError } from '../../flavour-error/NoSuchElementError';
import { FlavourAccountQuery } from '../../flavour-query/FlavourAccountQuery';
import { FlavourAccountCreateTransaction } from '../../flavour-transaction/FlavourAccountCreateTransaction';
import { FlavourAccountUpdateTransaction } from '../../flavour-transaction/FlavourAccountUpdateTransaction';
import { AccountName } from '../../flavour-vo/AccountName';
import { FlavourAccountID } from '../../flavour-vo/FlavourAccountID';
import { ISO639 } from '../../flavour-vo/ISO639';
import { LanguageID } from '../../flavour-vo/LanguageID';
import { LanguageName } from '../../flavour-vo/LanguageName';
import { AccountInteractor } from '../AccountInteractor';

describe('AccountInteractor', () => {
  describe('create', () => {
    it('the account is already in use', async () => {
      const stub: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.findByAccount = stub;
      stub.resolves();

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
      const flavourAccount: FlavourAccount = FlavourAccount.from(
        FlavourAccountID.of('b986f9a6-cddc-437f-9398-031ee96c91f0'),
        AccountName.of('account'),
        language
      );

      const accountInteractor: AccountInteractor = AccountInteractor.getInstance();

      try {
        await accountInteractor.create(flavourAccount);
        fail();
      }
      catch (err) {
        expect(err instanceof AccountAlreadyInUseError).toEqual(true);
      }
    });

    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.findByAccount = stub;
      stub.rejects(new NoSuchElementError(''));
      const spy: SinonSpy = sinon.spy();
      FlavourAccountCreateTransaction.prototype.with = spy;

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
      const flavourAccount: FlavourAccount = FlavourAccount.from(
        FlavourAccountID.of('b986f9a6-cddc-437f-9398-031ee96c91f0'),
        AccountName.of('account'),
        language
      );

      const accountInteractor: AccountInteractor = AccountInteractor.getInstance();
      await accountInteractor.create(flavourAccount);

      expect(spy.called).toEqual(true);
    });

    it('throws error', async () => {
      const stub: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.findByAccount = stub;
      stub.rejects(new Error());
      const spy: SinonSpy = sinon.spy();
      FlavourAccountCreateTransaction.prototype.with = spy;

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
      const flavourAccount: FlavourAccount = FlavourAccount.from(
        FlavourAccountID.of('b986f9a6-cddc-437f-9398-031ee96c91f0'),
        AccountName.of('account'),
        language
      );

      const accountInteractor: AccountInteractor = AccountInteractor.getInstance();

      try {
        await accountInteractor.create(flavourAccount);
        fail();
      }
      catch (err) {
        expect(spy.called).toEqual(false);
      }
    });
  });

  describe('update', () => {
    it('the account is already in use', () => {
      const stub: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.findByAccount = stub;
      stub.resolves();

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
      const flavourAccount: FlavourAccount = FlavourAccount.from(
        FlavourAccountID.of('b986f9a6-cddc-437f-9398-031ee96c91f0'),
        AccountName.of('account'),
        language
      );

      const accountInteractor: AccountInteractor = AccountInteractor.getInstance();

      expect(accountInteractor.update(flavourAccount)).rejects.toThrow(AccountAlreadyInUseError);
    });

    it('normal case', async () => {
      const stub1: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.findByAccount = stub1;
      stub1.rejects(new NoSuchElementError(''));
      const stub2: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.find = stub2;
      stub2.resolves({
        hash: 'hash',
        active: true
      });
      const spy: SinonSpy = sinon.spy();
      FlavourAccountUpdateTransaction.prototype.with = spy;

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
      const flavourAccount: FlavourAccount = FlavourAccount.from(
        FlavourAccountID.of('b986f9a6-cddc-437f-9398-031ee96c91f0'),
        AccountName.of('account'),
        language
      );

      const accountInteractor: AccountInteractor = AccountInteractor.getInstance();
      await accountInteractor.update(flavourAccount);

      expect(spy.called).toEqual(true);
    });

    it('throws error', async () => {
      const stub: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.findByAccount = stub;
      stub.rejects(new Error());
      const spy: SinonSpy = sinon.spy();
      FlavourAccountUpdateTransaction.prototype.with = spy;

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
      const flavourAccount: FlavourAccount = FlavourAccount.from(
        FlavourAccountID.of('b986f9a6-cddc-437f-9398-031ee96c91f0'),
        AccountName.of('account'),
        language
      );

      const accountInteractor: AccountInteractor = AccountInteractor.getInstance();

      try {
        await accountInteractor.update(flavourAccount);
        fail();
      }
      catch (err) {
        expect(spy.called).toEqual(false);
      }
    });
  });

  describe('regeneratePassword', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.find = stub;

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
      const flavourAccount: FlavourAccount = FlavourAccount.from(
        FlavourAccountID.of('b986f9a6-cddc-437f-9398-031ee96c91f0'),
        AccountName.of('account'),
        language
      );
      stub.resolves({
        flavourAccount,
        active: true
      });
      const spy: SinonSpy = sinon.spy();
      FlavourAccountUpdateTransaction.prototype.with = spy;

      const accountInteractor: AccountInteractor = AccountInteractor.getInstance();
      await accountInteractor.regeneratePassword(FlavourAccountID.of('b986f9a6-cddc-437f-9398-031ee96c91f0'));

      expect(spy.called).toEqual(true);
    });
  });

  describe('deactivate', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.find = stub;

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
      const flavourAccount: FlavourAccount = FlavourAccount.from(
        FlavourAccountID.of('b986f9a6-cddc-437f-9398-031ee96c91f0'),
        AccountName.of('account'),
        language
      );
      stub.resolves({
        flavourAccount,
        hash: 'hash'
      });
      const spy: SinonSpy = sinon.spy();
      FlavourAccountUpdateTransaction.prototype.with = spy;

      const accountInteractor: AccountInteractor = AccountInteractor.getInstance();
      await accountInteractor.deactivate(FlavourAccountID.of('b986f9a6-cddc-437f-9398-031ee96c91f0'));

      expect(spy.called).toEqual(true);
    });
  });

  describe('activate', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.find = stub;

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
      const flavourAccount: FlavourAccount = FlavourAccount.from(
        FlavourAccountID.of('b986f9a6-cddc-437f-9398-031ee96c91f0'),
        AccountName.of('account'),
        language
      );
      stub.resolves({
        flavourAccount,
        hash: 'hash'
      });
      const spy: SinonSpy = sinon.spy();
      FlavourAccountUpdateTransaction.prototype.with = spy;

      const accountInteractor: AccountInteractor = AccountInteractor.getInstance();
      await accountInteractor.activate(FlavourAccountID.of('b986f9a6-cddc-437f-9398-031ee96c91f0'));

      expect(spy.called).toEqual(true);
    });
  });
});
