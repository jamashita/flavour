import config from 'config';
import mysql from 'mysql';
import { MySQL } from '../flavour-general/MySQL/MySQL';

export const flavourMySQL: MySQL = new MySQL(config.get<mysql.PoolConfig>('mysql'));
