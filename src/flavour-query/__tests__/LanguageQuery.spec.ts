import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { LanguageCommand } from '../../flavour-command/LanguageCommand';
import { Languages } from '../../flavour-entity/collection/Languages';
import { flavourMySQL } from '../../flavour-infrastructure/FlavourMySQL';
import { flavourRedis } from '../../flavour-infrastructure/FlavourRedis';
import { LanguageQuery } from '../LanguageQuery';

describe('LanguageQuery', () => {
  describe('all', () => {
    it('Redis returns languages', async () => {
      const stub: SinonStub = sinon.stub();
      flavourRedis.getString().get = stub;
      stub.resolves('[{"languageID":1,"name":"日本語","iso639":"ja"},{"languageID":2,"name":"English","iso639":"en"}]');

      const languageQuery: LanguageQuery = LanguageQuery.getInstance();
      const languages: Languages = await languageQuery.all();

      expect(languages.length()).toEqual(2);
      expect(languages.get(0).getLanguageID().get()).toEqual(1);
      expect(languages.get(0).getName().get()).toEqual('日本語');
      expect(languages.get(0).getISO639().get()).toEqual('ja');
      expect(languages.get(1).getLanguageID().get()).toEqual(2);
      expect(languages.get(1).getName().get()).toEqual('English');
      expect(languages.get(1).getISO639().get()).toEqual('en');
    });

    it('MySQL returns languages', async () => {
      const stub1: SinonStub = sinon.stub();
      flavourRedis.getString().get = stub1;
      // @ts-ignore
      stub1.resolves(null);
      const stub2: SinonStub = sinon.stub();
      flavourMySQL.execute = stub2;
      stub2.resolves([
        {
          languageID: 1,
          name: '日本語',
          iso639: 'ja'
        },
        {
          languageID: 2,
          name: 'English',
          iso639: 'en'
        }
      ]);
      const stub3: SinonStub = sinon.stub();
      LanguageCommand.prototype.insertAll = stub3;

      const languageQuery: LanguageQuery = LanguageQuery.getInstance();
      const languages: Languages = await languageQuery.all();

      expect(languages.length()).toEqual(2);
      expect(languages.get(0).getLanguageID().get()).toEqual(1);
      expect(languages.get(0).getName().get()).toEqual('日本語');
      expect(languages.get(0).getISO639().get()).toEqual('ja');
      expect(languages.get(1).getLanguageID().get()).toEqual(2);
      expect(languages.get(1).getName().get()).toEqual('English');
      expect(languages.get(1).getISO639().get()).toEqual('en');
    });
  });
});
