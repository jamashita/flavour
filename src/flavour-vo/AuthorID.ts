import { RuntimeError } from '../flavour-error/RuntimeError';
import { UUID } from '../flavour-general/UUID';
import { ValueObject } from './ValueObject';

export class AuthorID extends ValueObject {
  private id: string;

  public static of(id: string): AuthorID {
    if (id.length === UUID.size()) {
      return new AuthorID(id);
    }

    throw new RuntimeError(`AuthorID requires ${UUID.size()} LENGTH`);
  }

  private constructor(id: string) {
    super();
    this.id = id;
  }

  public get(): string {
    return this.id;
  }

  public equals(other: AuthorID): boolean {
    if (this === other) {
      return true;
    }
    if (this.id === other.get()) {
      return true;
    }

    return false;
  }

  public toString(): string {
    return this.id;
  }
}
