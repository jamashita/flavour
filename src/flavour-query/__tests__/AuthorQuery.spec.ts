import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Author } from '../../flavour-entity/Author';
import { flavourMySQL } from '../../flavour-infrastructure/FlavourMySQL';
import { AuthorID } from '../../flavour-vo/AuthorID';
import { AuthorQuery } from '../AuthorQuery';

describe('AuthorQuery', () => {
  describe('find', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      flavourMySQL.execute = stub;
      stub.resolves([
        {
          authorID: '998106de-b2e7-4981-9643-22cd30cd74de',
          name: 'author'
        }
      ]);

      const authorQuery: AuthorQuery = AuthorQuery.getInstance();
      const author: Author = await authorQuery.find(AuthorID.of('998106de-b2e7-4981-9643-22cd30cd74de',));

      expect(author.getAuthorID().get()).toEqual('998106de-b2e7-4981-9643-22cd30cd74de');
      expect(author.getName().get()).toEqual('author');
    });
  });
});
