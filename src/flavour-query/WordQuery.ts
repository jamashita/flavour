import { Words } from '../flavour-entity/collection/Words';
import { Language } from '../flavour-entity/Language';
import { WordRow } from '../flavour-entity/Word';
import { flavourMySQL } from '../flavour-infrastructure/FlavourMySQL';
import { TankaID } from '../flavour-vo/TankaID';

export class WordQuery {
  private static instance: WordQuery = new WordQuery();

  public static getInstance(): WordQuery {
    return WordQuery.instance;
  }

  private constructor() {
  }

  public async findByTankaID(tankaID: TankaID, language: Language): Promise<Words> {
    const query: string = `SELECT
      R1.word_id AS wordID,
      R1.word,
      R3.meaning_id AS meaningID,
      R3.meaning
      FROM words R1
      INNER JOIN tanka_word_map R2
      USING(word_id)
      LEFT JOIN meanings R3
      USING(word_id)
      WHERE R2.tanka_id = :tankaID
      AND R3.language_id = :languageID;`;

    const wordRows: Array<WordRow> = await flavourMySQL.execute(query, {
      tankaID: tankaID.get(),
      languageID: language.getLanguageID().get()
    });

    return Words.fromRow(wordRows);
  }
}
