import express from 'express';
import { OK } from 'http-status';
import 'jest';
import supertest from 'supertest';
import { FlavourAccount } from '../../../flavour-entity/FlavourAccount';
import { Language } from '../../../flavour-entity/Language';
import { AccountName } from '../../../flavour-vo/AccountName';
import { FlavourAccountID } from '../../../flavour-vo/FlavourAccountID';
import { ISO639 } from '../../../flavour-vo/ISO639';
import { LanguageID } from '../../../flavour-vo/LanguageID';
import { LanguageName } from '../../../flavour-vo/LanguageName';
import { IdentityController } from '../IdentityController';

describe('IdentityController', () => {
  describe('GET /', () => {
    it('returns FlavourAccount as JSON', async () => {
      const app: express.Express = express();
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        // @ts-ignore
        req.user = FlavourAccount.from(FlavourAccountID.of('6ffd502d-e6d9-450c-81c6-05806302ed1b'), AccountName.of('account'), language);
        next();
      });
      app.use('/', IdentityController);

      const response: supertest.Response = await supertest(app).get('/');
      expect(response.status).toEqual(OK);
      expect(response.body).toEqual({
        flavourAccountID: '6ffd502d-e6d9-450c-81c6-05806302ed1b',
        account: 'account',
        language: {
          languageID: 1,
          name: '日本語',
          iso639: 'ja'
        }
      });
    });
  });
});
