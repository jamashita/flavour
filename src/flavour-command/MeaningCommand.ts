import { Meaning } from '../flavour-entity/Meaning';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { LanguageID } from '../flavour-vo/LanguageID';
import { MeaningID } from '../flavour-vo/MeaningID';
import { WordID } from '../flavour-vo/WordID';

export class MeaningCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): MeaningCommand {
    return new MeaningCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(meaning: Meaning, wordID: WordID, languageID: LanguageID): Promise<any> {
    const query: string = `INSERT INTO meanings VALUE(
      :meaningID,
      :wordID,
      :languageID,
      :meaning
      );`;

    return this.query.execute(query, {
      meaningID: meaning.getMeaningID().get(),
      wordID: wordID.get(),
      languageID: languageID.get(),
      meaning: meaning.getMeaning().get()
    });
  }

  public update(meaning: Meaning): Promise<any> {
    const query: string = `UPDATE meanings SET
      meaning = :meaning
      WHERE meaning_id = :meaningID;`;

    return this.query.execute(query, {
      meaning: meaning.getMeaning().get(),
      meaningID: meaning.getMeaningID().get()
    });
  }

  public delete(meaningID: MeaningID): Promise<any> {
    const query: string = `DELETE FROM meanings
      WHERE meaning_id = :meaningID;`;

    return this.query.execute(query, {
      meaningID: meaningID.get()
    });
  }

  public deleteByWordID(wordID: WordID): Promise<any> {
    const query: string = `DELETE FROM meanings
      WHERE word_id = :wordID;`;

    return this.query.execute(query, {
      wordID: wordID.get()
    });
  }
}
