import 'jest';
import { AccountName } from '../../flavour-vo/AccountName';
import { FlavourAccountID } from '../../flavour-vo/FlavourAccountID';
import { ISO639 } from '../../flavour-vo/ISO639';
import { LanguageID } from '../../flavour-vo/LanguageID';
import { LanguageName } from '../../flavour-vo/LanguageName';
import { FlavourAccount, FlavourAccountJSON, FlavourAccountRow } from '../FlavourAccount';
import { Language } from '../Language';

describe('FlavourAccount', () => {
  describe('equals', () => {
    it('returns true if the id equals', () => {
      const flavourAccount1: FlavourAccount = FlavourAccount.from(FlavourAccountID.of('07ca3964-f97e-4120-9888-64001682e301'), AccountName.of('account 1'), Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja')));
      const flavourAccount2: FlavourAccount = FlavourAccount.from(FlavourAccountID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AccountName.of('account 1'), Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja')));
      const flavourAccount3: FlavourAccount = FlavourAccount.from(FlavourAccountID.of('07ca3964-f97e-4120-9888-64001682e301'), AccountName.of('account 2'), Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja')));
      const flavourAccount4: FlavourAccount = FlavourAccount.from(FlavourAccountID.of('07ca3964-f97e-4120-9888-64001682e301'), AccountName.of('account 1'), Language.from(LanguageID.of(3), LanguageName.of('Español'), ISO639.of('es')));
      const flavourAccount5: FlavourAccount = FlavourAccount.from(FlavourAccountID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AccountName.of('account 2'), Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja')));
      const flavourAccount6: FlavourAccount = FlavourAccount.from(FlavourAccountID.of('07ca3964-f97e-4120-9888-64001682e301'), AccountName.of('account 2'), Language.from(LanguageID.of(3), LanguageName.of('Español'), ISO639.of('es')));
      const flavourAccount7: FlavourAccount = FlavourAccount.from(FlavourAccountID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AccountName.of('account 1'), Language.from(LanguageID.of(3), LanguageName.of('Español'), ISO639.of('es')));
      const flavourAccount8: FlavourAccount = FlavourAccount.from(FlavourAccountID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AccountName.of('account 2'), Language.from(LanguageID.of(3), LanguageName.of('Español'), ISO639.of('es')));

      expect(flavourAccount1.equals(flavourAccount1)).toEqual(true);
      expect(flavourAccount1.equals(flavourAccount2)).toEqual(false);
      expect(flavourAccount1.equals(flavourAccount3)).toEqual(true);
      expect(flavourAccount1.equals(flavourAccount4)).toEqual(true);
      expect(flavourAccount1.equals(flavourAccount5)).toEqual(false);
      expect(flavourAccount1.equals(flavourAccount6)).toEqual(true);
      expect(flavourAccount1.equals(flavourAccount7)).toEqual(false);
      expect(flavourAccount1.equals(flavourAccount8)).toEqual(false);
    });
  });

  describe('copy', () => {
    it('every properties are copied', () => {
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('07ca3964-f97e-4120-9888-64001682e301');
      const account: AccountName = AccountName.of('account');
      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));

      const flavourAccount: FlavourAccount = FlavourAccount.from(flavourAccountID, account, language);
      const copied: FlavourAccount = flavourAccount.copy();

      expect(copied.getFlavourAccountID()).toEqual(flavourAccountID);
      expect(copied.getAccount()).toEqual(account);
      expect(copied.getLanguage()).toEqual(language);
    });
  });

  describe('toJSON', () => {
    it('normal case', () => {
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('07ca3964-f97e-4120-9888-64001682e301');
      const account: AccountName = AccountName.of('account');
      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));

      const flavourAccount: FlavourAccount = FlavourAccount.from(flavourAccountID, account, language);

      expect(flavourAccount.toJSON()).toEqual({
        flavourAccountID: '07ca3964-f97e-4120-9888-64001682e301',
        account: 'account',
        language: {
          languageID: 1,
          name: '日本語',
          iso639: 'ja'
        }
      });
    });
  });

  describe('from', () => {
    it('normal case', () => {
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('07ca3964-f97e-4120-9888-64001682e301');
      const account: AccountName = AccountName.of('account');
      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('аҧсуа бызшәа'), ISO639.of('ab'));

      const flavourAccount: FlavourAccount = FlavourAccount.from(flavourAccountID, account, language);

      expect(flavourAccount.getFlavourAccountID()).toEqual(flavourAccountID);
      expect(flavourAccount.getAccount()).toEqual(account);
      expect(flavourAccount.getLanguage()).toEqual(language);
    });
  });

  describe('fromJSON', () => {
    it('normal case', () => {
      const json: FlavourAccountJSON = {
        flavourAccountID: '07ca3964-f97e-4120-9888-64001682e301',
        account: 'account',
        language: {
          languageID: 1,
          name: 'аҧсуа бызшәа',
          iso639: 'ab'
        }
      };

      const flavourAccount: FlavourAccount = FlavourAccount.fromJSON(json);

      expect(flavourAccount.getFlavourAccountID().get()).toEqual(json.flavourAccountID);
      expect(flavourAccount.getAccount().get()).toEqual(json.account);
      expect(flavourAccount.getLanguage().getLanguageID().get()).toEqual(json.language.languageID);
      expect(flavourAccount.getLanguage().getName().get()).toEqual(json.language.name);
      expect(flavourAccount.getLanguage().getISO639().get()).toEqual(json.language.iso639);
    });
  });

  describe('fromRow', () => {
    it('normal case', () => {
      const row: FlavourAccountRow = {
        flavourAccountID: '07ca3964-f97e-4120-9888-64001682e301',
        account: 'account',
        languageID: 1,
        languageName: 'аҧсуа бызшәа',
        iso639: 'ab',
        hash: 'hash',
        active: 1
      };

      const flavourAccount: FlavourAccount = FlavourAccount.fromRow(row);

      expect(flavourAccount.getFlavourAccountID().get()).toEqual(row.flavourAccountID);
      expect(flavourAccount.getAccount().get()).toEqual(row.account);
      expect(flavourAccount.getLanguage().getLanguageID().get()).toEqual(row.languageID);
      expect(flavourAccount.getLanguage().getName().get()).toEqual(row.languageName);
      expect(flavourAccount.getLanguage().getISO639().get()).toEqual(row.iso639);
    });
  });
});
