import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { TankaID } from '../../flavour-vo/TankaID';
import { ApprovedTankaCommand } from '../ApprovedTankaCommand';

describe('ApprovedTankaCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const tankaID: TankaID = TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const approvedTankaCommand: ApprovedTankaCommand = ApprovedTankaCommand.getInstance(query);

      await approvedTankaCommand.create(tankaID);

      expect(stub.withArgs(`INSERT INTO approved_tankas VALUES (
      :tankaID
      );`, {
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });

  describe('delete', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const tankaID: TankaID = TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const approvedTankaCommand: ApprovedTankaCommand = ApprovedTankaCommand.getInstance(query);

      await approvedTankaCommand.delete(tankaID);

      expect(stub.withArgs(`DELETE FROM approved_tankas
      WHERE tanka_id = :tankaID;`, {
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });
});
