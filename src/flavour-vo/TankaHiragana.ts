import { ValueObject } from './ValueObject';

export class TankaHiragana extends ValueObject {
  private hiragana: string;

  public static of(hiragana: string): TankaHiragana {
    return new TankaHiragana(hiragana);
  }

  private constructor(hiragana: string) {
    super();
    this.hiragana = hiragana;
  }

  public get(): string {
    return this.hiragana;
  }

  public equals(other: TankaHiragana): boolean {
    if (this === other) {
      return true;
    }
    if (this.hiragana === other.get()) {
      return true;
    }

    return false;
  }

  public toString(): string {
    return this.hiragana;
  }
}
