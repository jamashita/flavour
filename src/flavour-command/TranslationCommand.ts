import { Translation } from '../flavour-entity/Translation';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { LanguageID } from '../flavour-vo/LanguageID';
import { TankaID } from '../flavour-vo/TankaID';
import { TranslationID } from '../flavour-vo/TranslationID';

export class TranslationCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): TranslationCommand {
    return new TranslationCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(translation: Translation, tankaID: TankaID, languageID: LanguageID): Promise<any> {
    const query: string = `INSERT INTO translations VALUES (
      :translationID,
      :tankaID,
      :languageID,
      :translation
      );`;

    return this.query.execute(query, {
      translationID: translation.getTranslationID().get(),
      tankaID: tankaID.get(),
      languageID: languageID.get(),
      translation: translation.getTranslation().get()
    });
  }

  public update(translation: Translation): Promise<any> {
    const query: string = `UPDATE translations SET
      translation = :translation
      WHERE translation_id = :translationID;`;

    return this.query.execute(query, {
      translation: translation.getTranslation().get(),
      translationID: translation.getTranslationID().get()
    });
  }

  public delete(translationID: TranslationID): Promise<any> {
    const query: string = `DELETE FROM translations
      WHERE translation_id = :translationID;`;

    return this.query.execute(query, {
      translationID: translationID.get()
    });
  }

  public deleteByTankaID(tankaID: TankaID): Promise<any> {
    const query: string = `DELETE FROM translations
      WHERE tanka_id = :tankaID;`;

    return this.query.execute(query, {
      tankaID: tankaID.get()
    });
  }
}
