import { Meaning } from '../flavour-entity/Meaning';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { FlavourAccountID } from '../flavour-vo/FlavourAccountID';

export class MeaningEditLogCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): MeaningEditLogCommand {
    return new MeaningEditLogCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(meaning: Meaning, flavourAccountID: FlavourAccountID): Promise<any> {
    const query: string = `INSERT INTO meaning_edit_logs VALUES (
      :meaningID,
      :flavourAccountID,
      :meaning,
      UTC_TIMESTAMP()
      );`;

    return this.query.execute(query, {
      meaningID: meaning.getMeaningID().get(),
      flavourAccountID: flavourAccountID.get(),
      meaning: meaning.getMeaning().get()
    });
  }
}
