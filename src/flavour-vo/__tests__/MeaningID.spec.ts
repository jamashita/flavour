import 'jest';
import { MeaningID } from '../MeaningID';

describe('MeaningID', () => {
  describe('equals', () => {
    it('returns true if the property is the same', () => {
      const meaningID1: MeaningID = MeaningID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');
      const meaningID2: MeaningID = MeaningID.of('1f1600a8-803b-49a9-b283-e167c88523f7');
      const meaningID3: MeaningID = MeaningID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');

      expect(meaningID1.equals(meaningID1));
      expect(meaningID1.equals(meaningID2));
      expect(meaningID1.equals(meaningID3));
    });
  });
});
