import { TranslationDescription } from '../flavour-vo/TranslationDescription';
import { TranslationID } from '../flavour-vo/TranslationID';
import { Entity } from './Entity';

export type TranslationJSON = {
  translationID: string;
  translation: string;
};

export type TranslationRow = {
  translationID: string;
  translation: string;
};

export class Translation extends Entity<TranslationID> {
  private translationID: TranslationID;
  private translation: TranslationDescription;

  public static from(translationID: TranslationID, translation: TranslationDescription): Translation {
    return new Translation(translationID, translation);
  }

  public static fromJSON(json: TranslationJSON): Translation {
    const {
      translationID,
      translation
    } = json;

    return Translation.from(TranslationID.of(translationID), TranslationDescription.of(translation));
  }

  public static fromRow(row: TranslationRow): Translation {
    const {
      translationID,
      translation
    } = row;

    return Translation.from(TranslationID.of(translationID), TranslationDescription.of(translation));
  }

  private constructor(translationID: TranslationID, translation: TranslationDescription) {
    super();
    this.translationID = translationID;
    this.translation = translation;
  }

  public getTranslationID(): TranslationID {
    return this.translationID;
  }

  public getTranslation(): TranslationDescription {
    return this.translation;
  }

  public getIdentifier(): TranslationID {
    return this.translationID;
  }

  public copy(): Translation {
    const {
      translationID,
      translation
    } = this;

    return new Translation(translationID, translation);
  }

  public toJSON(): TranslationJSON {
    const {
      translationID,
      translation
    } = this;

    return {
      translationID: translationID.get(),
      translation: translation.get()
    };
  }

  public toString(): string {
    const {
      translationID,
      translation
    } = this;

    return `${translationID.toString()} ${translation.toString()}`;
  }
}
