import { AccountName } from '../flavour-vo/AccountName';
import { RuntimeError } from './RuntimeError';

export class AccountAlreadyInUseError extends RuntimeError {

  public constructor(account: AccountName) {
    super(`THIS ACCOUNT IS ALREADY IN USE: ${account.get()}`);
  }
}
