import { FlavourAccount } from '../flavour-entity/FlavourAccount';

declare global {
  namespace Express {
    interface Request {
      account: FlavourAccount;
    }
  }
}
