import { Author } from '../flavour-entity/Author';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { AuthorID } from '../flavour-vo/AuthorID';

export class AuthorCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): AuthorCommand {
    return new AuthorCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(author: Author): Promise<any> {
    const query: string = `INSERT INTO authors VALUES (
      :authorID
      :name,
      :hiragana,
      :romaji
      );`;

    return this.query.execute(query, {
      authorID: author.getAuthorID().get(),
      name: author.getName().get(),
      hiragana: author.getHiragana().get(),
      romaji: author.getRomaji().get()
    });
  }

  public update(author: Author): Promise<any> {
    const query: string = `UPDATE authors SET
      name = :name,
      hiragana = :hiragana,
      romaji = :romaji
      WHERE author_id = :authorID;`;

    return this.query.execute(query, {
      name: author.getName().get(),
      hiragana: author.getHiragana().get(),
      romaji: author.getRomaji().get(),
      authorID: author.getAuthorID().get()
    });
  }

  public delete(authorID: AuthorID): Promise<any> {
    const query: string = `DELETE FROM authors
      WHERE author_id = :authorID;`;

    return this.query.execute(query, {
      authorID: authorID.get()
    });
  }
}
