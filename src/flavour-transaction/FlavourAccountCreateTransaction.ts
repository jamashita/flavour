import { FlavourAccountCommand } from '../flavour-command/FlavourAccountCommand';
import { FlavourAccount } from '../flavour-entity/FlavourAccount';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { ITransaction } from '../flavour-general/MySQL/ITransaction';

export class FlavourAccountCreateTransaction implements ITransaction {
  private flavourAccount: FlavourAccount;
  private hash: string;

  public static getInstance(flavourAccount: FlavourAccount, hash: string): FlavourAccountCreateTransaction {
    return new FlavourAccountCreateTransaction(flavourAccount, hash);
  }

  private constructor(flavourAccount: FlavourAccount, hash: string) {
    this.flavourAccount = flavourAccount;
    this.hash = hash;
  }

  public with(query: IQuery): Promise<any> {
    const flavourAccountCommand: FlavourAccountCommand = FlavourAccountCommand.getInstance(query);

    const {
      flavourAccount,
      hash
    } = this;

    return flavourAccountCommand.create(flavourAccount, hash);
  }
}
