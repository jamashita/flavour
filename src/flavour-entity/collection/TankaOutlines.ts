import { NoSuchElementError } from '../../flavour-error/NoSuchElementError';
import { JSONable } from '../../flavour-general/JSONable';
import { TankaOutline, TankaOutlineJSON, TankaOutlineRow } from '../TankaOutline';

export class TankaOutlines implements JSONable {
  private outlines: Array<TankaOutline>;

  public static from(outlines: Array<TankaOutline>): TankaOutlines {
    return new TankaOutlines(outlines);
  }

  public static fromJSON(json: Array<TankaOutlineJSON>): TankaOutlines {
    return TankaOutlines.from(json.map<TankaOutline>((outline: TankaOutlineJSON): TankaOutline => {
      return TankaOutline.fromJSON(outline);
    }));
  }

  public static fromRow(rows: Array<TankaOutlineRow>): TankaOutlines {
    return TankaOutlines.from(rows.map<TankaOutline>((outline: TankaOutlineRow): TankaOutline => {
      return TankaOutline.fromRow(outline);
    }));
  }

  private constructor(outlines: Array<TankaOutline>) {
    this.outlines = outlines;
  }

  public get(index: number): TankaOutline {
    const outline: TankaOutline | undefined = this.outlines[index];

    if (outline === undefined) {
      throw new NoSuchElementError(index.toString());
    }

    return outline;
  }

  public length(): number {
    return this.outlines.length;
  }

  public equals(other: TankaOutlines): boolean {
    if (this === other) {
      return true;
    }

    const length: number = this.length();
    if (length !== other.length()) {
      return false;
    }
    for (let i: number = 0; i < length; i++) {
      if (!this.outlines[i].equals(other.get(i))) {
        return false;
      }
    }

    return true;
  }

  public copy(): TankaOutlines {
    return TankaOutlines.from(this.outlines.map<TankaOutline>((outline: TankaOutline): TankaOutline => {
      return outline.copy();
    }));
  }

  public toJSON(): Array<TankaOutlineJSON> {
    return this.outlines.map<TankaOutlineJSON>((outline: TankaOutline): TankaOutlineJSON => {
      return outline.toJSON();
    });
  }

  public toString(): string {
    return this.outlines.map<string>((outline: TankaOutline): string => {
      return outline.toString();
    }).join(', ');
  }
}
