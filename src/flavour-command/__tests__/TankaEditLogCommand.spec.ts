import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Author } from '../../flavour-entity/Author';
import { Words } from '../../flavour-entity/collection/Words';
import { Meaning } from '../../flavour-entity/Meaning';
import { Tanka } from '../../flavour-entity/Tanka';
import { Translation } from '../../flavour-entity/Translation';
import { Word } from '../../flavour-entity/Word';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { empty } from '../../flavour-general/Optional/Empty';
import { present } from '../../flavour-general/Optional/Present';
import { AuthorHiragana } from '../../flavour-vo/AuthorHiragana';
import { AuthorID } from '../../flavour-vo/AuthorID';
import { AuthorName } from '../../flavour-vo/AuthorName';
import { AuthorRomaji } from '../../flavour-vo/AuthorRomaji';
import { FlavourAccountID } from '../../flavour-vo/FlavourAccountID';
import { TankaDescription } from '../../flavour-vo/TankaDescription';
import { TankaHiragana } from '../../flavour-vo/TankaHiragana';
import { TankaID } from '../../flavour-vo/TankaID';
import { TankaRomaji } from '../../flavour-vo/TankaRomaji';
import { TranslationDescription } from '../../flavour-vo/TranslationDescription';
import { TranslationID } from '../../flavour-vo/TranslationID';
import { WordDescription } from '../../flavour-vo/WordDescription';
import { WordID } from '../../flavour-vo/WordID';
import { TankaEditLogCommand } from '../TankaEditLogCommand';

describe('TankaEditLogCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const tanka: Tanka = Tanka.from(
        TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
        Author.from(
          AuthorID.of('90d73259-1bc0-41e5-b2d8-c237915f5276'),
          AuthorName.of('author'),
          AuthorHiragana.of('author hiragana'),
          AuthorRomaji.of('author romaji')
        ),
        TankaDescription.of('tanka'),
        TankaHiragana.of('tanka hiragana'),
        TankaRomaji.of('tanka romaji'),
        present<Translation>(Translation.from(
          TranslationID.of('2a24aea8-5d97-4ff6-8168-de30f59417e6'),
          TranslationDescription.of('translation')
        )),
        Words.from([
          Word.from(
            WordID.of('377db089-13ac-4436-ac9f-01192b815c4a'),
            WordDescription.of('word'),
            empty<Meaning>()
          )
        ])
      );
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('7056ac2a-41eb-4e6b-a823-999e4014448b');

      const tankaEditLogCommand: TankaEditLogCommand = TankaEditLogCommand.getInstance(query);
      await tankaEditLogCommand.create(tanka, flavourAccountID);

      expect(stub.withArgs(`INSERT INTO tanka_edit_logs VALUES (
      :tankaID,
      :flavourAccountID,
      :authorID,
      :tanka,
      :hiragana,
      :romaji,
      UTC_TIMESTAMP()
      );`, {
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        flavourAccountID: '7056ac2a-41eb-4e6b-a823-999e4014448b',
        authorID: '90d73259-1bc0-41e5-b2d8-c237915f5276',
        tanka: 'tanka',
        hiragana: 'tanka hiragana',
        romaji: 'tanka romaji'
      }).called).toEqual(true);
    });
  });
});
