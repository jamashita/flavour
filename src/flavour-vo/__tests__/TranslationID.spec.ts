import 'jest';
import { TranslationID } from '../TranslationID';

describe('TranslationID', () => {
  describe('equals', () => {
    it('returns true if the property is the same', () => {
      const translationID1: TranslationID = TranslationID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');
      const translationID2: TranslationID = TranslationID.of('1f1600a8-803b-49a9-b283-e167c88523f7');
      const translationID3: TranslationID = TranslationID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');

      expect(translationID1.equals(translationID1)).toEqual(true);
      expect(translationID1.equals(translationID2)).toEqual(false);
      expect(translationID1.equals(translationID3)).toEqual(true);
    });
  });
});
