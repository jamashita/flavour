import 'jest';
import { TankaRomaji } from '../TankaRomaji';

describe('TankaRomaji', () => {
  describe('equals', () => {
    it('returns true if both properties are the same', () => {
      const romaji1: TankaRomaji = TankaRomaji.of('romaji 1');
      const romaji2: TankaRomaji = TankaRomaji.of('romaji 2');
      const romaji3: TankaRomaji = TankaRomaji.of('romaji 1');

      expect(romaji1.equals(romaji1)).toEqual(true);
      expect(romaji1.equals(romaji2)).toEqual(false);
      expect(romaji1.equals(romaji3)).toEqual(true);
    });
  });
});
