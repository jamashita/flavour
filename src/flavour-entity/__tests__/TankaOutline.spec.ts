import 'jest';
import { empty } from '../../flavour-general/Optional/Empty';
import { Optional } from '../../flavour-general/Optional/Optional';
import { present } from '../../flavour-general/Optional/Present';
import { AuthorHiragana } from '../../flavour-vo/AuthorHiragana';
import { AuthorID } from '../../flavour-vo/AuthorID';
import { AuthorName } from '../../flavour-vo/AuthorName';
import { AuthorRomaji } from '../../flavour-vo/AuthorRomaji';
import { TankaDescription } from '../../flavour-vo/TankaDescription';
import { TankaHiragana } from '../../flavour-vo/TankaHiragana';
import { TankaID } from '../../flavour-vo/TankaID';
import { TankaRomaji } from '../../flavour-vo/TankaRomaji';
import { TranslationDescription } from '../../flavour-vo/TranslationDescription';
import { TranslationID } from '../../flavour-vo/TranslationID';
import { Author } from '../Author';
import { TankaOutline, TankaOutlineJSON, TankaOutlineRow } from '../TankaOutline';
import { Translation } from '../Translation';

describe('TankaOutline', () => {
  describe('equals', () => {
    it('returns true if the id equals', () => {
      const tankaOutline1: TankaOutline = TankaOutline.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('たんか'), TankaRomaji.of('tanka'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))));
      const tankaOutline2: TankaOutline = TankaOutline.from(TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('たんか'), TankaRomaji.of('tanka'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))));
      const tankaOutline3: TankaOutline = TankaOutline.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('たんか'), TankaRomaji.of('tanka'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))));
      const tankaOutline4: TankaOutline = TankaOutline.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('haiku'), TankaHiragana.of('たんか'), TankaRomaji.of('tanka'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))));
      const tankaOutline5: TankaOutline = TankaOutline.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('はいく'), TankaRomaji.of('tanka'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))));
      const tankaOutline6: TankaOutline = TankaOutline.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('たんか'), TankaRomaji.of('haiku'), present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation'))));
      const tankaOutline7: TankaOutline = TankaOutline.from(TankaID.of('07ca3964-f97e-4120-9888-64001682e301'), Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')), TankaDescription.of('tanka'), TankaHiragana.of('たんか'), TankaRomaji.of('tanka'), empty<Translation>());

      expect(tankaOutline1.equals(tankaOutline1)).toEqual(true);
      expect(tankaOutline1.equals(tankaOutline2)).toEqual(false);
      expect(tankaOutline1.equals(tankaOutline3)).toEqual(true);
      expect(tankaOutline1.equals(tankaOutline4)).toEqual(true);
      expect(tankaOutline1.equals(tankaOutline5)).toEqual(true);
      expect(tankaOutline1.equals(tankaOutline6)).toEqual(true);
      expect(tankaOutline1.equals(tankaOutline7)).toEqual(true);
    });
  });

  describe('copy', () => {
    it('translation is empty', () => {
      const tankaID: TankaID = TankaID.of('07ca3964-f97e-4120-9888-64001682e301');
      const author: Author = Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha'));
      const tanka: TankaDescription = TankaDescription.of('tanka');
      const hiragana: TankaHiragana = TankaHiragana.of('ひらがな');
      const romaji: TankaRomaji = TankaRomaji.of('romaji');
      const translation: Optional<Translation> = empty<Translation>();

      const tankaOutline: TankaOutline = TankaOutline.from(tankaID, author, tanka, hiragana, romaji, translation);
      const copied: TankaOutline = tankaOutline.copy();

      expect(copied.getTankaID()).toEqual(tankaID);
      expect(copied.getAuthor()).toEqual(author);
      expect(copied.getTanka()).toEqual(tanka);
      expect(copied.getHiragana()).toEqual(hiragana);
      expect(copied.getRomaji()).toEqual(romaji);
      expect(copied.getTranslation()).toEqual(translation);
    });

    it('translation is present', () => {
      const tankaID: TankaID = TankaID.of('07ca3964-f97e-4120-9888-64001682e301');
      const author: Author = Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha'));
      const tanka: TankaDescription = TankaDescription.of('tanka');
      const hiragana: TankaHiragana = TankaHiragana.of('ひらがな');
      const romaji: TankaRomaji = TankaRomaji.of('romaji');
      const translation: Optional<Translation> = present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation')));

      const tankaOutline: TankaOutline = TankaOutline.from(tankaID, author, tanka, hiragana, romaji, translation);
      const copied: TankaOutline = tankaOutline.copy();

      expect(copied.getTankaID()).toEqual(tankaID);
      expect(copied.getAuthor()).toEqual(author);
      expect(copied.getTanka()).toEqual(tanka);
      expect(copied.getHiragana()).toEqual(hiragana);
      expect(copied.getRomaji()).toEqual(romaji);
      expect(copied.getTranslation()).toEqual(translation);
    });
  });

  describe('toJSON', () => {
    it('translation is empty', () => {
      const tankaID: TankaID = TankaID.of('07ca3964-f97e-4120-9888-64001682e301');
      const author: Author = Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha'));
      const tanka: TankaDescription = TankaDescription.of('tanka');
      const hiragana: TankaHiragana = TankaHiragana.of('ひらがな');
      const romaji: TankaRomaji = TankaRomaji.of('romaji');
      const translation: Optional<Translation> = empty<Translation>();

      const tankaOutline: TankaOutline = TankaOutline.from(tankaID, author, tanka, hiragana, romaji, translation);

      expect(tankaOutline.toJSON()).toEqual({
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        author: {
          authorID: '731ec97d-9dc0-4691-9609-32703890c1cf',
          name: 'author',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        },
        tanka: 'tanka',
        hiragana: 'ひらがな',
        romaji: 'romaji',
        translation: null
      });
    });

    it('translation is present', () => {
      const tankaID: TankaID = TankaID.of('07ca3964-f97e-4120-9888-64001682e301');
      const author: Author = Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha'));
      const tanka: TankaDescription = TankaDescription.of('tanka');
      const hiragana: TankaHiragana = TankaHiragana.of('ひらがな');
      const romaji: TankaRomaji = TankaRomaji.of('romaji');
      const translation: Optional<Translation> = present<Translation>(Translation.from(TranslationID.of('985e107a-e61e-4122-be8d-c4d2dbd22b33'), TranslationDescription.of('translation')));

      const tankaOutline: TankaOutline = TankaOutline.from(tankaID, author, tanka, hiragana, romaji, translation);

      expect(tankaOutline.toJSON()).toEqual({
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        author: {
          authorID: '731ec97d-9dc0-4691-9609-32703890c1cf',
          name: 'author',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        },
        tanka: 'tanka',
        hiragana: 'ひらがな',
        romaji: 'romaji',
        translation: {
          translationID: '985e107a-e61e-4122-be8d-c4d2dbd22b33',
          translation: 'translation'
        }
      });
    });
  });

  describe('from', () => {
    it('normal case', () => {
      const tankaID: TankaID = TankaID.of('07ca3964-f97e-4120-9888-64001682e301');
      const author: Author = Author.from(AuthorID.of('731ec97d-9dc0-4691-9609-32703890c1cf'), AuthorName.of('author'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha'));
      const tanka: TankaDescription = TankaDescription.of('tanka');
      const hiragana: TankaHiragana = TankaHiragana.of('ひらがな');
      const romaji: TankaRomaji = TankaRomaji.of('romaji');
      const translation: Optional<Translation> = present(Translation.from(TranslationID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'), TranslationDescription.of('translation')));

      const tankaOutline: TankaOutline = TankaOutline.from(tankaID, author, tanka, hiragana, romaji, translation);

      expect(tankaOutline.getTankaID()).toEqual(tankaID);
      expect(tankaOutline.getAuthor()).toEqual(author);
      expect(tankaOutline.getTanka()).toEqual(tanka);
      expect(tankaOutline.getHiragana()).toEqual(hiragana);
      expect(tankaOutline.getRomaji()).toEqual(romaji);
      expect(tankaOutline.getTranslation()).toEqual(translation);
    });
  });

  describe('fromJSON', () => {
    it('translation is empty', () => {
      const json: TankaOutlineJSON = {
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        author: {
          authorID: '731ec97d-9dc0-4691-9609-32703890c1cf',
          name: 'author',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        },
        tanka: 'tanka',
        hiragana: 'ひらがな',
        romaji: 'romaji',
        translation: null
      };

      const tankaOutline: TankaOutline = TankaOutline.fromJSON(json);

      expect(tankaOutline.getTankaID().get()).toEqual(json.tankaID);
      expect(tankaOutline.getAuthor().getAuthorID().get()).toEqual(json.author.authorID);
      expect(tankaOutline.getAuthor().getName().get()).toEqual(json.author.name);
      expect(tankaOutline.getAuthor().getHiragana().get()).toEqual(json.author.hiragana);
      expect(tankaOutline.getAuthor().getRomaji().get()).toEqual(json.author.romaji);
      expect(tankaOutline.getTanka().get()).toEqual(json.tanka);
      expect(tankaOutline.getHiragana().get()).toEqual(json.hiragana);
      expect(tankaOutline.getRomaji().get()).toEqual(json.romaji);
      expect(tankaOutline.getTranslation().isPresent()).toEqual(false);
    });

    it('translation is present', () => {
      const json: TankaOutlineJSON = {
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        author: {
          authorID: '731ec97d-9dc0-4691-9609-32703890c1cf',
          name: 'author',
          hiragana: 'ちょしゃ',
          romaji: 'chosha'
        },
        tanka: 'tanka',
        hiragana: 'ひらがな',
        romaji: 'romaji',
        translation: {
          translationID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
          translation: 'translation'
        }
      };

      const tankaOutline: TankaOutline = TankaOutline.fromJSON(json);

      expect(tankaOutline.getTankaID().get()).toEqual(json.tankaID);
      expect(tankaOutline.getAuthor().getAuthorID().get()).toEqual(json.author.authorID);
      expect(tankaOutline.getAuthor().getName().get()).toEqual(json.author.name);
      expect(tankaOutline.getAuthor().getHiragana().get()).toEqual(json.author.hiragana);
      expect(tankaOutline.getAuthor().getRomaji().get()).toEqual(json.author.romaji);
      expect(tankaOutline.getTanka().get()).toEqual(json.tanka);
      expect(tankaOutline.getHiragana().get()).toEqual(json.hiragana);
      expect(tankaOutline.getRomaji().get()).toEqual(json.romaji);
      expect(tankaOutline.getTranslation().isPresent()).toEqual(true);
      // @ts-ignore
      expect(tankaOutline.getTranslation().get().getTranslationID().get()).toEqual(json.translation.translationID);
      // @ts-ignore
      expect(tankaOutline.getTranslation().get().getTranslation().get()).toEqual(json.translation.translation);
    });
  });

  describe('fromRow', () => {
    it('translation is empty', () => {
      const row: TankaOutlineRow = {
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        authorID: '731ec97d-9dc0-4691-9609-32703890c1cf',
        author: 'author',
        authorHiragana: 'ちょしゃ',
        authorRomaji: 'chosha',
        tanka: 'tanka',
        hiragana: 'ひらがな',
        romaji: 'romaji',
        translationID: null,
        translation: null
      };

      const tankaOutline: TankaOutline = TankaOutline.fromRow(row);

      expect(tankaOutline.getTankaID().get()).toEqual(row.tankaID);
      expect(tankaOutline.getAuthor().getAuthorID().get()).toEqual(row.authorID);
      expect(tankaOutline.getAuthor().getName().get()).toEqual(row.author);
      expect(tankaOutline.getAuthor().getHiragana().get()).toEqual(row.authorHiragana);
      expect(tankaOutline.getAuthor().getRomaji().get()).toEqual(row.authorRomaji);
      expect(tankaOutline.getTanka().get()).toEqual(row.tanka);
      expect(tankaOutline.getHiragana().get()).toEqual(row.hiragana);
      expect(tankaOutline.getRomaji().get()).toEqual(row.romaji);
      expect(tankaOutline.getTranslation().isPresent()).toEqual(false);
    });

    it('translation is present', () => {
      const row: TankaOutlineRow = {
        tankaID: '07ca3964-f97e-4120-9888-64001682e301',
        authorID: '731ec97d-9dc0-4691-9609-32703890c1cf',
        author: 'author',
        authorHiragana: 'ちょしゃ',
        authorRomaji: 'chosha',
        tanka: 'tanka',
        hiragana: 'ひらがな',
        romaji: 'romaji',
        translationID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        translation: 'translation'
      };

      const tankaOutline: TankaOutline = TankaOutline.fromRow(row);

      expect(tankaOutline.getTankaID().get()).toEqual(row.tankaID);
      expect(tankaOutline.getAuthor().getAuthorID().get()).toEqual(row.authorID);
      expect(tankaOutline.getAuthor().getName().get()).toEqual(row.author);
      expect(tankaOutline.getAuthor().getHiragana().get()).toEqual(row.authorHiragana);
      expect(tankaOutline.getAuthor().getRomaji().get()).toEqual(row.authorRomaji);
      expect(tankaOutline.getTanka().get()).toEqual(row.tanka);
      expect(tankaOutline.getHiragana().get()).toEqual(row.hiragana);
      expect(tankaOutline.getRomaji().get()).toEqual(row.romaji);
      expect(tankaOutline.getTranslation().isPresent()).toEqual(true);
      expect(tankaOutline.getTranslation().get().getTranslationID().get()).toEqual(row.translationID);
      expect(tankaOutline.getTranslation().get().getTranslation().get()).toEqual(row.translation);
    });
  });
});
