import { AuthorCommand } from '../flavour-command/AuthorCommand';
import { Author } from '../flavour-entity/Author';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { ITransaction } from '../flavour-general/MySQL/ITransaction';

export class AuthorUpdateTransaction implements ITransaction {
  private author: Author;

  public static getInstance(author: Author): AuthorUpdateTransaction {
    return new AuthorUpdateTransaction(author);
  }

  private constructor(author: Author) {
    this.author = author;
  }

  public with(query: IQuery): Promise<any> {
    const authorCommand: AuthorCommand = AuthorCommand.getInstance(query);

    const {
      author
    } = this;

    return authorCommand.update(author);
  }
}
