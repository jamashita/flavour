import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { NoSuchElementError } from '../../flavour-error/NoSuchElementError';
import { Digest } from '../../flavour-general/Digest';
import { FlavourAccountQuery } from '../../flavour-query/FlavourAccountQuery';
import { AuthenticationInteractor } from '../AuthenticationInteractor';

describe('AuthenticationInteractor', () => {
  describe('review', () => {
    it('account not found', (done) => {
      const account: string = 'dummy account';
      const password: string = 'dummy password';

      const stub1: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.findByAccount = stub1;
      stub1.rejects(new NoSuchElementError(account));
      const stub2: SinonStub = sinon.stub();
      Digest.compare = stub2;
      stub2.resolves(true);

      const authenticationInteractor: AuthenticationInteractor = AuthenticationInteractor.getInstance();
      authenticationInteractor.review(account, password, (err: any, ret: any) => {
        expect(err).toEqual(null);
        expect(ret).toEqual(false);
        done();
      });
    });

    it('Digest.compare returns false', (done) => {
      const account: string = 'dummy account';
      const password: string = 'dummy password';

      const stub1: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.findByAccount = stub1;
      stub1.resolves({
        flavourAccount: null,
        hash: 'dummy hash'
      });
      const stub2: SinonStub = sinon.stub();
      Digest.compare = stub2;
      stub2.resolves(false);

      const authenticationInteractor: AuthenticationInteractor = AuthenticationInteractor.getInstance();
      authenticationInteractor.review(account, password, (err: any, ret: any) => {
        expect(err).toEqual(null);
        expect(ret).toEqual(false);
        done();
      });
    });

    it('normal case', (done) => {
      const account: string = 'dummy account';
      const password: string = 'dummy password';

      const stub1: SinonStub = sinon.stub();
      FlavourAccountQuery.prototype.findByAccount = stub1;
      stub1.resolves({
        flavourAccount: 'dummy flavourAccount',
        hash: 'dummy hash'
      });
      const stub2: SinonStub = sinon.stub();
      Digest.compare = stub2;
      stub2.resolves(true);

      const authenticationInteractor: AuthenticationInteractor = AuthenticationInteractor.getInstance();
      authenticationInteractor.review(account, password, (err: any, ret: any) => {
        expect(err).toEqual(null);
        expect(ret).toEqual('dummy flavourAccount');
        done();
      });
    });
  });
});
