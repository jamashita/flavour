import { ValueObject } from './ValueObject';

export class AuthorRomaji extends ValueObject {
  private romaji: string;

  public static of(romaji: string): AuthorRomaji {
    return new AuthorRomaji(romaji);
  }

  private constructor(romaji: string) {
    super();
    this.romaji = romaji;
  }

  public get(): string {
    return this.romaji;
  }

  public equals(other: AuthorRomaji): boolean {
    if (this === other) {
      return true;
    }
    if (this.romaji === other.get()) {
      return true;
    }

    return false;
  }

  public toString(): string {
    return this.romaji;
  }
}
