import { ApprovedTankaCommand } from '../flavour-command/ApprovedTankaCommand';
import { PendingTankaCommand } from '../flavour-command/PendingTankaCommand';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { ITransaction } from '../flavour-general/MySQL/ITransaction';
import { TankaID } from '../flavour-vo/TankaID';

export class TankaApproveTransaction implements ITransaction {
  private tankaID: TankaID;

  public static getInstance(tankaID: TankaID): TankaApproveTransaction {
    return new TankaApproveTransaction(tankaID);
  }

  private constructor(tankaID: TankaID) {
    this.tankaID = tankaID;
  }

  public with(query: IQuery): Promise<any> {
    const pendingTankaCommand: PendingTankaCommand = PendingTankaCommand.getInstance(query);
    const approvedTankaCommand: ApprovedTankaCommand = ApprovedTankaCommand.getInstance(query);

    const {
      tankaID
    } = this;

    return Promise.all<any>([
      pendingTankaCommand.delete(tankaID),
      approvedTankaCommand.create(tankaID)
    ]);
  }
}
