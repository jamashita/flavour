import 'jest';
import { FlavourAccountID } from '../FlavourAccountID';

describe('FlavourAccountID', () => {
  describe('equals', () => {
    it('returns true if the property is the same', () => {
      const flavourAccountID1: FlavourAccountID = FlavourAccountID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');
      const flavourAccountID2: FlavourAccountID = FlavourAccountID.of('1f1600a8-803b-49a9-b283-e167c88523f7');
      const flavourAccountID3: FlavourAccountID = FlavourAccountID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');

      expect(flavourAccountID1.equals(flavourAccountID1)).toEqual(true);
      expect(flavourAccountID1.equals(flavourAccountID2)).toEqual(false);
      expect(flavourAccountID1.equals(flavourAccountID3)).toEqual(true);
    });
  });
});
