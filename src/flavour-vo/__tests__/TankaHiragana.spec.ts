import 'jest';
import { TankaHiragana } from '../TankaHiragana';

describe('TankaHiragana', () => {
  describe('equals', () => {
    it('returns true if both properties are the same', () => {
      const hiragana1: TankaHiragana = TankaHiragana.of('hiragana 1');
      const hiragana2: TankaHiragana = TankaHiragana.of('hiragana 2');
      const hiragana3: TankaHiragana = TankaHiragana.of('hiragana 1');

      expect(hiragana1.equals(hiragana1)).toEqual(true);
      expect(hiragana1.equals(hiragana2)).toEqual(false);
      expect(hiragana1.equals(hiragana3)).toEqual(true);
    });
  });
});
