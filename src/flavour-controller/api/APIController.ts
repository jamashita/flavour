import express from 'express';
import { AuthenticationMiddleware } from '../middlewares/AuthenticationMiddleware';
import { AccountController } from './AccountController';
import { AuthController } from './AuthController';
import { AuthorController } from './AuthorController';
import { DestroyController } from './DestroyController';
import { IdentityController } from './IdentityController';
import { TankaController } from './TankaController';

const router: express.Router = express.Router();

const authenticationMiddleware: AuthenticationMiddleware = AuthenticationMiddleware.getInstance();

router.use('/auth', AuthController);
router.use('/destroy', DestroyController);
router.use('/accounts', AccountController);
router.use('/tankas', TankaController);
router.use(authenticationMiddleware.apply());
router.use('/identity', IdentityController);
router.use('/authors', AuthorController);

export const APIController: express.Router = router;
