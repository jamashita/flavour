import { Identifier } from '../flavour-general/Identifier';

export abstract class ValueObject implements Identifier {

  public abstract equals(other: ValueObject): boolean;

  public abstract toString(): string;
}
