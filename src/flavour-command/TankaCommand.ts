import { Tanka } from '../flavour-entity/Tanka';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { TankaID } from '../flavour-vo/TankaID';

export class TankaCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): TankaCommand {
    return new TankaCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(tanka: Tanka): Promise<any> {
    const query: string = `INSERT INTO tankas VALUES (
      :tankaID,
      :authorID,
      :tanka,
      :hiragana,
      :romaji
      );`;

    return this.query.execute(query, {
      tankaID: tanka.getTankaID().get(),
      authorID: tanka.getAuthor().getAuthorID().get(),
      tanka: tanka.getTanka().get(),
      hiragana: tanka.getHiragana().get(),
      romaji: tanka.getRomaji().get()
    });
  }

  public update(tanka: Tanka): Promise<any> {
    const query: string = `UPDATE tankas SET
      author_id = :authorID,
      tanka = :tanka,
      hiragana = :hiragana,
      romaji = :romaji
      WHERE tanka_id = :tankaID;`;

    return this.query.execute(query, {
      authorID: tanka.getAuthor().getAuthorID().get(),
      tanka: tanka.getTanka().get(),
      hiragana: tanka.getHiragana().get(),
      romaji: tanka.getRomaji().get(),
      tankaID: tanka.getTankaID().get()
    });
  }

  public delete(tankaID: TankaID): Promise<any> {
    const query: string = `DELETE FROM tankas
      WHERE tanka_id = :tankaID;`;

    return this.query.execute(query, {
      tankaID: tankaID.get()
    });
  }
}
