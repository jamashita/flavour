import 'jest';
import { NoSuchElementError } from '../../../flavour-error/NoSuchElementError';
import { empty } from '../../../flavour-general/Optional/Empty';
import { WordDescription } from '../../../flavour-vo/WordDescription';
import { WordID } from '../../../flavour-vo/WordID';
import { Meaning } from '../../Meaning';
import { Word, WordJSON, WordRow } from '../../Word';
import { Words } from '../Words';

describe('Words', () => {
  describe('equals', () => {
    it('returns true if the elements and their order are same', () => {
      const word1: Word = Word.from(WordID.of('07ca3964-f97e-4120-9888-64001682e301'), WordDescription.of('word 1'), empty<Meaning>());
      const word2: Word = Word.from(WordID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'), WordDescription.of('word 2'), empty<Meaning>());

      const words1: Words = Words.from([
        word1,
        word2
      ]);
      const words2: Words = Words.from([
        word1
      ]);
      const words3: Words = Words.from([
        word2
      ]);
      const words4: Words = Words.from([
        word2,
        word1
      ]);
      const words5: Words = Words.from([
        word1,
        word2
      ]);

      expect(words1.equals(words1)).toEqual(true);
      expect(words1.equals(words2)).toEqual(false);
      expect(words1.equals(words3)).toEqual(false);
      expect(words1.equals(words4)).toEqual(false);
      expect(words1.equals(words5)).toEqual(true);
    });
  });

  describe('add', () => {
    it('returns merged object', () => {
      const word1: Word = Word.from(WordID.of('07ca3964-f97e-4120-9888-64001682e301'), WordDescription.of('word 1'), empty<Meaning>());
      const word2: Word = Word.from(WordID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'), WordDescription.of('word 2'), empty<Meaning>());
      let words: Words = Words.from([]);

      expect(words.length()).toEqual(0);
      words = words.add(word1);
      expect(words.length()).toEqual(1);
      words = words.add(word2);
      expect(words.length()).toEqual(2);
    });
  });

  describe('get', () => {
    it('returns Word instance at the correct index', () => {
      const word1: Word = Word.from(WordID.of('07ca3964-f97e-4120-9888-64001682e301'), WordDescription.of('word 1'), empty<Meaning>());
      const word2: Word = Word.from(WordID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'), WordDescription.of('word 2'), empty<Meaning>());
      const words: Words = Words.from([
        word1,
        word2
      ]);

      expect(words.get(0)).toEqual(word1);
      expect(words.get(1)).toEqual(word2);
    });

    it('throws NoSuchElementError if the index is out of bound', () => {
      const word1: Word = Word.from(WordID.of('07ca3964-f97e-4120-9888-64001682e301'), WordDescription.of('word 1'), empty<Meaning>());
      const word2: Word = Word.from(WordID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'), WordDescription.of('word 2'), empty<Meaning>());
      const words: Words = Words.from([
        word1,
        word2
      ]);

      expect(() => {
        words.get(-1);
      }).toThrow(NoSuchElementError);
      expect(() => {
        words.get(2);
      }).toThrow(NoSuchElementError);
    });
  });

  describe('copy', () => {
    it('elements are deeply copied', () => {
      const word1: Word = Word.from(WordID.of('07ca3964-f97e-4120-9888-64001682e301'), WordDescription.of('word 1'), empty<Meaning>());
      const word2: Word = Word.from(WordID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'), WordDescription.of('word 2'), empty<Meaning>());
      const words: Words = Words.from([
        word1,
        word2
      ]);
      const copied: Words = words.copy();

      expect(word1).not.toBe(copied.get(0));
      expect(copied.get(0).equals(word1)).toEqual(true);
      expect(word2).not.toBe(copied.get(1));
      expect(copied.get(1).equals(word2)).toEqual(true);
    });
  });

  describe('toJSON', () => {
    it('equals each element\'s toJSON', () => {
      const word1: Word = Word.from(WordID.of('07ca3964-f97e-4120-9888-64001682e301'), WordDescription.of('word 1'), empty<Meaning>());
      const word2: Word = Word.from(WordID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'), WordDescription.of('word 2'), empty<Meaning>());
      const words: Words = Words.from([
        word1,
        word2
      ]);

      expect(words.toJSON()).toEqual([
        word1.toJSON(),
        word2.toJSON()
      ]);
    });
  });

  describe('fromJSON', () => {
    it('normal case', () => {
      const json: Array<WordJSON> = [
        {
          wordID: '07ca3964-f97e-4120-9888-64001682e301',
          word: 'estantería',
          meaning: {
            meaningID: '731ec97d-9dc0-4691-9609-32703890c1cf',
            meaning: 'bookshelf'
          }
        },
        {
          wordID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
          word: 'escritorio',
          meaning: null
        }
      ];

      const words: Words = Words.fromJSON(json);

      expect(words.length()).toEqual(2);
      for (let i = 0; i < words.length(); i++) {
        expect(words.get(i).getWordID().get()).toEqual(json[i].wordID);
        expect(words.get(i).getWord().get()).toEqual(json[i].word);
        if (words.get(i).getMeaning().isPresent()) {
          expect(words.get(i).getMeaning().get().getMeaningID().get()).toEqual(json[i].meaning!.meaningID);
          expect(words.get(i).getMeaning().get().getMeaning().get()).toEqual(json[i].meaning!.meaning);
        }
      }
    });
  });

  describe('fromRow', () => {
    it('normal case', () => {
      const rows: Array<WordRow> = [
        {
          wordID: '07ca3964-f97e-4120-9888-64001682e301',
          word: 'estantería',
          meaningID: '731ec97d-9dc0-4691-9609-32703890c1cf',
          meaning: 'bookshelf'
        },
        {
          wordID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
          word: 'escritorio',
          meaningID: null,
          meaning: null
        }
      ];

      const words: Words = Words.fromRow(rows);

      expect(words.length()).toEqual(2);
      for (let i = 0; i < words.length(); i++) {
        expect(words.get(i).getWordID().get()).toEqual(rows[i].wordID);
        expect(words.get(i).getWord().get()).toEqual(rows[i].word);
        if (words.get(i).getMeaning().isPresent()) {
          expect(words.get(i).getMeaning().get().getMeaningID().get()).toEqual(rows[i].meaningID);
          expect(words.get(i).getMeaning().get().getMeaning().get()).toEqual(rows[i].meaning);
        }
      }
    });
  });
});
