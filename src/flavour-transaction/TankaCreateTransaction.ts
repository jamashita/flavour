import { PendingTankaCommand } from '../flavour-command/PendingTankaCommand';
import { TankaCommand } from '../flavour-command/TankaCommand';
import { TankaEditLogCommand } from '../flavour-command/TankaEditLogCommand';
import { Tanka } from '../flavour-entity/Tanka';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { ITransaction } from '../flavour-general/MySQL/ITransaction';
import { FlavourAccountID } from '../flavour-vo/FlavourAccountID';

export class TankaCreateTransaction implements ITransaction {
  private tanka: Tanka;
  private flavourAccountID: FlavourAccountID;

  public static getInstance(tanka: Tanka, flavourAccountID: FlavourAccountID): TankaCreateTransaction {
    return new TankaCreateTransaction(tanka, flavourAccountID);
  }

  private constructor(tanka: Tanka, flavourAccountID: FlavourAccountID) {
    this.tanka = tanka;
    this.flavourAccountID = flavourAccountID;
  }

  public async with(query: IQuery): Promise<any> {
    const tankaCommand: TankaCommand = TankaCommand.getInstance(query);
    const tankaEditLogCommand: TankaEditLogCommand = TankaEditLogCommand.getInstance(query);
    const pendingTankaCommand: PendingTankaCommand = PendingTankaCommand.getInstance(query);

    const {
      tanka,
      flavourAccountID
    } = this;

    await tankaCommand.create(tanka);

    return Promise.all<any>([
      tankaEditLogCommand.create(tanka, flavourAccountID),
      pendingTankaCommand.create(tanka.getTankaID())
    ]);
  }
}
