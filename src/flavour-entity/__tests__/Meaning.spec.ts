import 'jest';
import { MeaningDescription } from '../../flavour-vo/MeaningDescription';
import { MeaningID } from '../../flavour-vo/MeaningID';
import { Meaning, MeaningJSON, MeaningRow } from '../Meaning';

describe('Meaning', () => {
  describe('equals', () => {
    it('returns true if the id equals', () => {
      const meaning1: Meaning = Meaning.from(MeaningID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3'), MeaningDescription.of('one'));
      const meaning2: Meaning = Meaning.from(MeaningID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3'), MeaningDescription.of('two'));
      const meaning3: Meaning = Meaning.from(MeaningID.of('1f1600a8-803b-49a9-b283-e167c88523f7'), MeaningDescription.of('one'));
      const meaning4: Meaning = Meaning.from(MeaningID.of('1f1600a8-803b-49a9-b283-e167c88523f7'), MeaningDescription.of('two'));

      expect(meaning1.equals(meaning1)).toEqual(true);
      expect(meaning1.equals(meaning2)).toEqual(true);
      expect(meaning1.equals(meaning3)).toEqual(false);
      expect(meaning1.equals(meaning4)).toEqual(false);
    });
  });

  describe('copy', () => {
    it('every properties are copied', () => {
      const meaningID: MeaningID = MeaningID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3');
      const meaningString: MeaningDescription = MeaningDescription.of('one');

      const meaning: Meaning = Meaning.from(meaningID, meaningString);
      const copied: Meaning = meaning.copy();

      expect(copied.getMeaningID()).toEqual(meaningID);
      expect(copied.getMeaning()).toEqual(meaningString);
    });
  });

  describe('toJSON', () => {
    it('normal case', () => {
      const meaning: Meaning = Meaning.from(MeaningID.of('6bd0255c-1972-4060-9bb4-88b4670e51b3'), MeaningDescription.of('one'));

      expect(meaning.toJSON()).toEqual({
        meaningID: '6bd0255c-1972-4060-9bb4-88b4670e51b3',
        meaning: 'one'
      });
    });
  });

  describe('from', () => {
    it('normal case', () => {
      const meaningID: MeaningID = MeaningID.of('07ca3964-f97e-4120-9888-64001682e301');
      const meaningString: MeaningDescription = MeaningDescription.of('book');

      const meaning: Meaning = Meaning.from(meaningID, meaningString);

      expect(meaning.getMeaningID()).toEqual(meaningID);
      expect(meaning.getMeaning()).toEqual(meaningString);
    });
  });

  describe('fromJSON', () => {
    it('normal case', () => {
      const json: MeaningJSON = {
        meaningID: '07ca3964-f97e-4120-9888-64001682e301',
        meaning: 'book'
      };

      const meaning: Meaning = Meaning.fromJSON(json);

      expect(meaning.getMeaningID().get()).toEqual(json.meaningID);
      expect(meaning.getMeaning().get()).toEqual(json.meaning);
    });
  });

  describe('fromRow', () => {
    it('normal case', () => {
      const row: MeaningRow = {
        meaningID: '07ca3964-f97e-4120-9888-64001682e301',
        meaning: 'book'
      };

      const meaning: Meaning = Meaning.fromRow(row);

      expect(meaning.getMeaningID().get()).toEqual(row.meaningID);
      expect(meaning.getMeaning().get()).toEqual(row.meaning);
    });
  });
});
