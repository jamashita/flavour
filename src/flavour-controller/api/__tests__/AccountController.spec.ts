import bodyParser from 'body-parser';
import express from 'express';
import { BAD_REQUEST, CREATED, INTERNAL_SERVER_ERROR, OK, PRECONDITION_FAILED } from 'http-status';
import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import supertest from 'supertest';
import { FlavourAccount } from '../../../flavour-entity/FlavourAccount';
import { Language } from '../../../flavour-entity/Language';
import { AccountAlreadyInUseError } from '../../../flavour-error/AccountAlreadyInUseError';
import { AccountInteractor } from '../../../flavour-interactor/AccountInteractor';
import { AccountName } from '../../../flavour-vo/AccountName';
import { FlavourAccountID } from '../../../flavour-vo/FlavourAccountID';
import { ISO639 } from '../../../flavour-vo/ISO639';
import { LanguageID } from '../../../flavour-vo/LanguageID';
import { LanguageName } from '../../../flavour-vo/LanguageName';
import { AccountController } from '../AccountController';

describe('AccountController', () => {
  describe('POST /', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      AccountInteractor.prototype.create = stub;
      stub.resolves('password');
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).post('/').send({
        flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de',
        account: 'account',
        language: {
          languageID: 1,
          name: '日本語',
          iso639: 'ja'
        }
      });

      expect(response.status).toEqual(CREATED);
      expect(response.body).toEqual({
        password: 'password'
      });
    });

    it('flavourAccountID is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).post('/').send({
        account: 'account',
        language: {
          languageID: 1,
          name: '日本語',
          iso639: 'ja'
        }
      });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('account is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).post('/').send({
        flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de',
        language: {
          languageID: 1,
          name: '日本語',
          iso639: 'ja'
        }
      });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('language is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).post('/').send({
        flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de',
        account: 'account'
      });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('thrown AccountAlreadyInUseError', async () => {
      const stub: SinonStub = sinon.stub();
      AccountInteractor.prototype.create = stub;
      stub.rejects(new AccountAlreadyInUseError(AccountName.default()));
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).post('/').send({
        flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de',
        account: 'account',
        language: {
          languageID: 1,
          name: '日本語',
          iso639: 'ja'
        }
      });

      expect(response.status).toEqual(PRECONDITION_FAILED);
    });

    it('thrown Error', async () => {
      const stub: SinonStub = sinon.stub();
      AccountInteractor.prototype.create = stub;
      stub.rejects(new Error());
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).post('/').send({
        flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de',
        account: 'account',
        language: {
          languageID: 1,
          name: '日本語',
          iso639: 'ja'
        }
      });

      expect(response.status).toEqual(INTERNAL_SERVER_ERROR);
    });
  });

  describe('PUT /', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      AccountInteractor.prototype.update = stub;
      stub.resolves();
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de',),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/').send({
        account: 'account',
        language: {
          languageID: 1,
          name: '日本語',
          iso639: 'ja'
        }
      });

      expect(response.status).toEqual(OK);
    });

    it('account is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de',),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/').send({
        language: {
          languageID: 1,
          name: '日本語',
          iso639: 'ja'
        }
      });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('language is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/').send({
        account: 'account'
      });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('language.languageID is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/').send({
        account: 'account',
        language: {
          name: '日本語',
          iso639: 'ja'
        }
      });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('language.name is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/').send({
        account: 'account',
        language: {
          languageID: 1,
          iso639: 'ja'
        }
      });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('language.iso639 is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/').send({
        account: 'account',
        language: {
          languageID: 1,
          name: '日本語'
        }
      });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('thrown AccountAlreadyInUseError', async () => {
      const stub: SinonStub = sinon.stub();
      AccountInteractor.prototype.update = stub;
      stub.rejects(new AccountAlreadyInUseError(AccountName.default()));
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/').send({
        account: 'account',
        language: {
          languageID: 1,
          name: '日本語',
          iso639: 'ja'
        }
      });

      expect(response.status).toEqual(PRECONDITION_FAILED);
    });

    it('thrown Error', async () => {
      const stub: SinonStub = sinon.stub();
      AccountInteractor.prototype.update = stub;
      stub.rejects(new Error());
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/').send({
        account: 'account',
        language: {
          languageID: 1,
          name: '日本語',
          iso639: 'ja'
        }
      });

      expect(response.status).toEqual(INTERNAL_SERVER_ERROR);
    });
  });

  describe('PUT /regenerate', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      AccountInteractor.prototype.regeneratePassword = stub;
      stub.resolves('password');
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/regenerate').send({
        flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de'
      });

      expect(response.status).toEqual(OK);
      expect(response.body).toEqual({
        password: 'password'
      });
    });

    it('flavourAccountID is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/regenerate').send({
      });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('thrown Error', async () => {
      const stub: SinonStub = sinon.stub();
      AccountInteractor.prototype.regeneratePassword = stub;
      stub.rejects(new Error());
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/regenerate').send({
        flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de'
      });

      expect(response.status).toEqual(INTERNAL_SERVER_ERROR);
    });
  });

  describe('PUT /activate', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      AccountInteractor.prototype.activate = stub;
      stub.resolves();
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/activate').send({
        flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de'
      });

      expect(response.status).toEqual(OK);
    });

    it('flavourAccountID is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/activate').send({
      });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('thrown Error', async () => {
      const stub: SinonStub = sinon.stub();
      AccountInteractor.prototype.activate = stub;
      stub.rejects(new Error());
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/activate').send({
        flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de'
      });

      expect(response.status).toEqual(INTERNAL_SERVER_ERROR);
    });
  });

  describe('PUT /deactivate', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      AccountInteractor.prototype.deactivate = stub;
      stub.resolves();
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/deactivate').send({
        flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de'
      });

      expect(response.status).toEqual(OK);
    });

    it('flavourAccountID is missing', async () => {
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/deactivate').send({
      });

      expect(response.status).toEqual(BAD_REQUEST);
    });

    it('thrown Error', async () => {
      const stub: SinonStub = sinon.stub();
      AccountInteractor.prototype.deactivate = stub;
      stub.rejects(new Error());
      const app: express.Express = express();
      app.use(bodyParser.urlencoded({
        extended: false
      }));
      app.use(bodyParser.json());
      app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));
        const flavourAccount: FlavourAccount = FlavourAccount.from(
          FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'),
          AccountName.of('account'),
          language
        );
        req.user = flavourAccount;

        next();
      });
      app.use('/', AccountController);

      const response: supertest.Response = await supertest(app).put('/deactivate').send({
        flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de'
      });

      expect(response.status).toEqual(INTERNAL_SERVER_ERROR);
    });
  });
});
