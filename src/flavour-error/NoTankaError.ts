import { RuntimeError } from './RuntimeError';

export class NoTankaError extends RuntimeError {

  public constructor() {
    super('NO TANKAS');
  }
}
