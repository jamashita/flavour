import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { TranslationID } from '../../flavour-vo/TranslationID';
import { PendingTranslationCommand } from '../PendingTranslationCommand';

describe('PendingTranslationCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const translationID: TranslationID = TranslationID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const pendingTranslationCommand: PendingTranslationCommand = PendingTranslationCommand.getInstance(query);
      await pendingTranslationCommand.create(translationID);

      expect(stub.withArgs(`INSERT INTO pending_translations VALUES (
      :translationID,
      0
      );`, {
        translationID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });

  describe('update', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const translationID: TranslationID = TranslationID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const pendingTranslationCommand: PendingTranslationCommand = PendingTranslationCommand.getInstance(query);
      await pendingTranslationCommand.update(translationID, 100);

      expect(stub.withArgs(`UPDATE pending_translations SET
      points = :points
      WHERE translation_id = :translationID;`, {
        points: 100,
        translationID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });

  describe('delete', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const translationID: TranslationID = TranslationID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const pendingTranslationCommand: PendingTranslationCommand = PendingTranslationCommand.getInstance(query);
      await pendingTranslationCommand.delete(translationID);

      expect(stub.withArgs(`DELETE FROM pending_translations
      WHERE translation_id = :translationID;`, {
        translationID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });
});
