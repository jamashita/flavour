import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Translation } from '../../flavour-entity/Translation';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { LanguageID } from '../../flavour-vo/LanguageID';
import { TankaID } from '../../flavour-vo/TankaID';
import { TranslationDescription } from '../../flavour-vo/TranslationDescription';
import { TranslationID } from '../../flavour-vo/TranslationID';
import { TranslationCommand } from '../TranslationCommand';

describe('TranslationCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const translation: Translation = Translation.from(
        TranslationID.of('2a24aea8-5d97-4ff6-8168-de30f59417e6'),
        TranslationDescription.of('translation')
      );
      const tankaID: TankaID = TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');
      const languageID: LanguageID = LanguageID.of(52);

      const translationCommand: TranslationCommand = TranslationCommand.getInstance(query);
      await translationCommand.create(translation, tankaID, languageID);

      expect(stub.withArgs(`INSERT INTO translations VALUES (
      :translationID,
      :tankaID,
      :languageID,
      :translation
      );`, {
        translationID: '2a24aea8-5d97-4ff6-8168-de30f59417e6',
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        languageID: 52,
        translation: 'translation'
      }).called).toEqual(true);
    });
  });

  describe('update', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const translation: Translation = Translation.from(
        TranslationID.of('2a24aea8-5d97-4ff6-8168-de30f59417e6'),
        TranslationDescription.of('translation')
      );

      const translationCommand: TranslationCommand = TranslationCommand.getInstance(query);
      await translationCommand.update(translation);

      expect(stub.withArgs(`UPDATE translations SET
      translation = :translation
      WHERE translation_id = :translationID;`, {
        translation: 'translation',
        translationID: '2a24aea8-5d97-4ff6-8168-de30f59417e6'
      }).called).toEqual(true);
    });
  });

  describe('delete', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const translationID: TranslationID = TranslationID.of('2a24aea8-5d97-4ff6-8168-de30f59417e6');

      const translationCommand: TranslationCommand = TranslationCommand.getInstance(query);
      await translationCommand.delete(translationID);

      expect(stub.withArgs(`DELETE FROM translations
      WHERE translation_id = :translationID;`, {
        translationID: '2a24aea8-5d97-4ff6-8168-de30f59417e6'
      }).called).toEqual(true);
    });
  });

  describe('deleteByTankaID', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const tankaID: TankaID = TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const translationCommand: TranslationCommand = TranslationCommand.getInstance(query);
      await translationCommand.deleteByTankaID(tankaID);

      expect(stub.withArgs(`DELETE FROM translations
      WHERE tanka_id = :tankaID;`, {
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });
});
