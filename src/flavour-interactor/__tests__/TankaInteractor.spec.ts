import 'jest';
import sinon from 'sinon';
import { SinonSpy, SinonStub } from 'sinon';
import { Author } from '../../flavour-entity/Author';
import { Words } from '../../flavour-entity/collection/Words';
import { Language } from '../../flavour-entity/Language';
import { Tanka } from '../../flavour-entity/Tanka';
import { TankaOutline } from '../../flavour-entity/TankaOutline';
import { Translation } from '../../flavour-entity/Translation';
import { NoTankaError } from '../../flavour-error/NoTankaError';
import { MySQL } from '../../flavour-general/MySQL/MySQL';
import { empty } from '../../flavour-general/Optional/Empty';
import { TankaQuery } from '../../flavour-query/TankaQuery';
import { AuthorHiragana } from '../../flavour-vo/AuthorHiragana';
import { AuthorID } from '../../flavour-vo/AuthorID';
import { AuthorName } from '../../flavour-vo/AuthorName';
import { AuthorRomaji } from '../../flavour-vo/AuthorRomaji';
import { FlavourAccountID } from '../../flavour-vo/FlavourAccountID';
import { ISO639 } from '../../flavour-vo/ISO639';
import { LanguageID } from '../../flavour-vo/LanguageID';
import { LanguageName } from '../../flavour-vo/LanguageName';
import { TankaDescription } from '../../flavour-vo/TankaDescription';
import { TankaHiragana } from '../../flavour-vo/TankaHiragana';
import { TankaID } from '../../flavour-vo/TankaID';
import { TankaRomaji } from '../../flavour-vo/TankaRomaji';
import { TankaInteractor } from '../TankaInteractor';

describe('TankaInteractor', () => {
  describe('findTankaAtRandom', () => {
    it('no tankas', async () => {
      const stub: SinonStub = sinon.stub();
      TankaQuery.prototype.count = stub;
      stub.resolves(0);

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));

      const tankaInteractor: TankaInteractor = TankaInteractor.getInstance();

      try {
        await tankaInteractor.findTankaAtRandom(language);
        fail();
      }
      catch (err) {
        expect(err instanceof NoTankaError).toEqual(true);
      }
    });

    it('there is no tankas selected', async () => {
      const stub1: SinonStub = sinon.stub();
      TankaQuery.prototype.count = stub1;
      stub1.resolves(100);
      const stub2: SinonStub = sinon.stub();
      TankaQuery.prototype.findByLanguage = stub2;
      stub2.resolves([]);

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));

      const tankaInteractor: TankaInteractor = TankaInteractor.getInstance();

      try {
        await tankaInteractor.findTankaAtRandom(language);
        fail();
      }
      catch (err) {
        expect(err instanceof NoTankaError).toEqual(true);
      }
    });

    it('normal case', async () => {
      const stub1: SinonStub = sinon.stub();
      TankaQuery.prototype.count = stub1;
      stub1.resolves(100);
      const stub2: SinonStub = sinon.stub();
      TankaQuery.prototype.findByLanguage = stub2;
      const tankaOutline1: TankaOutline = TankaOutline.from(
        TankaID.of('643cd3a1-7e4d-4e68-b246-d4ea6f0e5fc7'),
        Author.from(AuthorID.of('1c666a92-d6b4-41fe-b88d-c4170b146ec0'), AuthorName.of('著者1'), AuthorHiragana.of('ちょしゃ1'), AuthorRomaji.of('chosha1')),
        TankaDescription.of('短歌1'),
        TankaHiragana.of('たんか1'),
        TankaRomaji.of('tanka1'),
        empty<Translation>()
      );
      const tankaOutline2: TankaOutline = TankaOutline.from(
        TankaID.of('0d103e30-9425-4cd5-a74b-67523ccee26f'),
        Author.from(AuthorID.of('c595472c-d47a-4f7c-9b42-cc0895bbde36'), AuthorName.of('著者2'), AuthorHiragana.of('ちょしゃ2'), AuthorRomaji.of('chosha2')),
        TankaDescription.of('短歌2'),
        TankaHiragana.of('たんか2'),
        TankaRomaji.of('tanka2'),
        empty<Translation>()
      );
      stub2.resolves([
        tankaOutline1,
        tankaOutline2
      ]);

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));

      const tankaInteractor: TankaInteractor = TankaInteractor.getInstance();
      const tankaOutline: TankaOutline = await tankaInteractor.findTankaAtRandom(language);

      expect(tankaOutline).toEqual(tankaOutline1);
    });
  });

  describe('findTanka', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      TankaQuery.prototype.find = stub;
      const tankaID: TankaID = TankaID.of('643cd3a1-7e4d-4e68-b246-d4ea6f0e5fc7');
      const tanka: Tanka = Tanka.from(
        tankaID,
        Author.from(AuthorID.of('1c666a92-d6b4-41fe-b88d-c4170b146ec0'), AuthorName.of('著者'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')),
        TankaDescription.of('短歌'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>(),
        Words.from([])
      );
      stub.resolves(tanka);

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));

      const tankaInteractor: TankaInteractor = TankaInteractor.getInstance();
      const found: Tanka = await tankaInteractor.findTanka(tankaID, language);

      expect(found).toEqual(tanka);
    });
  });

  describe('create', () => {
    it('normal case', async () => {
      const spy: SinonSpy = sinon.spy();
      MySQL.prototype.transact = spy;
      const tanka: Tanka = Tanka.from(
        TankaID.of('643cd3a1-7e4d-4e68-b246-d4ea6f0e5fc7'),
        Author.from(AuthorID.of('1c666a92-d6b4-41fe-b88d-c4170b146ec0'), AuthorName.of('著者'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')),
        TankaDescription.of('短歌'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>(),
        Words.from([])
      );
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('291a7831-c185-43df-8214-65d89b34a7c4');

      const tankaInteractor: TankaInteractor = TankaInteractor.getInstance();
      await tankaInteractor.create(tanka, flavourAccountID);

      expect(spy.called).toEqual(true);
    });
  });

  describe('findPendings', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      TankaQuery.prototype.findPendingTankas = stub;
      const tankaOutline1: TankaOutline = TankaOutline.from(
        TankaID.of('643cd3a1-7e4d-4e68-b246-d4ea6f0e5fc7'),
        Author.from(AuthorID.of('1c666a92-d6b4-41fe-b88d-c4170b146ec0'), AuthorName.of('著者1'), AuthorHiragana.of('ちょしゃ1'), AuthorRomaji.of('chosha1')),
        TankaDescription.of('短歌1'),
        TankaHiragana.of('たんか1'),
        TankaRomaji.of('tanka1'),
        empty<Translation>()
      );
      const tankaOutline2: TankaOutline = TankaOutline.from(
        TankaID.of('0d103e30-9425-4cd5-a74b-67523ccee26f'),
        Author.from(AuthorID.of('c595472c-d47a-4f7c-9b42-cc0895bbde36'), AuthorName.of('著者2'), AuthorHiragana.of('ちょしゃ2'), AuthorRomaji.of('chosha2')),
        TankaDescription.of('短歌2'),
        TankaHiragana.of('たんか2'),
        TankaRomaji.of('tanka2'),
        empty<Translation>()
      );
      stub.resolves([
        tankaOutline1,
        tankaOutline2
      ]);

      const language: Language = Language.from(LanguageID.of(1), LanguageName.of('日本語'), ISO639.of('ja'));

      const tankaInteractor: TankaInteractor = TankaInteractor.getInstance();
      const tankaOutlines: Array<TankaOutline> = await tankaInteractor.findPendings(language, 1);

      expect(tankaOutlines.length).toEqual(2);
      expect(tankaOutlines[0]).toEqual(tankaOutline1);
      expect(tankaOutlines[1]).toEqual(tankaOutline2);
    });
  });

  describe('approve', () => {
    it('normal case', async () => {
      const spy: SinonSpy = sinon.spy();
      MySQL.prototype.transact = spy;
      const tankaID: TankaID = TankaID.of('0d103e30-9425-4cd5-a74b-67523ccee26f');

      const tankaInteractor: TankaInteractor = TankaInteractor.getInstance();
      await tankaInteractor.approve(tankaID);

      expect(spy.called).toEqual(true);
    });
  });

  describe('update', () => {
    it('normal case', async () => {
      const spy: SinonSpy = sinon.spy();
      MySQL.prototype.transact = spy;
      const tanka: Tanka = Tanka.from(
        TankaID.of('643cd3a1-7e4d-4e68-b246-d4ea6f0e5fc7'),
        Author.from(AuthorID.of('1c666a92-d6b4-41fe-b88d-c4170b146ec0'), AuthorName.of('著者'), AuthorHiragana.of('ちょしゃ'), AuthorRomaji.of('chosha')),
        TankaDescription.of('短歌'),
        TankaHiragana.of('たんか'),
        TankaRomaji.of('tanka'),
        empty<Translation>(),
        Words.from([])
      );
      const flavourAccountID: FlavourAccountID = FlavourAccountID.of('291a7831-c185-43df-8214-65d89b34a7c4');

      const tankaInteractor: TankaInteractor = TankaInteractor.getInstance();
      await tankaInteractor.update(tanka, flavourAccountID);

      expect(spy.called).toEqual(true);
    });
  });
});
