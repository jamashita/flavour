import { Word } from '../flavour-entity/Word';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { WordID } from '../flavour-vo/WordID';

export class WordCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): WordCommand {
    return new WordCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public async create(word: Word): Promise<any> {
    const query: string = `INSERT INTO words VALUES (
      :wordID,
      :word
      );`;

    return this.query.execute(query, {
      wordID: word.getWordID().get(),
      word: word.getWord().get()
    });
  }

  public update(word: Word): Promise<any> {
    const query: string = `UPDATE words SET
      word = :word
      WHERE word_id = :wordID;`;

    return this.query.execute(query, {
      word: word.getWord().get(),
      wordID: word.getWordID().get()
    });
  }

  public async delete(wordID: WordID): Promise<any> {
    const query: string = `DELETE FROM words
      WHERE word_id = :wordID;`;

    return this.query.execute(query, {
      wordID: wordID.get()
    });
  }
}
