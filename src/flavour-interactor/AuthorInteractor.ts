import { Author } from '../flavour-entity/Author';
import { ITransaction } from '../flavour-general/MySQL/ITransaction';
import { flavourMySQL } from '../flavour-infrastructure/FlavourMySQL';
import { AuthorCreateTransaction } from '../flavour-transaction/AuthorCreateTransaction';
import { AuthorDeleteTransaction } from '../flavour-transaction/AuthorDeleteTransaction';
import { AuthorUpdateTransaction } from '../flavour-transaction/AuthorUpdateTransaction';
import { AuthorID } from '../flavour-vo/AuthorID';

export class AuthorInteractor {
  private static instance: AuthorInteractor = new AuthorInteractor();

  public static getInstance(): AuthorInteractor {
    return AuthorInteractor.instance;
  }

  private constructor() {
  }

  public create(author: Author): Promise<any> {
    const authorCreateTransaction: ITransaction = AuthorCreateTransaction.getInstance(author);

    return authorCreateTransaction.with(flavourMySQL);
  }

  public update(author: Author): Promise<any> {
    const authorUpdateTransaction: ITransaction = AuthorUpdateTransaction.getInstance(author);

    return authorUpdateTransaction.with(flavourMySQL);
  }

  public delete(authorID: AuthorID): Promise<any> {
    const authorDeleteTransaction: ITransaction = AuthorDeleteTransaction.getInstance(authorID);

    return authorDeleteTransaction.with(flavourMySQL);
  }
}
