import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Meaning } from '../../flavour-entity/Meaning';
import { Word } from '../../flavour-entity/Word';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { present } from '../../flavour-general/Optional/Present';
import { MeaningDescription } from '../../flavour-vo/MeaningDescription';
import { MeaningID } from '../../flavour-vo/MeaningID';
import { WordDescription } from '../../flavour-vo/WordDescription';
import { WordID } from '../../flavour-vo/WordID';
import { WordCommand } from '../WordCommand';

describe('WordCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const word: Word = Word.from(
        WordID.of('2a24aea8-5d97-4ff6-8168-de30f59417e6'),
        WordDescription.of('word'),
        present<Meaning>(Meaning.from(
          MeaningID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
          MeaningDescription.of('meaning')
        ))
      );

      const wordCommand: WordCommand = WordCommand.getInstance(query);
      await wordCommand.create(word);

      expect(stub.withArgs(`INSERT INTO words VALUES (
      :wordID,
      :word
      );`, {
        wordID: '2a24aea8-5d97-4ff6-8168-de30f59417e6',
        word: 'word'
      }).called).toEqual(true);
    });
  });

  describe('update', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const word: Word = Word.from(
        WordID.of('2a24aea8-5d97-4ff6-8168-de30f59417e6'),
        WordDescription.of('word'),
        present<Meaning>(Meaning.from(
          MeaningID.of('9cbe1496-e10c-4550-89cd-36f501f89e93'),
          MeaningDescription.of('meaning')
        ))
      );

      const wordCommand: WordCommand = WordCommand.getInstance(query);
      await wordCommand.update(word);

      expect(stub.withArgs(`UPDATE words SET
      word = :word
      WHERE word_id = :wordID;`, {
        word: 'word',
        wordID: '2a24aea8-5d97-4ff6-8168-de30f59417e6'
      }).called).toEqual(true);
    });
  });

  describe('delete', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const wordID: WordID = WordID.of('2a24aea8-5d97-4ff6-8168-de30f59417e6');

      const wordCommand: WordCommand = WordCommand.getInstance(query);
      await wordCommand.delete(wordID);

      expect(stub.withArgs(`DELETE FROM words
      WHERE word_id = :wordID;`, {
        wordID: '2a24aea8-5d97-4ff6-8168-de30f59417e6'
      }).called).toEqual(true);
    });
  });

});
