import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { FlavourAccount } from '../../flavour-entity/FlavourAccount';
import { NoSuchElementError } from '../../flavour-error/NoSuchElementError';
import { flavourMySQL } from '../../flavour-infrastructure/FlavourMySQL';
import { AccountName } from '../../flavour-vo/AccountName';
import { FlavourAccountID } from '../../flavour-vo/FlavourAccountID';
import { FlavourAccountHash, FlavourAccountQuery } from '../FlavourAccountQuery';

describe('FlavourAccountQuery', () => {
  describe('findByAccount', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      flavourMySQL.execute = stub;
      stub.resolves([
        {
          flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de',
          account: 'account',
          languageID: 1,
          languageName: '日本語',
          iso639: 'ja'
        }
      ]);

      const flavourAccountQuery: FlavourAccountQuery = FlavourAccountQuery.getInstance();
      const flavourAccountHash: FlavourAccountHash = await flavourAccountQuery.findByAccount(AccountName.of('account'));
      const flavourAccount: FlavourAccount = flavourAccountHash.flavourAccount;

      expect(flavourAccount.getFlavourAccountID().get()).toEqual('998106de-b2e7-4981-9643-22cd30cd74de');
      expect(flavourAccount.getAccount().get()).toEqual('account');
      expect(flavourAccount.getLanguage().getLanguageID().get()).toEqual(1);

      expect(flavourAccount.getLanguage().getName().get()).toEqual('日本語');
      expect(flavourAccount.getLanguage().getISO639().get()).toEqual('ja');
    });
  });

  describe('find', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      flavourMySQL.execute = stub;
      stub.resolves([
        {
          flavourAccountID: '998106de-b2e7-4981-9643-22cd30cd74de',
          account: 'account',
          languageID: 1,
          languageName: '日本語',
          iso639: 'ja'
        }
      ]);

      const flavourAccountQuery: FlavourAccountQuery = FlavourAccountQuery.getInstance();
      const flavourAccountHash: FlavourAccountHash = await flavourAccountQuery.find(FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'));
      const flavourAccount: FlavourAccount = flavourAccountHash.flavourAccount;

      expect(flavourAccount.getFlavourAccountID().get()).toEqual('998106de-b2e7-4981-9643-22cd30cd74de');
      expect(flavourAccount.getAccount().get()).toEqual('account');
      expect(flavourAccount.getLanguage().getLanguageID().get()).toEqual(1);

      expect(flavourAccount.getLanguage().getName().get()).toEqual('日本語');
      expect(flavourAccount.getLanguage().getISO639().get()).toEqual('ja');
    });

    it('throws error when the account didn\'t find', () => {
      const stub: SinonStub = sinon.stub();
      flavourMySQL.execute = stub;
      stub.resolves([
      ]);

      const flavourAccountQuery: FlavourAccountQuery = FlavourAccountQuery.getInstance();
      expect(flavourAccountQuery.find(FlavourAccountID.of('998106de-b2e7-4981-9643-22cd30cd74de'))).rejects.toThrow(NoSuchElementError);
    });
  });
});
