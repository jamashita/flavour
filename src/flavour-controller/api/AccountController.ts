import express from 'express';
import { BAD_REQUEST, CREATED, INTERNAL_SERVER_ERROR, OK, PRECONDITION_FAILED } from 'http-status';
import log4js from 'log4js';
import { FlavourAccount, FlavourAccountJSON } from '../../flavour-entity/FlavourAccount';
import { Language } from '../../flavour-entity/Language';
import { AccountAlreadyInUseError } from '../../flavour-error/AccountAlreadyInUseError';
import { Type } from '../../flavour-general/Type/Type';
import { AccountInteractor } from '../../flavour-interactor/AccountInteractor';
import { AccountName } from '../../flavour-vo/AccountName';
import { FlavourAccountID } from '../../flavour-vo/FlavourAccountID';
import { AuthenticationMiddleware } from '../middlewares/AuthenticationMiddleware';

const router: express.Router = express.Router();
const logger: log4js.Logger = log4js.getLogger();

const authenticationMiddleware: AuthenticationMiddleware = AuthenticationMiddleware.getInstance();
const accountInteractor: AccountInteractor = AccountInteractor.getInstance();

router.post('/', async (req: express.Request, res: express.Response): Promise<any> => {
  const {
    flavourAccountID,
    account,
    language
  } = req.body;

  if (!Type.isString(flavourAccountID)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isString(account)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isPlainObject(language)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isInteger(language.languageID)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isString(language.name)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isString(language.iso639)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }

  try {
    const json: FlavourAccountJSON = req.body;
    const flavourAccount: FlavourAccount = FlavourAccount.fromJSON(json);
    const password: string = await accountInteractor.create(flavourAccount);

    res.status(CREATED).send({
      password
    });
  }
  catch (err) {
    if (err instanceof AccountAlreadyInUseError) {
      res.sendStatus(PRECONDITION_FAILED);
      return;
    }

    logger.fatal(err.message);
    res.sendStatus(INTERNAL_SERVER_ERROR);
  }
});

router.put('/', authenticationMiddleware.apply(), async (req: express.Request, res: express.Response): Promise<any> => {
  const {
    account,
    language
  } = req.body;

  if (!Type.isString(account)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isPlainObject(language)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isNumber(language.languageID)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isString(language.name)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }
  if (!Type.isString(language.iso639)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }

  try {
    const flavourAccountID: FlavourAccountID = req.account.getFlavourAccountID();
    const accountName: AccountName = AccountName.of(account);
    const flavourAccount: FlavourAccount = FlavourAccount.from(flavourAccountID, accountName, Language.fromJSON(language));

    await accountInteractor.update(flavourAccount);
    res.sendStatus(OK);
  }
  catch (err) {
    if (err instanceof AccountAlreadyInUseError) {
      res.sendStatus(PRECONDITION_FAILED);
      return;
    }

    logger.fatal(err.message);
    res.sendStatus(INTERNAL_SERVER_ERROR);
  }
});

router.put('/regenerate', authenticationMiddleware.apply(), async (req: express.Request, res: express.Response): Promise<any> => {
  const {
    flavourAccountID
  } = req.body;

  if (!Type.isString(flavourAccountID)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }

  try {
    const password: string = await accountInteractor.regeneratePassword(FlavourAccountID.of(flavourAccountID));

    res.send({
      password
    });
  }
  catch (err) {
    logger.fatal(err.message);
    res.sendStatus(INTERNAL_SERVER_ERROR);
  }
});

router.put('/activate', authenticationMiddleware.apply(), async (req: express.Request, res: express.Response): Promise<any> => {
  const {
    flavourAccountID
  } = req.body;

  if (!Type.isString(flavourAccountID)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }

  try {
    await accountInteractor.activate(FlavourAccountID.of(flavourAccountID));

    res.sendStatus(OK);
  }
  catch (err) {
    logger.fatal(err.message);
    res.sendStatus(INTERNAL_SERVER_ERROR);
  }
});

router.put('/deactivate', authenticationMiddleware.apply(), async (req: express.Request, res: express.Response): Promise<any> => {
  const {
    flavourAccountID
  } = req.body;

  if (!Type.isString(flavourAccountID)) {
    res.sendStatus(BAD_REQUEST);
    return;
  }

  try {
    await accountInteractor.deactivate(FlavourAccountID.of(flavourAccountID));

    res.sendStatus(OK);
  }
  catch (err) {
    logger.fatal(err.message);
    res.sendStatus(INTERNAL_SERVER_ERROR);
  }
});

export const AccountController: express.Router = router;
