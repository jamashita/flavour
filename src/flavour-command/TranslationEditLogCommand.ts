import { Translation } from '../flavour-entity/Translation';
import { IQuery } from '../flavour-general/MySQL/IQuery';
import { FlavourAccountID } from '../flavour-vo/FlavourAccountID';

export class TranslationEditLogCommand {
  private query: IQuery;

  public static getInstance(query: IQuery): TranslationEditLogCommand {
    return new TranslationEditLogCommand(query);
  }

  private constructor(query: IQuery) {
    this.query = query;
  }

  public create(translation: Translation, flavourAccountID: FlavourAccountID): Promise<any> {
    const query: string = `INSERT INTO translation_edit_logs VALUES (
      :translationID,
      :flavourAccountID,
      :translation,
      UTC_TIMESTAMP()
      );`;

    return this.query.execute(query, {
      translationID: translation.getTranslationID().get(),
      flavourAccountID: flavourAccountID.get(),
      translation: translation.getTranslation().get()
    });
  }
}
