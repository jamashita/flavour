import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { Languages } from '../../flavour-entity/collection/Languages';
import { Language } from '../../flavour-entity/Language';
import { Redis } from '../../flavour-general/Redis/Redis';
import { RedisString } from '../../flavour-general/Redis/RedisString';
import { ISO639 } from '../../flavour-vo/ISO639';
import { LanguageID } from '../../flavour-vo/LanguageID';
import { LanguageName } from '../../flavour-vo/LanguageName';
import { LanguageCommand } from '../LanguageCommand';

describe('LanguageCommand', () => {
  describe('insertAll', () => {
    it('normal case', async () => {
      const stub1: SinonStub = sinon.stub();
      RedisString.prototype.set = stub1;
      const stub2: SinonStub = sinon.stub();
      Redis.prototype.expires = stub2;

      const languages: Languages = Languages.from([
        Language.from(
          LanguageID.of(1),
          LanguageName.of('language 1'),
          ISO639.of('aa')
        ),
        Language.from(
          LanguageID.of(2),
          LanguageName.of('language 2'),
          ISO639.of('bb')
        )
      ]);
      const languageCommand: LanguageCommand = LanguageCommand.getInstance();
      await languageCommand.insertAll(languages);

      expect(stub1.withArgs('LANGUAGES', '[{"languageID":1,"name":"language 1","iso639":"aa"},{"languageID":2,"name":"language 2","iso639":"bb"}]').called).toEqual(true);
      expect(stub2.withArgs('LANGUAGES', 3 * 60 * 60).called).toEqual(true);
    });
  });

  describe('deleteAll', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      Redis.prototype.delete = stub;

      const languageCommand: LanguageCommand = LanguageCommand.getInstance();
      await languageCommand.deleteAll();

      expect(stub.withArgs('LANGUAGES').called).toEqual(true);
    });
  });
});
