import { RuntimeError } from '../flavour-error/RuntimeError';
import { empty } from '../flavour-general/Optional/Empty';
import { Optional } from '../flavour-general/Optional/Optional';
import { present } from '../flavour-general/Optional/Present';
import { MeaningDescription } from '../flavour-vo/MeaningDescription';
import { MeaningID } from '../flavour-vo/MeaningID';
import { WordDescription } from '../flavour-vo/WordDescription';
import { WordID } from '../flavour-vo/WordID';
import { Entity } from './Entity';
import { Meaning, MeaningJSON } from './Meaning';

export type WordJSON = {
  wordID: string;
  word: string;
  meaning: MeaningJSON | null;
};

export type WordRow = {
  wordID: string;
  word: string;
  meaningID: string | null;
  meaning: string | null;
};

export class Word extends Entity<WordID> {
  private wordID: WordID;
  private word: WordDescription;
  private meaning: Optional<Meaning>;

  public static from(wordID: WordID, word: WordDescription, meaning: Optional<Meaning>): Word {
    return new Word(wordID, word, meaning);
  }

  public static fromJSON(json: WordJSON): Word {
    const {
      wordID,
      word,
      meaning
    } = json;

    if (meaning === null) {
      return Word.from(WordID.of(wordID), WordDescription.of(word), empty<Meaning>());
    }

    return Word.from(WordID.of(wordID), WordDescription.of(word), present<Meaning>(Meaning.fromJSON(meaning)));
  }

  public static fromRow(row: WordRow): Word {
    const {
      wordID,
      word,
      meaningID,
      meaning
    } = row;

    if (meaningID === null) {
      return Word.from(WordID.of(wordID), WordDescription.of(word), empty<Meaning>());
    }
    if (meaning === null) {
      throw new RuntimeError('MEANING ID IS NOT NULL BUT MEANING IS NULL');
    }

    return Word.from(WordID.of(wordID), WordDescription.of(word), present<Meaning>(Meaning.from(MeaningID.of(meaningID), MeaningDescription.of(meaning))));
  }

  private constructor(wordID: WordID, word: WordDescription, meaning: Optional<Meaning>) {
    super();
    this.wordID = wordID;
    this.word = word;
    this.meaning = meaning;
  }

  public getWordID(): WordID {
    return this.wordID;
  }

  public getWord(): WordDescription {
    return this.word;
  }

  public getMeaning(): Optional<Meaning> {
    return this.meaning;
  }

  public getIdentifier(): WordID {
    return this.wordID;
  }

  public copy(): Word {
    const {
      wordID,
      word,
      meaning
    } = this;

    if (meaning.isPresent()) {
      const newMeaning: Meaning = meaning.get().copy();

      return new Word(wordID, word, present<Meaning>(newMeaning));
    }

    return new Word(wordID, word, empty<Meaning>());
  }

  public toJSON(): WordJSON {
    const {
      wordID,
      word,
      meaning
    } = this;

    if (meaning.isPresent()) {
      return {
        wordID: wordID.get(),
        word: word.get(),
        meaning: meaning.get().toJSON()
      };
    }

    return {
      wordID: wordID.get(),
      word: word.get(),
      meaning: null
    };
  }

  public toString(): string {
    const {
      wordID,
      word,
      meaning
    } = this;

    return `${wordID.toString()} ${word.toString()} ${meaning.toString()}`;
  }
}
