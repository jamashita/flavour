import 'jest';
import { TankaDescription } from '../TankaDescription';

describe('TankaDescription', () => {
  describe('equals', () => {
    it('returns true if both properties are the same', () => {
      const description1: TankaDescription = TankaDescription.of('description 1');
      const description2: TankaDescription = TankaDescription.of('description 2');
      const description3: TankaDescription = TankaDescription.of('description 1');

      expect(description1.equals(description1)).toEqual(true);
      expect(description1.equals(description2)).toEqual(false);
      expect(description1.equals(description3)).toEqual(true);
    });
  });
});
