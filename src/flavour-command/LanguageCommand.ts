import { Languages } from '../flavour-entity/collection/Languages';
import { flavourRedis } from '../flavour-infrastructure/FlavourRedis';

const REDIS_KEY: string = 'LANGUAGES';
const DURATION: number = 3 * 60 * 60;

export class LanguageCommand {
  private static instance: LanguageCommand = new LanguageCommand();

  public static getInstance(): LanguageCommand {
    return LanguageCommand.instance;
  }

  private constructor() {
  }

  public async insertAll(languages: Languages): Promise<any> {
    await flavourRedis.getString().set(REDIS_KEY, JSON.stringify(languages.toJSON()));

    return flavourRedis.expires(REDIS_KEY, DURATION);
  }

  public async deleteAll(): Promise<any> {
    return flavourRedis.delete(REDIS_KEY);
  }
}
