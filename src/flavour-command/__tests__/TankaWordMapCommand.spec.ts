import 'jest';
import sinon from 'sinon';
import { SinonStub } from 'sinon';
import { QueryMock } from '../../flavour-general/MySQL/QueryMock';
import { TankaID } from '../../flavour-vo/TankaID';
import { WordID } from '../../flavour-vo/WordID';
import { TankaWordMapCommand } from '../TankaWordMapCommand';

describe('TankaWordMapCommand', () => {
  describe('create', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const tankaID: TankaID = TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');
      const wordID: WordID = WordID.of('7056ac2a-41eb-4e6b-a823-999e4014448b');

      const tankaWordMapCommand: TankaWordMapCommand = TankaWordMapCommand.getInstance(query);
      await tankaWordMapCommand.create(tankaID, wordID);

      expect(stub.withArgs(`INSERT INTO tanka_word_map VALUES (
      :tankaID,
      :wordID
      );`, {
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93',
        wordID: '7056ac2a-41eb-4e6b-a823-999e4014448b'
      }).called).toEqual(true);
    });
  });

  describe('deleteByTankaID', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const tankaID: TankaID = TankaID.of('9cbe1496-e10c-4550-89cd-36f501f89e93');

      const tankaWordMapCommand: TankaWordMapCommand = TankaWordMapCommand.getInstance(query);
      await tankaWordMapCommand.deleteByTankaID(tankaID);

      expect(stub.withArgs(`DELETE FROM tanka_word_map
      WHERE tanka_id = :tankaID;`, {
        tankaID: '9cbe1496-e10c-4550-89cd-36f501f89e93'
      }).called).toEqual(true);
    });
  });

  describe('deleteByWordID', () => {
    it('normal case', async () => {
      const stub: SinonStub = sinon.stub();
      QueryMock.prototype.execute = stub;

      const query: QueryMock = new QueryMock();
      const wordID: WordID = WordID.of('7056ac2a-41eb-4e6b-a823-999e4014448b');

      const tankaWordMapCommand: TankaWordMapCommand = TankaWordMapCommand.getInstance(query);
      await tankaWordMapCommand.deleteByWordID(wordID);

      expect(stub.withArgs(`DELETE FROM tanka_word_map
      WHERE word_id = :wordID;`, {
        wordID: '7056ac2a-41eb-4e6b-a823-999e4014448b'
      }).called).toEqual(true);
    });
  });
});
